/* This file is a part of the program Kira.
 * Copyright (C) Johann Usovitsch <jusovitsch@googlemail.com>
 * Philipp Maierhoefer <particle@maierhoefer.net>
 * Peter Uwer <peter.uwer@physik.hu-berlin.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version , or (at
 * your option) any later version as published by the Free Software
 * Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
*/
#ifndef READYAMLFILES_H_
#define READYAMLFILES_H_
#include <algorithm>
#include <tuple>
#include "ginac/ginac.h"
#include "tools.h"
#include "integral.h"
#include "yaml-cpp/yaml.h"
#define SIZESYM 64
// #include "trivial_sym.h"

typedef std::vector< symmetries > SYM;
typedef SYM::iterator ItSYM;

typedef std::vector< BaseEquation * > VE;
typedef VE::iterator ItVE;

struct Jobs {
  std::vector<std::tuple<std::vector<std::string> /*topologies*/,
  std::vector<int> /*sectors*/,
  int /*rmax*/,
  int /*smax*/,
  int /*dmax*/> > reductSpec;

  std::vector<std::tuple<std::vector<std::string> /*topologies*/,
  std::vector<int> /*sectors*/,
  int /*rmax*/,
  int /*smax*/,
  int /*dmax*/> > selectSpec;

  std::vector<std::pair<std::string,std::string> > sector2Reduce;
  std::vector<std::pair<std::string,std::string> > den;
  std::vector<std::pair<std::string,std::string> > num;
  std::vector<std::pair<std::string,std::string> > mandatoryFile;
  std::vector<std::pair<std::string,std::string> > optionalFile;
#ifdef KIRAFIREFLY
  std::string mandatoryIDFile;
  std::string ff_recon;
#endif
  std::vector<std::vector<std::string> > mandatoryRec;
  std::vector<std::vector<std::string> > optionalRec;
  std::string masters;
  std::string specialname;
  std::string runBacksubstitution;
  std::string runTriangular;
  std::string runPyred;
  std::string pyredDatabase;
  std::string runInitiate;
  int integralOrdering;
  unsigned max_parallel_sectors;
  std::string runSymmetries;
  std::string dataBase;
  std::string dataFile;
  std::string outputDir;
  std::string serialWrite;
  std::string skipPyred;
  std::string writeNumericalSystem;
  std::string conditional;
  std::string LIflag;
  std::string algebraicReconstruction;
  unsigned resumeRun;
  std::multimap<std::string, std::vector<int> > selectMastersReduction;
  std::string inputSystem;
};

struct Kira2File {
  std::vector<std::pair<std::string,std::string> > target;
  std::vector<std::vector<std::string> > mandatoryRec;
  std::string inputDir;
  std::string reconstructMass;
  std::vector<std::tuple<std::vector<std::string> /*topologies*/,
  std::vector<int> /*sectors*/,
  int /*rmax*/,
  int /*smax*/,
  int /*dmax*/> > selectSpec;
};

struct Merge {
  std::vector<std::string > files2merge;
  std::string outputDir;
};

struct Dgl {
  std::vector<std::string> filesDGL;
};

struct Integral_F {
  std::string name;
  std::vector<std::string> loop;
  std::vector<GiNaC::possymbol> loopVar;
  std::vector<GiNaC::possymbol> allVar;
  GiNaC::lst loop2loop, loop2loop2, loopVarList, loopVarList2;
  GiNaC::lst props, propsMomFlowA;
  std::vector<GiNaC::lst> scal2Props, propsMomFlowB;
  int jule;
  GiNaC::possymbol *propSymb;
  GiNaC::lst scalprod;
  GiNaC::ex G, Gsym, U, F; // U + F Polynomial
  GiNaC::lst FPolynom;
  std::vector<int> *mask, *allowSector;
  std::set<int> *skipSector;
  std::vector<int> zeroSector;

  VE identitiesIBP, identitiesLI, identitiesDGLmom, identitiesLEE, identitiesDGLmasses, identitiesDGLxKira;

  std::vector<std::pair<std::string, BaseEquation*> > identitiesDGL;

  SYM *symVec, *relVec, *symVecReverse;
  std::vector<int> sector2Reduce, bound;
  int biggestBound;
  int biggestSector2Reduce;
  std::vector <unsigned> lowestSectors;
  int topology;
  std::vector <std::pair<std::string,std::string> > propagator;
  int* invarID;
  std::vector<int> topLevelSectors;
  std::vector<int> cutProps;
  unsigned maskCut;
  std::vector<int> permute;
  std::vector<std::tuple<std::vector<int>,int,int,int > > reductSpec;
  std::string magic_relations;
  std::unordered_map<int,std::vector<std::tuple<size_t,std::vector<int* >, int, int >
    > > symmetries;
  std::unordered_map<int,std::vector<std::tuple<size_t,std::vector<int* >, int, int >
    > > symmetriesCrossed;
  int propsMomentaFlowMask;
  std::vector<int> symbolicIBP;
};

struct Kinematics {
  std::vector<std::string> im;
  std::vector<std::string> om;
  std::string rpl;
  std::pair<std::string,std::string> mc;
  std::vector<std::pair<std::string,int> > ki;
  std::vector<GiNaC::possymbol> invariantsPlaceholder;
  GiNaC::lst invariantsReplacement, invariantsReplacementRev;
  std::vector <std::pair<std::pair<std::string,std::string>,std::string> > sr;
#ifdef KIRAFIREFLY
  std::vector<std::string> symbol_order;
#endif
};

namespace YAML {
  template<>
  struct convert<Jobs> {
    static bool decode(const Node& node, Jobs& rhs) {

      for(YAML::const_iterator it=node.begin(); it != node.end();++it){
        std::string token = (*it).first.as<std::string>();
        if(token != "algebraic_reconstruction"
	  && token != "variables" && token != "input_system"
	  && token != "reduce"
	  && token != "sector_selection" && token != "identities"
	  && token != "select_integrals"
	  && token != "select_masters"  && token != "preferred_masters"
          && token != "run_triangular" && token != "run_back_substitution"
	  && token != "run_pyred" && token != "integral_ordering"
          && token != "run_initiate" && token != "run_symmetries"
          && token != "alt_dir" && token != "data_file" && token != "data_base"
          && token != "serial_write" && token != "skip_pyred"
	  && token != "max_parallel_sectors"
          && token != "write_numerical_system" && token != "select_masters_reduction"
	  && token != "conditional" && token != "bs_reuse_memory"
	  && token != "lorenz_invariance"
#ifndef KIRAFIREFLY
	  && token != "pyred_database") {
#else
	  && token != "pyred_database" && token != "finite_fields_reconstruction") {
#endif
          std::cout << "\n***********************************************************\n";
          std::cout << "***Kira does not know the option: " << token << std::endl;
          std::cout << "***In the job file.";
          std::cout << "\n***********************************************************\n";
          exit(-1);
        }
      }
      if(node["reduce"]){
	const Node& nodeibp = node["reduce"];

	for(unsigned i = 0; i < nodeibp.size(); i++){
	  std::vector<std::string> topologies;
	  std::vector<int> sectors;
	  int rmax = -1;
	  int smax = -1;
	  int dmax = std::numeric_limits<int>::max();

	  if(nodeibp[i]["topologies"]){

	    for(unsigned itT = 0; itT != nodeibp[i]["topologies"].size(); itT++){

	      topologies.push_back(nodeibp[i]["topologies"][itT].as<std::string>());
	    }
	  }

	  if(nodeibp[i]["sectors"]){

	    for(unsigned itT = 0; itT != nodeibp[i]["sectors"].size(); itT++)
	      sectors.push_back(nodeibp[i]["sectors"][itT].as<int>());
	  }

	  if(nodeibp[i]["r"] && nodeibp[i]["r"].size() == 0)
	    rmax = nodeibp[i]["r"].as<int>();

	  if(nodeibp[i]["s"] && nodeibp[i]["s"].size() == 0)
	    smax = nodeibp[i]["s"].as<int>();

	  if(nodeibp[i]["d"] && nodeibp[i]["d"].size() == 0)
	    dmax = nodeibp[i]["d"].as<int>();


	  if(rmax < 0){
	    std::cout << "rmax should be a positive number in the option r: rmax.\n";
	    exit(-1);
	  }
	  if(smax < 0){
	    std::cout << "smax should be a positive number in the option s: smax.\n";
	    exit(-1);
	  }
	  if(dmax < 0){
	    std::cout << "dmax should be a positive number in the option d: dmax.\n";
	    exit(-1);
	  }

	  if(dmax == std::numeric_limits<int>::max())
	    dmax = -1;

	  rhs.reductSpec.push_back(std::make_tuple(topologies, sectors, rmax, smax, dmax));

	  if(nodeibp[i]["r"] && nodeibp[i]["r"].size() == 2)
	    rhs.den.push_back(make_pair(nodeibp[i]["r"][0].as<std::string>(),nodeibp[i]["r"][1].as<std::string>()));
	  if(nodeibp[i]["s"] && nodeibp[i]["s"].size() == 2)
	    rhs.num.push_back(make_pair(nodeibp[i]["s"][0].as<std::string>(),nodeibp[i]["s"][1].as<std::string>()));
	}
      }

      if(node["input_system"]){
	rhs.inputSystem = node["input_system"].as<std::string>();
      }

      if(node["sector_selection"]){

        for(YAML::const_iterator it=node["sector_selection"].begin(); it != node["sector_selection"].end();++it){
          std::string token = (*it).first.as<std::string>();
          if(token != "select_recursively") {
            std::cout << "\n***********************************************************\n";
            std::cout << "***Kira does not know the option: " << token << std::endl;
            std::cout << "***In the job file.";
            std::cout << "\n***********************************************************\n";
            exit(-1);
          }
        }

	if(node["sector_selection"]["select_recursively"]){

	  const Node& nodeb = node["sector_selection"]["select_recursively"];

	  for(unsigned i=0;i<nodeb.size();i++){

	    rhs.sector2Reduce.push_back(make_pair(nodeb[i][0].as<std::string>(),nodeb[i][1].as<std::string>()));
	  }
	}
      }

      if(node["identities"]){

        for(YAML::const_iterator it=node["identities"].begin(); it != node["identities"].end();++it){
          std::string token = (*it).first.as<std::string>();
          if(token != "ibp") {
            std::cout << "\n***********************************************************\n";
            std::cout << "***Kira does not know the option: " << token << std::endl;
            std::cout << "***In the job file.";
            std::cout << "\n***********************************************************\n";
            exit(-1);
          }
        }

	if(node["identities"]["ibp"]){

	  const Node& nodeibp = node["identities"]["ibp"];

	  for(unsigned i = 0; i < nodeibp.size(); i++){
	    if(nodeibp[i]["r"] && nodeibp[i]["r"].size() == 2)
	      rhs.den.push_back(make_pair(nodeibp[i]["r"][0].as<std::string>(),nodeibp[i]["r"][1].as<std::string>()));
	    if(nodeibp[i]["s"] && nodeibp[i]["s"].size() == 2)
	      rhs.num.push_back(make_pair(nodeibp[i]["s"][0].as<std::string>(),nodeibp[i]["s"][1].as<std::string>()));
	  }
	}
      }

      if(node["select_masters"]){

	const Node& nodeibp = node["select_masters"];

	rhs.masters=nodeibp.as<std::string>();

      }

      if(node["preferred_masters"]){

	const Node& nodeibp = node["preferred_masters"];

	rhs.masters=nodeibp.as<std::string>();

      }

      if(node["select_integrals"]){

        for(YAML::const_iterator it=node["select_integrals"].begin(); it != node["select_integrals"].end();++it){
          std::string token = (*it).first.as<std::string>();
          if(token != "select_mandatory_list" && token != "select_optional_list"
	    && token != "select_mandatory_recursively"
	    && token != "select_optional_recursively"
#ifndef KIRAFIREFLY
	    && token !=  "select_masters" && token != "preferred_masters" ) {
#else
	    && token !=  "select_masters" && token != "preferred_masters" && token != "select_mandatory_idlist") {
#endif
            std::cout << "\n***********************************************************\n";
            std::cout << "***Kira does not know the option: " << token << std::endl;
            std::cout << "***In the job file.";
            std::cout << "\n***********************************************************\n";
            exit(-1);
          }
        }

        if(node["select_integrals"]["select_masters"]){

	  const Node& nodeibp = node["select_integrals"]["select_masters"];

          rhs.masters=nodeibp.as<std::string>();

	}

	if(node["select_integrals"]["preferred_masters"]){

	  const Node& nodeibp = node["select_integrals"]["preferred_masters"];

          rhs.masters=nodeibp.as<std::string>();

	}

	if(node["select_integrals"]["select_mandatory_list"]){

	  const Node& nodeibp = node["select_integrals"]["select_mandatory_list"];

	  for(unsigned i=0;i<nodeibp.size();i++){

	    rhs.mandatoryFile.push_back(make_pair(nodeibp[i][0].as<std::string>(),nodeibp[i][1].as<std::string>()));
	  }
	}

#ifdef KIRAFIREFLY
	if(node["select_integrals"]["select_mandatory_idlist"]){
	  rhs.mandatoryIDFile = node["select_integrals"]["select_mandatory_idlist"].as<std::string>();
	}
#endif

	if(node["select_integrals"]["select_optional_list"]){

	  const Node& nodeibp = node["select_integrals"]["select_optional_list"];

	  for(unsigned i=0;i<nodeibp.size();i++){

	    rhs.optionalFile.push_back(make_pair(nodeibp[i][0].as<std::string>(),nodeibp[i][1].as<std::string>()));
	  }
	}

	if(node["select_integrals"]["select_mandatory_recursively"]){
	  const Node& nodeibp = node["select_integrals"]["select_mandatory_recursively"];

// 	  rhs.selectSpec.push_back()


	  for(unsigned i = 0; i<nodeibp.size(); i++){

	    if(nodeibp[i]["r"] && nodeibp[i]["s"]){
	      std::vector<std::string> topologies;
	      std::vector<int> sectors;
	      int rmax = -1;
	      int smax = -1;
	      int dmax = std::numeric_limits<int>::max();

	      if(nodeibp[i]["topologies"]){

		for(unsigned itT = 0; itT != nodeibp[i]["topologies"].size(); itT++){

		  topologies.push_back(nodeibp[i]["topologies"][itT].as<std::string>());
		}
	      }

	      if(nodeibp[i]["sectors"]){

		for(unsigned itT = 0; itT != nodeibp[i]["sectors"].size(); itT++)
		  sectors.push_back(nodeibp[i]["sectors"][itT].as<int>());
	      }

	      if(nodeibp[i]["r"] && nodeibp[i]["r"].size() == 0)
		rmax = nodeibp[i]["r"].as<int>();

	      if(nodeibp[i]["s"] && nodeibp[i]["s"].size() == 0)
		smax = nodeibp[i]["s"].as<int>();

	      if(nodeibp[i]["d"] && nodeibp[i]["d"].size() == 0)
		dmax = nodeibp[i]["d"].as<int>();

	      if(rmax < 0){
		std::cout << "rmax should be a positive number in the option r: rmax.\n";
		exit(-1);
	      }

	      if(smax < 0){
		std::cout << "smax should be a positive number in the option s: smax.\n";
		exit(-1);
	      }
	      if(dmax < 0){
		std::cout << "dmax should be a positive number in the option d: dmax.\n";
		exit(-1);
	      }

	      if(dmax == std::numeric_limits<int>::max())
		dmax = -1;

	      rhs.selectSpec.push_back(std::make_tuple(topologies, sectors, rmax, smax, dmax));
	    }
	    else if(nodeibp[i].size() == 4){
	      std::vector<std::string> info;
	      info.push_back(nodeibp[i][0].as<std::string>());
	      info.push_back(nodeibp[i][1].as<std::string>());
	      info.push_back(nodeibp[i][2].as<std::string>());
	      info.push_back(nodeibp[i][3].as<std::string>());
	      rhs.mandatoryRec.push_back(info);
	    }
	  }
	}

	if(node["select_integrals"]["select_optional_recursively"]){
	  const Node& nodeibp = node["select_integrals"]["select_optional_recursively"];
	  for(unsigned i = 0; i<nodeibp.size(); i++){
	    std::vector<std::string> info;
	    info.push_back(nodeibp[i][0].as<std::string>());
	    info.push_back(nodeibp[i][1].as<std::string>());
	    info.push_back(nodeibp[i][2].as<std::string>());
	    info.push_back(nodeibp[i][3].as<std::string>());

	    rhs.optionalRec.push_back(info);
	  }
	}
      }

      if(node["select_masters_reduction"]){

        const Node& nodeTop = node["select_masters_reduction"];

        for(unsigned iM=0;iM<nodeTop.size();iM++) {

          if(nodeTop[iM].size()!=2){
            std::cout << "Wrong options in:\n";
            std::cout << "select_masters_reduction\n";
            continue;
          }
          std::vector<int> token;
          for(unsigned jM=0; jM<nodeTop[iM][1].size();jM++) {
            token.push_back(nodeTop[iM][1][jM].as<int>());
          }
          rhs.selectMastersReduction.insert(std::make_pair(nodeTop[iM][0].as<std::string>(),token));
        }
      }

      if (node["run_triangular"])
        rhs.runTriangular=node["run_triangular"].as<std::string>();
      if (node["run_back_substitution"])
        rhs.runBacksubstitution=node["run_back_substitution"].as<std::string>();
      if (node["run_pyred"]){
        rhs.runPyred=node["run_pyred"].as<std::string>();
      }
      if (node["pyred_database"])
        rhs.pyredDatabase=node["pyred_database"].as<std::string>();
      if (node["run_initiate"])
        rhs.runInitiate=node["run_initiate"].as<std::string>();
      rhs.max_parallel_sectors=0;
      if (node["max_parallel_sectors"])
        rhs.max_parallel_sectors=node["max_parallel_sectors"].as<unsigned>();
      if (node["run_symmetries"])
        rhs.runSymmetries=node["run_symmetries"].as<std::string>();
      if (node["data_file"])
        rhs.dataFile=node["data_file"].as<std::string>();
      if (node["alt_dir"])
        rhs.outputDir=node["alt_dir"].as<std::string>();
      if (node["serial_write"])
        rhs.serialWrite=node["serial_write"].as<std::string>();
      if (node["skip_pyred"])
        rhs.skipPyred=node["skip_pyred"].as<std::string>();
      if (node["write_numerical_system"])
        rhs.writeNumericalSystem=node["write_numerical_system"].as<std::string>();
      if (node["conditional"])
        rhs.conditional=node["conditional"].as<std::string>();
      if (node["lorenz_invariance"]){
	rhs.LIflag=node["lorenz_invariance"].as<std::string>();
      }
      if (node["algebraic_reconstruction"]){
	rhs.algebraicReconstruction=node["algebraic_reconstruction"].as<std::string>();
      }

      rhs.integralOrdering = 9;
      if (node["integral_ordering"]){
	if(node["integral_ordering"].as<int>() < 1 || node["integral_ordering"].as<int>() > 8){
	  std::cout << "The option integral_ordering in your job file is invalid."<< std::endl;
	  exit(-1);
	}
        rhs.integralOrdering=node["integral_ordering"].as<int>();
      }

#ifdef KIRAFIREFLY
      if (node["finite_fields_reconstruction"]) rhs.ff_recon=node["finite_fields_reconstruction"].as<std::string>();
#endif

      return true;
    }
  };


  template<>
  struct convert<Kira2File> {
    static bool decode(const Node& node, Kira2File& rhs) {

      for(YAML::const_iterator it = node.begin(); it != node.end(); it++){
        std::string token = (*it).first.as<std::string>();
        if(token != "target" && token != "reconstruct_mass" && token != "alt_dir") {
          std::cout << "\n***********************************************************\n";
          std::cout << "***Kira does not know the option: " << token << std::endl;
          std::cout << "***In the job file.";
          std::cout << "\n***********************************************************\n";
        }
      }

      if(node["target"]){

        const Node& nodet = node["target"];

	for(unsigned i=0;i<nodet.size();i++){
	  if(nodet[i]["r"] && nodet[i]["s"]){
	    std::vector<std::string> topologies;
	    std::vector<int> sectors;
	    int rmax = -1;
	    int smax = -1;
	    int dmax = std::numeric_limits<int>::max();

	    if(nodet[i]["topologies"]){

	      for(unsigned itT = 0; itT != nodet[i]["topologies"].size(); itT++){

		topologies.push_back(nodet[i]["topologies"][itT].as<std::string>());
	      }
	    }

	    if(nodet[i]["sectors"]){

	      for(unsigned itT = 0; itT != nodet[i]["sectors"].size(); itT++)
		sectors.push_back(nodet[i]["sectors"][itT].as<int>());
	    }

	    if(nodet[i]["r"] && nodet[i]["r"].size() == 0)
	      rmax = nodet[i]["r"].as<int>();

	    if(nodet[i]["s"] && nodet[i]["s"].size() == 0)
	      smax = nodet[i]["s"].as<int>();

	    if(nodet[i]["d"] && nodet[i]["d"].size() == 0)
	      dmax = nodet[i]["d"].as<int>();

	    if(rmax < 0){
	      std::cout << "rmax should be a positive number in the option r: rmax.\n";
	      exit(-1);
	    }

	    if(smax < 0){
	      std::cout << "smax should be a positive number in the option s: smax.\n";
	      exit(-1);
	    }
	    if(dmax < 0){
	      std::cout << "dmax should be a positive number in the option d: dmax.\n";
	      exit(-1);
	    }

	    if(dmax == std::numeric_limits<int>::max())
	      dmax = -1;

	    rhs.selectSpec.push_back(std::make_tuple(topologies, sectors, rmax, smax, dmax));
	  }
	  else if(nodet[i].size()==2){

	    rhs.target.push_back(make_pair(nodet[i][0].as<std::string>(),nodet[i][1].as<std::string>()));
	  }
	  else if(nodet[i].size()==4){

	    std::vector<std::string> info;
	    info.push_back(nodet[i][0].as<std::string>());
	    info.push_back(nodet[i][1].as<std::string>());
	    info.push_back(nodet[i][2].as<std::string>());
	    info.push_back(nodet[i][3].as<std::string>());
	    rhs.mandatoryRec.push_back(info);
	  }
	}
      }

      if (node["alt_dir"])
        rhs.inputDir=node["alt_dir"].as<std::string>();
      if (node["reconstruct_mass"])
        rhs.reconstructMass=node["reconstruct_mass"].as<std::string>();

      return true;
    }
  };

  template<>
  struct convert<Merge> {
    static bool decode(const Node& node, Merge& rhs) {

      for(YAML::const_iterator it = node.begin(); it != node.end(); it++){
        std::string token = (*it).first.as<std::string>();
        if(token != "files2merge" && token != "alt_dir" ) {
          std::cout << "\n***********************************************************\n";
          std::cout << "***Kira does not know the option: " << token << std::endl;
          std::cout << "***In the job file.";
          std::cout << "\n***********************************************************\n";
        }
      }

      if(node["files2merge"]){

        const Node& nodet = node["files2merge"];

	for(unsigned i=0; i<nodet.size();i++){

	  rhs.files2merge.push_back(node["files2merge"][i].as<std::string>());
	}
      }
      if (node["alt_dir"])
        rhs.outputDir=node["alt_dir"].as<std::string>();

      return true;
    }
  };

  template<>
  struct convert<Dgl> {
    static bool decode(const Node& node, Dgl& rhs) {

      for(YAML::const_iterator it = node.begin(); it != node.end(); it++){
        std::string token = (*it).first.as<std::string>();
        if(token != "derive_dgl" ) {
          std::cout << "\n***********************************************************\n";
          std::cout << "***Kira does not know the option: " << token << std::endl;
          std::cout << "***In the job file.";
          std::cout << "\n***********************************************************\n";
        }
      }

      if(node["derive_dgl"]){
	  std::cout << "fff" << std::endl;
        const Node& nodet = node["derive_dgl"];

	for(unsigned i=0; i<nodet.size();i++){

	  rhs.filesDGL.push_back(node["derive_dgl"][i].as<std::string>());
	}
      }

      return true;
    }
  };


  template<>
  struct convert<Kinematics> {
    static bool decode(const Node& node, Kinematics& rhs) {

      for(YAML::const_iterator it=node.begin(); it != node.end();++it){
        std::string token = (*it).first.as<std::string>();

	if(token != "incoming_momenta" && token != "outgoing_momenta" && token != "momentum_conservation"
          && token != "kinematic_invariants" && token != "scalarproduct_rules"
#ifndef KIRAFIREFLY
	  && token != "symbol_to_replace_by_one") {
#else
          && token != "symbol_to_replace_by_one"
          && token != "symbol_order") {
#endif
          std::cout << "Kira does not know the option: " << token << std::endl;
          std::cout << "In config/kinematics.yaml" << std::endl;
          exit(-1);
        }
      }

      if(node["incoming_momenta"]){
        const Node& nodei = node["incoming_momenta"];
        for(unsigned i=0;i<nodei.size();i++) {
          rhs.im.push_back(nodei[i].as<std::string>());
        }
      }
      if(node["outgoing_momenta"]){
        const Node& nodeo = node["outgoing_momenta"];
        for(unsigned i=0;i<nodeo.size();i++) {
          rhs.om.push_back(nodeo[i].as<std::string>());
        }
      }

      if(node["momentum_conservation"]){
        const Node& nodem = node["momentum_conservation"];
        if (nodem.size())
          rhs.mc=make_pair(nodem[0].as<std::string>(),nodem[1].as<std::string>());
      }

      if(node["kinematic_invariants"]){
        const Node& nodek = node["kinematic_invariants"];
        for(unsigned i=0;i<nodek.size();i++) {
          rhs.ki.push_back(std::pair<std::string, int > (nodek[i][0].as<std::string>(), nodek[i][1].as<int>()));

	  std::string invariantsToken = rhs.ki[i].first + "2place";

	  rhs.invariantsPlaceholder.push_back(get_symbol(invariantsToken));

	  rhs.invariantsReplacement.append(get_symbol(rhs.ki[i].first) == get_symbol(invariantsToken)); // inv == inv'

	  rhs.invariantsReplacementRev.append(get_symbol(invariantsToken) == get_symbol(rhs.ki[i].first)); // inv' == inv

        }
      }
      if(node["scalarproduct_rules"]){
        const Node& nodes = node["scalarproduct_rules"];
        for(unsigned i=0;i<nodes.size();i++) {
          std::pair<std::string,std::string> pure(nodes[i][0][0].as<std::string>(),
                                  nodes[i][0][1].as<std::string>());
          rhs.sr.push_back(std::pair<std::pair<std::string,std::string>,std::string>
                          (pure,nodes[i][1].as<std::string>()));
        }
      }

      if(node["symbol_to_replace_by_one"]){
	const Node noderpl=node["symbol_to_replace_by_one"];
	rhs.rpl=noderpl.as<std::string>();
      }

#ifdef KIRAFIREFLY
      // symbol_order can introduce additional symbols which are not kinematic invariants
      if (node["symbol_order"]) {
        const Node& nodei = node["symbol_order"];
        for(unsigned i=0;i<nodei.size();i++) {
          rhs.symbol_order.emplace_back(nodei[i].as<std::string>());
        }
      }
#endif

      return true;
    }
  };

  template<>
  struct convert<Integral_F> {
    static bool decode(const Node& node, Integral_F& rhs) {
      for(YAML::const_iterator it=node.begin(); it != node.end();++it){
        std::string token = (*it).first.as<std::string>();
        if(token != "propagators" && token != "loop_momenta" && token != "name"
	  && token != "top_level_sectors" && token != "magic_relations"
	  && token != "cut_propagators" && token != "permute_propagators"
	  && token != "symbolic_ibp") {

          std::cout << "Kira does not know the option: " << token << std::endl;
          std::cout << "In config/integralfamilies.yaml" << std::endl;
          exit(-1);
        }
      }

      if(!node["propagators"]){
        std::cout << "Option propagators is missing in"
        << "config/integralfamilies.yaml" << std::endl;
        exit(-1);
      }
      if(!node["loop_momenta"]){
        std::cout << "Option loop_momenta is missing in"
        << "config/integralfamilies.yaml" << std::endl;
        exit(-1);
      }
      if(!node["name"]){
        std::cout << "Option name is missing in"
        << "config/integralfamilies.yaml" << std::endl;
        exit(-1);
      }

      rhs.name = node["name"].as<std::string>();

      if (node["magic_relations"])
	rhs.magic_relations = node["magic_relations"].as<std::string>();
      else
	rhs.magic_relations = "false";

      const Node& nodet = node["loop_momenta"];
      for(unsigned i=0;i<nodet.size();i++) {
	rhs.loop.push_back(nodet[i].as<std::string>());
	rhs.allVar.push_back(get_symbol(nodet[i].as<std::string>()));
	rhs.loopVar.push_back(get_symbol(nodet[i].as<std::string>()));
	rhs.loopVarList.append(get_symbol(nodet[i].as<std::string>()));

	std::string loop2 = rhs.loop[i] + "2";
        rhs.loopVarList2.append(get_symbol(loop2));

        rhs.loop2loop2.append(rhs.loopVar[i]==get_symbol(loop2)); // k == k'
	rhs.loop2loop.append(get_symbol(loop2)==rhs.loopVar[i]); // k' == k
      }

      if(node["permute_propagators"]){

        const Node& nodeTop = node["permute_propagators"];

        for(unsigned i = 0; i < nodeTop.size(); i++) {

          rhs.permute.push_back(nodeTop[i].as<int>());
        }
      }

      const Node& nodep = node["propagators"];

      for(unsigned i = 0; i < nodep.size(); i++) {

	unsigned entry = i;
	if(rhs.permute.size() == nodep.size()){
	  entry = rhs.permute[i]-1;
	}

	if (nodep[entry]["bilinear"]) {
	  const Node &bilinear = nodep[entry]["bilinear"];
	  rhs.propagator.push_back( std::pair<std::string, std::string>
				    ( "("+bilinear[0][0].as<std::string>()+")*("+bilinear[0][1].as<std::string>()+")",
				      bilinear[1].as<std::string>() ) );
	} else {
	  rhs.propagator.push_back( std::pair<std::string, std::string >
				    ( nodep[entry][0].as<std::string>(),
				      nodep[entry][1].as<std::string>() ) );
	}
      }

      rhs.jule=static_cast<int>(nodep.size());

      int usedOption = 0;

      if(node["top_level_sectors"]){

        const Node& nodeTop = node["top_level_sectors"];

        for(unsigned i=0; i<nodeTop.size();i++) {

          rhs.topLevelSectors.push_back(nodeTop[i].as<int>());

	  int countBount = 0;

	  for (int jIt=0; jIt<rhs.jule; jIt++){
	    if ( nodeTop[i].as<int>() & 1<<jIt ) {
	      countBount++;
	    }
	  }
	  rhs.bound.push_back(countBount);
        }
        usedOption = 1;
      }

      rhs.maskCut=0;

      if(node["cut_propagators"]){

        const Node& nodeTop = node["cut_propagators"];

        for(unsigned i=0;i<nodeTop.size();i++) {

          rhs.cutProps.push_back(nodeTop[i].as<int>());
          rhs.maskCut+=(1<<(nodeTop[i].as<int>()-1));
        }
      }

      if(usedOption == 0){
	rhs.topLevelSectors.push_back((1<<rhs.jule)-1);
	rhs.bound.push_back(rhs.jule);
      }


      if(rhs.bound.size() > 0)
	rhs.biggestBound = rhs.bound[0];
      else
	rhs.biggestBound = 0;

      for(unsigned itB = 0; itB < rhs.bound.size(); itB++) {
	if(rhs.bound[itB] > rhs.biggestBound)
	  rhs.biggestBound = rhs.bound[itB];
      }

      if(node["symbolic_ibp"]){
	const Node& nodeS = node["symbolic_ibp"];
	for(unsigned i=0; i < nodeS.size(); i++) {

	  rhs.symbolicIBP.push_back((nodeS[i].as<int>())-1);
	}
      }
      return true;
    }
  };

  template<>
  struct convert<IBPIntegral> {
    static bool decode(const Node& node, IBPIntegral& rhs) {

      const Node& noderef = node;

      rhs.id = noderef[0].as<int>();
      rhs.flag2 = noderef[1].as<int>();
      rhs.characteristics[SECTOR] = noderef[2].as<int>();
      rhs.characteristics[DOTS] = noderef[3].as<int>();
      rhs.characteristics[NUM] = noderef[4].as<int>();
      rhs.length = noderef[5].as<int>();
      for(unsigned i = 6; i<noderef.size()-1; i++){
        rhs.indices[i-6] = noderef[i].as<int>();
      }
      rhs.coefficientString = noderef[noderef.size()-1].as<std::string>();
      return true;

    }
  };
}

#endif
