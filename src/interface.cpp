/*
Copyright (C) 2017-2019 Philipp Maierhoefer

This file is part of pyRed.

pyRed is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pyRed is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pyRed.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <fstream>
#include <stack>
#include <stdexcept>
#include <unistd.h> // getopt

#include "config.h"
#include "interface.h"
#include "parser.h"

namespace pyred {

float time_diff(const std::clock_t& start, const std::clock_t& end) {
  return (float(end-start)/CLOCKS_PER_SEC);
}

float time_diff(const std::chrono::time_point<std::chrono::system_clock>& start,
                const std::chrono::time_point<std::chrono::system_clock>& end) {
  return static_cast<std::chrono::duration<float>>(end-start).count();
}

/***************************
 * Logger and LockedLogger *
 ***************************/

Logger::Logger(int cout_verb, int file_verb)
: m_verbosity{std::make_pair(cout_verb,file_verb)}
, m_logfilestream{}
{}

std::pair<int,int> Logger::verbosity(int cout_verb, int file_verb) {
  // The lowest verbosity is -1 (suppress all output).
  // Use file_verb=-2 (default) sets file_verb=cout_verb.
  if (cout_verb < -1) {
    throw input_error("Verbosity level <-1 is not allowed.");
  }
  m_verbosity.first = cout_verb;
  m_verbosity.second = cout_verb;
  if (file_verb >= -1) m_verbosity.second = file_verb;
  return m_verbosity;
}

void Logger::attach_logfile(const std::string &fn,
                    const FileOpenMode mode) {
  if (mode == FileOpenMode::create &&
      keyvaluedb::KeyValueDB::file_exists(fn)) {
    throw input_error("Log file " + fn + " already exists.");
  }
  auto omode = std::ios::trunc;
  if (mode == FileOpenMode::append) {
    omode = std::ios::app;
  }
  m_logfilestream = std::ofstream(fn, omode);
  if (!m_logfilestream.good()) {
    throw input_error("Log file " + fn + " is not writable.");
  }
}

void Logger::detach_logfile() {
  if (m_logfilestream.is_open()) {
    m_logfilestream.close();
  }
}

LockedLogger Logger::operator()(int cout_lev, int file_lev) {
  return LockedLogger(*this, cout_lev, file_lev);
}

LockedLogger::LockedLogger(Logger& lg, int cout_lev, int file_lev)
: m_verbosity{lg.m_verbosity}
, m_level{std::make_pair(cout_lev, file_lev)}
, m_logfilestream{lg.m_logfilestream}
, m_lck{lg.m_mtx}
{
  if (file_lev < 0) m_level.second = cout_lev;
  if (m_level.second < 0) {
    throw input_error("Logger: log level <0 is not allowed.");
  }
  std::cout << std::flush;
}

LockedLogger& LockedLogger::operator<<(std::ostream& (*omanip)(std::ostream&)) {
  if (m_level.first <= m_verbosity.first) std::cout << (*omanip);
  if (m_level.second <= m_verbosity.second && m_logfilestream.is_open()) {
    m_logfilestream << (*omanip);
  }
  return *this;
}

/**********
 * Config *
 **********/

Logger Config::s_log_val{1};
int Config::s_coeff_cls_val{1};
bool Config::s_backward_val{true};
int Config::s_parallel{1};
bool Config::s_auto_symseed{true};
std::pair<int,int> Config::s_symlimits{std::numeric_limits<int>::max(),2};
int Config::s_lookahead{1};
int Config::s_insertion_tracer{1};
std::pair<std::string,bool> Config::s_database_file{"./insertions",false};
bool Config::s_finished{false};
bool Config::s_johanntrick{false};

void Config::coeff_cls(int ccls) {
  if (ccls < 1 || ccls > PYRED_PP_NCOEFFCLASSES) {
    std::ostringstream ss;
    ss << "Invalid coefficient class number (must be 1..."
       << PYRED_PP_NCOEFFCLASSES << "): " << ccls;
    throw init_error(ss.str());
  }
  s_coeff_cls_val = ccls;
}

void Config::backward(bool bkwd) {
  s_backward_val = bkwd;
}

std::pair<int,int> Config::verbosity(int verb, int file_verb) {
  return s_log_val.verbosity(verb, file_verb);
}

void Config::parallel(int n) {
  if (n < 0) n = 1;
  s_parallel = n;
}

void Config::auto_symseed(const bool ass) {
  s_auto_symseed = ass;
}

void Config::symlimits(const int maxdots, const int maxsps) {
  s_symlimits = {maxdots, maxsps};
}

void Config::lookahead(int lah) {
  if (lah < -1 || lah > 2) {
    throw init_error("Config::lookahead may only be initialised to -1,0,1,2.");
  }
  s_lookahead = lah;
}

void Config::insertion_tracer(int mode) {
  if (mode < 0 || mode > 4) {
    throw init_error("Config::insertion_tracer may only be initialised to "
                     "0 (off), 1 (in memory, default), 2 (SQLite), "
                     "3 (Kyoto Cabinet), "
                     "4 (Kyoto Cabinet if available, otherwise SQLite).");
  }
#ifdef PYRED_KCDB
  if (mode == 4) mode = 3;
#else
  if (mode == 4) mode = 2;
  if (mode == 3) {
    throw init_error("To use Config::insertion_tracer=3, pyRed must be built "
                     "with Kyoto Cabinet database support.");
  }
#endif
  s_insertion_tracer = mode;
}

std::pair<std::string,std::string> Config::parse(const std::string& conf) {
  auto tmp = split(conf, ' ');
  tmp.insert(tmp.begin(), ""); // getopt() ignores the first element
  return parse(tmp);
}

std::pair<std::string,std::string> Config::parse(
    const std::vector<std::string>& conf) {
  if (s_finished) {
    throw init_error(
      "Config::parse() error: finalised config cannot be changed anymore.");
  }
  // convert conf to char**
  int argc = conf.size();
  std::vector<char*> argv;
  argv.reserve(argc);
  for (const auto& opt: conf) {
    argv.push_back(const_cast<char*>(opt.c_str()));
  }
  int opt;
  std::string infile{};
  std::string outfile{};
  optind = 1;
  while ((opt = getopt(argc, argv.data(), "1:c:i:o:s:b:t:v:")) != -1) {
    /*
     * -1:string symbol to set to one
     * -c:int use coefficient array of this size
     * -i:string declare symbol (comma separated or multiple -i options)
     * -o:string output file name
     * -s:int integer seed for random number generator
     * -b:int 0/1 to turn off/on backward insertion (default=1)
     */
    int seed;
    switch (opt) {
    case '1':
      CoeffHelper::settoone(std::string(optarg));
      break;
    case 'c':
      try {
        CoeffHelper::coeff_n(string_to_int(std::string(optarg)));
      }
      catch(std::invalid_argument& e) {
        std::ostringstream ss;
        ss << "Option c argument must be integer, but is "
           << optarg << std::endl;
        throw init_error(ss.str());
      }
      coeff_cls(2);
      break;
    case 'i':
      for (const auto& var: split(std::string(optarg), ',')) {
        CoeffHelper::add_invariant(var);
      }
      break;
    case 'o':
      outfile = optarg;
      break;
    case 's':
      try {
        seed = string_to_int(std::string(optarg));
      }
      catch(std::invalid_argument& e) {
        std::ostringstream ss;
        ss << "Seed argument must be integer, but is "
           << optarg << std::endl;
        throw init_error(ss.str());
      }
      CoeffHelper::random_seed(seed);
      break;
    case 'b':
      if (std::string(optarg) == "0") {
        backward(false);
      }
      else if (std::string(optarg) == "1") {
        backward(true);
      }
      else {
        std::ostringstream ss;
        ss << "Option -b must either be 0 or 1, but is "
           << optarg << std::endl;
        throw init_error(ss.str());
      }
      break;
    case 'v':
      try {
        verbosity(string_to_int(std::string(optarg)));
      }
      catch(std::invalid_argument& e) {
        std::ostringstream ss;
        ss << "Verbosity argument must be integer, but is "
           << optarg << std::endl;
        throw init_error(ss.str());
      }
      break;
    default:
      std::ostringstream ss;
      ss << "Invalid argument " << opt << std::endl;
      throw init_error(ss.str());
    }
  }
  if (optind == argc-1) {
    infile = argv[optind];
  }
  else if (optind < argc-1) {
    std::ostringstream ss;
    ss << "Too many arguments" << std::endl;
    throw init_error(ss.str());
  }
  return {infile, outfile};
}

void Config::finish() {
# define PYRED_PP_CCS_CONFIGFINISH(k) \
  PYRED_PP_IF_TRUE(k,else) if (coeff_cls() == k+1) \
  { \
    PYRED_PP_COEFFCLASS(k)::init(); \
  }
  PYRED_PP_REPEAT0(PYRED_PP_NCOEFFCLASSES, PYRED_PP_CCS_CONFIGFINISH)
  s_finished = true;
}

/**********
 * System *
 **********/

System::System()
: sys{}, coeff_cls{Config::coeff_cls()}, m_content_prepared{false}
{}

System::System(const std::vector<eqdata>& eqs)
: sys{eqs}, coeff_cls{Config::coeff_cls()}, m_content_prepared{false}
{}

System::System(std::vector<eqdata>&& eqs)
: sys{std::move(eqs)}, coeff_cls{Config::coeff_cls()}, m_content_prepared{false}
{}

System::System(const std::string& infile)
: sys{}, coeff_cls{Config::coeff_cls()}, m_content_prepared{false}
{
  add(infile);
}

std::size_t System::size() const {
# define PYRED_PP_CCS_SYSTEMSIZE(k) \
  PYRED_PP_IF_TRUE(k,else) if (systemtype() == k+1) \
  { \
    return m_numsys.PYRED_PP_COEFFCLASSMEM(k).size(); \
  }
  PYRED_PP_REPEAT0(PYRED_PP_NCOEFFCLASSES, PYRED_PP_CCS_SYSTEMSIZE)
  else if (systemtype() == 0) {
    return sys.size();
  }
  else { // systemtype() == -1: empty system
    return 0;
  }
}

int System::systemtype(bool fatal_empty) const {
# define PYRED_PP_CCS_NUMSYSEMPTY(k) \
  && m_numsys.PYRED_PP_COEFFCLASSMEM(k).empty()
  if (!sys.empty() // &&
      PYRED_PP_REPEAT0(PYRED_PP_NCOEFFCLASSES, PYRED_PP_CCS_NUMSYSEMPTY)) {
    // String coefficients.
    return 0;
  }
# define PYRED_PP_CCS_NUMSYSNONEMPTY(k) \
  +(m_numsys.PYRED_PP_COEFFCLASSMEM(k).empty()?0:1)
  else if (sys.empty() && (1 == 0 // +
      PYRED_PP_REPEAT0(PYRED_PP_NCOEFFCLASSES, PYRED_PP_CCS_NUMSYSNONEMPTY))) {
    // Exactly one of the numsys is non-empty.
    return coeff_cls;
  }
  else if (sys.empty() // &&
      PYRED_PP_REPEAT0(PYRED_PP_NCOEFFCLASSES, PYRED_PP_CCS_NUMSYSEMPTY)) {
    if (fatal_empty) {
      throw init_error("System is empty.");
    }
    else {
      // Empty system.
      return -1;
    }
  }
  else {
    throw init_error("System mixes different coefficient types.");
  }
}

std::vector<intid> System::generate_solve(
    const std::vector<SeedSpec>& ibp_seedspec,
    const std::vector<SeedSpec>& ibp_seedcompl,
    const std::vector<SeedSpec>& sym_seedspec) {
  if (Integral::sector_ordering() == 1 &&
      (Config::lookahead() == 0 || Config::lookahead() == 1)) {
    // In sector ordering 1, sectors are not ordered by number of lines.
    // Therefore the on-the-fly generator/solver which sorts equations
    // across seed sectors (i.e. lookahead != -1) can only be used with
    // lookahead=2, i.e. the entire system is generated before it is solved.
    throw init_error("System::generate_solve() with sectors ordering 1 "
      "(i.e. by sector number) can only be used with lookahead=-1 "
      "(no sorting across seed sectors) or lookahead=2 "
      "(generate the entire system before sorting).");
  }
  m_is_generated = true;
  m_seeds = std::make_tuple(ibp_seedspec, ibp_seedcompl, sym_seedspec);
  std::vector<intid> indep_eqnums;
# define PYRED_PP_CCS_SYSTEMGENERATESOLVE(k) \
  PYRED_PP_IF_TRUE(k,else) if (coeff_cls == k+1) \
  { \
    indep_eqnums = generate_solve_tmpl<PYRED_PP_COEFFCLASS(k)>( \
      ibp_seedspec, ibp_seedcompl, sym_seedspec); \
  }
  PYRED_PP_REPEAT0(PYRED_PP_NCOEFFCLASSES, PYRED_PP_CCS_SYSTEMGENERATESOLVE)
  if (Config::backward()) {
    backward();
  }
  return indep_eqnums;
}

void System::generate_retrieve(
    const std::vector<SeedSpec>& ibp_seedspec,
    const std::vector<SeedSpec>& ibp_seedcompl,
    const std::vector<SeedSpec>& sym_seedspec,
    const std::function<void(eqdata &&)> &treateq,
    const std::function<std::string(const std::string&)> &treatcoeff) {
  // Generate a system of equation for the given seed, insert coefficients
  // as strings, optionally simplified by treatcoeff,
  // and apply treateq to all equations (i.e. no selection).
# define PYRED_PP_CCS_SYSTEMGENERATERETRIEVE1(k) \
  PYRED_PP_IF_TRUE(k,else) if (coeff_cls == k+1) \
  { \
    GeneratorHelper::generate_and_retrieve<PYRED_PP_COEFFCLASS(k)>( \
      ibp_seedspec, ibp_seedcompl, sym_seedspec, \
      Config::auto_symseed(), {}, treatcoeff, treateq, \
      Config::parallel()); \
  }
  PYRED_PP_REPEAT0(PYRED_PP_NCOEFFCLASSES, PYRED_PP_CCS_SYSTEMGENERATERETRIEVE1)
}

void System::generate_retrieve(
    std::vector<intid> &&eqnums,
    const std::function<void(eqdata &&)> &treateq,
    const std::function<std::string(const std::string&)> &treatcoeff) {
  // Re-generate the system, insert coefficients as strings,
  // optionally simplified by treatcoeff,
  // and apply treateq to the (selected) equations with numbers in eqnums.
  if (!m_is_generated) {
    throw init_error(
      "System::generate_retrieve(): seeds have not been set for re-generation."
    );
  }
  if (eqnums.empty()) {
    return;
  }
# define PYRED_PP_CCS_SYSTEMGENERATERETRIEVE2(k) \
  PYRED_PP_IF_TRUE(k,else) if (coeff_cls == k+1) \
  { \
    GeneratorHelper::generate_and_retrieve<PYRED_PP_COEFFCLASS(k)>( \
      std::get<0>(m_seeds), std::get<1>(m_seeds), std::get<2>(m_seeds), \
      Config::auto_symseed(), std::move(eqnums), treatcoeff, treateq, \
      Config::parallel()); \
  }
  PYRED_PP_REPEAT0(PYRED_PP_NCOEFFCLASSES, PYRED_PP_CCS_SYSTEMGENERATERETRIEVE2)
}

void System::reserve(const std::size_t sz) {
  sys.reserve(sz);
}

void System::add(const eqdata& eq) {
  sys.push_back(eq);
}

void System::add(eqdata&& eq) {
  sys.push_back(std::move(eq));
}

void System::add(const std::vector<eqdata>& eqs){
  reserve(sys.size() + eqs.size());
  for (const auto& eq: eqs) {
    sys.push_back(eq);
  }
}

void System::add(std::vector<eqdata>&& eqs){
  reserve(sys.size() + eqs.size());
  for (auto& eq: eqs) {
    sys.push_back(std::move(eq));
  }
}

void System::add_forward(const eqdata& eq, intid neq) {
# define PYRED_PP_CCS_SYSTEMADDFORWARD(k) \
  PYRED_PP_IF_TRUE(k,else) if (coeff_cls == k+1) \
  { \
    add_forward_tmpl<PYRED_PP_COEFFCLASS(k)>( \
      eq, get_numsys<PYRED_PP_COEFFCLASS(k)>(), \
      m_sols.PYRED_PP_COEFFCLASSMEM(k), neq); \
  }
  PYRED_PP_REPEAT0(PYRED_PP_NCOEFFCLASSES, PYRED_PP_CCS_SYSTEMADDFORWARD)
}

void System::add(const std::string& infile) {
  Config::log(1) << "import equations" << std::flush;
  auto starttime = std::clock();
  std::string line;
  eqdata tmpeq;
  if (infile.empty()) {
    // read equations from stdin
    while (std::getline(std::cin, line)) {
      if (line.empty() && !tmpeq.empty()) {
        sys.push_back(std::move(tmpeq));
        tmpeq.clear();
      }
      else {
        auto wc = split_intcoeff(line);
        if (wc.first != Integral::zero_weight) {
          tmpeq.push_back(std::move(wc));
        }
      }
    }
    if (!tmpeq.empty()) {
      sys.push_back(std::move(tmpeq));
    }
  }
  else {
    // import equations from file
    std::ifstream instream(infile);
    if (!instream.good()) {
      std::ostringstream ss;
      ss << "failed reading file \"" << infile << "\"" << std::endl;
      throw input_error(ss.str());
    }
    while (std::getline(instream, line)) {
      if (line.empty() && !tmpeq.empty()) {
        sys.push_back(std::move(tmpeq));
        tmpeq.clear();
      }
      else {
        auto wc = split_intcoeff(line);
        if (wc.first != Integral::zero_weight) {
          tmpeq.push_back(std::move(wc));
        }
      }
    }
  }
  if (!tmpeq.empty()) {
    sys.push_back(std::move(tmpeq));
  }
  auto endtime = std::clock();
  Config::log(1) << ":   " << time_diff(starttime, endtime) << std::endl;
}

std::vector<intid> System::solve() {
  std::vector<intid> indep_eqnums;
# define PYRED_PP_CCS_SYSTEMSOLVE(k) \
  PYRED_PP_IF_TRUE(k,else) if (coeff_cls == k+1) \
  { \
    indep_eqnums = solve_tmpl<PYRED_PP_COEFFCLASS(k)>(); \
  }
  PYRED_PP_REPEAT0(PYRED_PP_NCOEFFCLASSES, PYRED_PP_CCS_SYSTEMSOLVE)
  return indep_eqnums;
}

void System::backward() {
# define PYRED_PP_CCS_SYSTEMBACKWARD(k) \
  PYRED_PP_IF_TRUE(k,else) if (coeff_cls == k+1) \
  { \
    m_sols.PYRED_PP_COEFFCLASSMEM(k).clear(); \
    backward_tmpl<PYRED_PP_COEFFCLASS(k)>( \
      get_numsys<PYRED_PP_COEFFCLASS(k)>()); \
  }
  PYRED_PP_REPEAT0(PYRED_PP_NCOEFFCLASSES, PYRED_PP_CCS_SYSTEMBACKWARD)
}

std::vector<intid> System::independent() {
  std::vector<intid> indep_eqnums;
  std::size_t nequations{0};
# define PYRED_PP_CCS_SYSTEMINDEPENDENT(k) \
  PYRED_PP_IF_TRUE(k,else) if (coeff_cls == k+1) \
  { \
    auto &numsys = get_numsys<PYRED_PP_COEFFCLASS(k)>(); \
    nequations = numsys.size(); \
    indep_eqnums = numsys.independent(); \
  }
  PYRED_PP_REPEAT0(PYRED_PP_NCOEFFCLASSES, PYRED_PP_CCS_SYSTEMINDEPENDENT)
  auto neqs_indep = indep_eqnums.size();
  Config::log(1) << std::setw(8) << nequations << " equations: "
                 << nequations-neqs_indep << " zero + "
                 << neqs_indep << " independent" << std::endl;
  return indep_eqnums;
}

std::unique_ptr<keyvaluedb::KeyValueDB> &System::get_db() {
# define PYRED_PP_CCS_SYSTEMGETDB(k) \
  PYRED_PP_IF_TRUE(k,else) if (coeff_cls == k+1) \
  { \
    return get_numsys<PYRED_PP_COEFFCLASS(k)>().get_db(); \
  }
  PYRED_PP_REPEAT0(PYRED_PP_NCOEFFCLASSES, PYRED_PP_CCS_SYSTEMGETDB)
  else {
    // Avoid compiler warning
    throw init_error("Oops! This should never happen.");
  }
}

std::pair<std::vector<intid>,std::vector<intid>> System::select(
  const std::vector<intid>& mandatory, const std::vector<intid>& if_reduced) {
  /*
   * 'mandatory':
   * - Vector of integrals for which a reduction formula is required.
   * 'if_reduced':
   * - Vector of integrals to reduce, if there is a reduction formula known.
   * return: pair(neededeqs, unreduced)
   * 'neededeqs'
   * - The numbers of all equations which are needed to reduce integrals
   *   from 'mandatory' and 'if_reduced'.
   * 'unreduced':
   * - All integrals which appear in reduction formulas of mandatory integrals.
   * - All mandatory integrals for which there is no reduction formula known.
   */
  if (!m_content_prepared) {
    throw init_error("Content map has not been prepared for selection. "
                     "The system must be solved with backward insertion.");
  }
  // Stack of untreated integrals for which the dependencies must be looked up.
  std::stack<intid,std::vector<intid>> intstack;
  std::unordered_set<intid> neededset;
  std::unordered_set<intid> mandatoryset;
  // Set of integrals which are regarded as master integrals.
  std::unordered_set<intid> unreducedset;
  for (const auto iid: mandatory) {
    if (iid) neededset.insert(iid);
  }
  mandatoryset = neededset;
  for (const auto iid: if_reduced) {
    if (iid) neededset.insert(iid);
  }
  for (const auto iid: neededset) intstack.push(iid);
  while (!intstack.empty()) {
    auto next = intstack.top();
    intstack.pop();
    const auto cntit = m_content.find(next);
    if (cntit != m_content.cend()) {
      // A reduction formula is known.
      // Depends on equations with the numbers in the respective 'insertions'.
      // Get them from the insertions database.
      auto deps = get_db()->get(cntit->second.first/*eqnum*/, false);
      for (const auto& dep: deps) {
        // For each integral on which 'next' depends:
        // add it to the set of needed integrals.
        // If it was not already in the set, push it onto the stack.
        auto isnew = neededset.insert(dep).second;
        if (isnew) intstack.push(dep);
      }
      if (mandatoryset.find(next) != mandatoryset.cend()) {
        // If 'next' is in 'mandatory', treat integral content masters.
        for (const auto& iid: cntit->second.second/*unreduced*/) {
          unreducedset.insert(iid);
        }
      }
    }
    else {
      // No reduction formula is known.
      // Dependencies always correspond to (inserted) equations,
      // i.e. this branch is only reached for unreduced integrals
      // in 'mandatory' and 'if_reduced'.
      if (mandatoryset.find(next) != mandatoryset.cend()) {
        // 'next' is in 'mandatory', but there is no reduction formula.
        // -> Regard it as a master integral.
        unreducedset.insert(next);
      }
      else {
        // 'next' is in 'if_reduced', but there is no reduction formula.
        // -> Remove it from neededset. It won't be added again,
        //    because it was never inserted anywhere.
        // Do not regard it as a master integral.
        neededset.erase(next);
      }
    }
  }
  // For each integral id in 'neededset':
  // push the corresponding equation number on 'neededeqs',
  // if a reduction formula exists.
  std::vector<intid> neededeqs;
  neededeqs.reserve(neededset.size());
  for (const auto iid: neededset) {
    const auto cntit = m_content.find(iid);
    if (cntit != m_content.cend()) {
      neededeqs.push_back(cntit->second.first/*eqnum*/);
    }
  }
  std::sort(neededeqs.begin(), neededeqs.end());
  // Convert 'unreducedset' to a vector.
  std::vector<intid> unreduced;
  for (const auto iid: unreducedset) unreduced.push_back(iid);
  std::sort(unreduced.begin(), unreduced.end());
  return {neededeqs, unreduced};
}

std::vector<eqdata> System::retrieve(std::vector<intid> &&eqnums) {
  if (sys.empty()) {
    throw init_error("System::retrieve(): empty system.");
  }
  if (eqnums.empty()) {
    return {};
  }
  std::sort(eqnums.begin(), eqnums.end());
  std::vector<eqdata> result;
  intid neq = 0;
  auto selit = eqnums.begin();
  for (auto &eq: sys) {
    if (neq == *selit) {
      result.push_back(std::move(eq));
      ++selit;
    }
    eq.clear();
    ++neq;
  }
  sys.clear();
  return result;
}

#ifdef KIRAFIREFLY
std::vector<firefly::FFInt> System::solve(const std::vector<std::pair<std::string, uint64_t>>& randvars, const std::set<uint64_t>& mandatory, std::vector<std::pair<uint64_t, uint64_t>>& assignment, std::unordered_map<uint64_t, int>& equation_lengths, std::vector<double>& times) const {
  auto time0 = std::chrono::high_resolution_clock::now();

  auto numsys = SystemOfEqs<firefly::FFInt>(sys, randvars);

  auto time1 = std::chrono::high_resolution_clock::now();

  numsys.solve();
  numsys.truncate();

  auto time2 = std::chrono::high_resolution_clock::now();

  std::vector<firefly::FFInt> result = backward(numsys, mandatory, assignment, equation_lengths);

  auto time3 = std::chrono::high_resolution_clock::now();

  times = {std::chrono::duration<double>(time1 - time0).count(), std::chrono::duration<double>(time2 - time1).count(), std::chrono::duration<double>(time3 - time2).count()};

  return result;
}

std::vector<firefly::FFInt> System::backward(SystemOfEqs<firefly::FFInt>& numsys, const std::set<uint64_t>& mandatory, std::vector<std::pair<uint64_t, uint64_t>>& assignment, std::unordered_map<uint64_t, int>& equation_lengths) const {
  if (!mandatory.empty()) {
    std::set<uint64_t> mandatory_tmp = mandatory;
    std::vector<Equation<firefly::FFInt>> new_sys_reverse;

    for (auto it = numsys.sys.rbegin(); it != numsys.sys.rend(); ++it) {
      auto found = mandatory_tmp.find(it->front().first);

      if (found != mandatory_tmp.end()) {
        for (auto itt = ++(it->eq.begin()); itt != it->eq.end(); ++itt) {
          mandatory_tmp.insert(itt->first);
        }

        new_sys_reverse.emplace_back(std::move(*it));
      }
    }

    numsys.sys.clear();
    numsys.sys.shrink_to_fit();
    numsys.sys.reserve(new_sys_reverse.size());

    for (auto it = new_sys_reverse.rbegin(); it != new_sys_reverse.rend(); ++it) {
      numsys.sys.emplace_back(std::move(*it));
    }
  }

  numsys.solve();

  std::vector<firefly::FFInt> result {};

  if (!mandatory.empty()) {
    for (auto it = numsys.sys.rbegin(); it != numsys.sys.rend(); ++it) {
      auto found = mandatory.find(it->front().first);

      if (found != mandatory.end()) {
        equation_lengths.emplace(std::make_pair(it->front().first, it->size()));

        for (auto itt = ++(it->eq.begin()); itt != it->eq.end(); ++itt) {
          assignment.emplace_back(std::make_pair(it->front().first, itt->first));
          result.emplace_back(itt->second);
        }

      }
      it->eq.clear();
    }
  } else {
    for (auto it = numsys.sys.rbegin(); it != numsys.sys.rend(); ++it) {
      equation_lengths.emplace(std::make_pair(it->front().first, it->size()));

      for (auto itt = ++(it->eq.begin()); itt != it->eq.end(); ++itt) {
        assignment.emplace_back(std::make_pair(it->front().first, itt->first));
        result.emplace_back(itt->second);
      }

      it->eq.clear();
    }
  }

  numsys.sys.clear();

  return result;
}
#endif

}
