/* This file is a part of the program Kira.
 * Copyright (C) Johann Usovitsch <jusovitsch@googlemail.com>
 * Philipp Maierhoefer <particle@maierhoefer.net>
 * Peter Uwer <peter.uwer@physik.hu-berlin.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version , or (at
 * your option) any later version as published by the Free Software 
 * Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
*/
// #include <sstream>
// #include <fstream>
// #include <cmath>
// #include <algorithm>
// #include "integral.h"
// #include "sort_rules.h"
// #include "tools.h"
// #include "sqlite3/sqlite3.h"
// #include "dataBase.h"
// #include "createSeeds.h"
// #include <sys/stat.h>
// #include <kira.h>
// 
// #include "defs.h"
// #include "parser.h"
// #include "integrals.h"
// #include "relations.h"
// #include "coeff_int.h"
// #include "interface.h"
// 
// #include <algorithm>
// 
// using namespace pyred;
// 
// static Loginfo & logger = Loginfo::instance();
// 
// using namespace std;
// 
// typedef std::vector< symmetries > SYM;
// typedef SYM::iterator ItSYM;
// 
// 
// SeedsKira::SeedsKira(Kira& kira_, int nOfTopology_) {
//   
//   kira=&kira_;
//   
//   nOfTopology = nOfTopology_;
//   
//   aIBPint = new vector<std::uint64_t>* [nOfTopology];
// 
//   nSector = new int [nOfTopology];
//   mappingSector = new int * [nOfTopology];
//   mappingSectorReverse = new int * [nOfTopology];
//   
//   for(int i = 0; i < nOfTopology; i++){
//     
//     nSector[i] = 0;
//     
//     string topoName = kira->collectReductions[i];
//     
//     mappingSector[i] = new int [(1<<kira->integralfamily.jule)];
//     mappingSectorReverse[i] = new int [(1<<kira->integralfamily.jule)];
//     int countSec=0;
//     
//     for (int j = 0; j < kira->biggestBound+1; j++){
// 
//       nSector[i] += static_cast<int>(kira->topology[ topoName ].mask[j].size());
//     
//       for (unsigned k = 0; k < kira->topology[ topoName ].mask[j].size(); k++){
// 	
// 	mappingSectorReverse[i][ countSec ] = kira->topology[ topoName ].mask[j][k];
//         mappingSector[i][ kira->topology[ topoName ].mask[j][k] ] = countSec++;
//       }
//     }
//     aIBPint[i] = new vector<std::uint64_t> [nSector[i]];
//   }
//   
//   den_max=kira->den[0];
//   num_max=kira->num[0];
//   num_max_sym=2;
//   
//   for(unsigned int i=0;i<kira->den.size();i++){
//     
//     int tokenDen = kira->den[i];
//     
//     if(kira->denTPlus[i])
//       tokenDen+=kira->biggestBound;
//     
//     if (den_max<tokenDen)
//       den_max=tokenDen;
//     
//     if (num_max<kira->num[i])
//       num_max=kira->num[i];
//     
//   }
// }
// 
// SeedsKira::~SeedsKira(){
//   
//   for(int i = 0; i < nOfTopology; i++){
//     
//     delete [] aIBPint[i];
//     delete [] mappingSector[i];
//     delete [] mappingSectorReverse[i];
//   }
// 
//   delete [] aIBPint;
//   delete [] mappingSector;
//   delete [] mappingSectorReverse;
//   delete [] nSector;
// }
// 
// void SeedsKira::gen_int(int n, int **array, int maxI, int option){
//   
//   Vint initiate, temporary;
//   VVint indices, indices2;
//   pair<set<Vint>::iterator, bool> ret;
//   set<Vint > testSet;
//   int length, count=0, a, init;
//   
//   if (option==1){ // denominator/numerator
//     a=+1;
//     init=1;
//   } else {
//     a=-1;
//     init=0;
//   }
//   
//   for (int i3=0; i3<maxI; i3++){
//     if(i3==0){
//       for(int i=0;i<n;i++){
//         initiate.push_back(init);
//       }
//       indices.push_back(initiate);
//     } else {
//       for (ItVVint i2=indices.begin();i2<indices.end();++i2){
//         for(int i=0;i<n;i++){
//           temporary = *i2;
//           temporary[i]=temporary[i]+a;
//           sort(temporary.begin(),temporary.end());
//           ret=testSet.insert(temporary);
//           if(ret.second){
//             do {
//               indices2.push_back(temporary);
//             } while (next_permutation(temporary.begin(),temporary.end()) );
//           }
//           temporary.clear();
//         }
//       }
//       indices.clear();
//       indices=indices2;
//       indices2.clear();
//     }
//     length = static_cast<int>(indices.size());
//     count=0;
//     array[i3] = new int [length*n+2];
//     array[i3][count++]=length*n+2;
//     array[i3][count++]=maxI;
//     for(int i=0;i<length;i++){
//       for(int j=0;j<n;j++){
//         array[i3][count++]=(indices[i])[j];
//       }
//     }
//   }
// }
// 
// void SeedsKira::fusion(int j, int iS2, int topology, int noIBP, int symDOTS, int symNUMS){
//   
//   int k=0;
//   
//   do{
//     int outRange = 0;
//     int outRangeTT = 0;
//     int k2=0;
//     do{
//       
//       outRange=1;
//       
//       for (size_t itK=0; itK<kira->den.size();itK++) {
// 	
// 	int tokenDen=k+j;
// 	if(kira->denTPlus[itK])
// 	  tokenDen-=j;
// 	
// 	if (tokenDen <= kira->den[itK] && k2 <= num_max) {
// 	
// 	  outRange = 0;
// 	  break;
// 	}
//       }
//       
//       outRangeTT = 1;
//       
//       for (size_t itK=0; itK<kira->den.size();itK++) {
// 	
// 	int tokenDen=k+j;
// 	if(kira->denTPlus[itK])
// 	  tokenDen-=kira->biggestBound;
// 	
// 	if (tokenDen <= kira->den[itK] && k2 <= num_max) {
// 	
// 	  outRangeTT = 0;
// 	  break;
// 	}
//       }
//       
//       int i=2;
//       do{
// 	int i2=2;
// 	int it=i;
// 	do{
// 
// 	  it=i;
//           
//           vector<int> pows;
//           pows.reserve(kira->integralfamily.jule);
// 
// 	  for (int ik=0;ik<kira->integralfamily.jule;ik++){
// 	    if (((iS2) & 1<<ik)) {
//               pows.push_back(dotsMaster[j][k][it++]);
// 	    }
// 	    else{
//               pows.push_back(scalMaster[j][k2][i2++]);
// 	    }
// 	  }
// 	  
// 	  auto igl = pyred::Integral( topology, std::move(pows) );
//           
// 	  std::uint64_t ID = igl.to_weight();
//           
// 	  int secmap = mappingSector[topology][iS2];
//           
//           int flag = 0;
//           
//           if (outRangeTT){
//             flag = -1;
//           }
//           
//           auto mapContent = integralMap.find(ID);
//           
//           if ( mapContent != integralMap.end() ) {
//             
//             if(get<2>(mapContent->second) == -1 && flag == 0){
//               
//               integralMap.erase(ID);
//               integralMap.insert(pair<std::uint64_t, tuple<int, int, int> > (ID, make_tuple(iS2, topology, flag)));
//             }
//             else
//               continue;
//           }
//           else
//             integralMap.insert(pair<std::uint64_t, tuple<int, int, int> > (ID, make_tuple(iS2, topology, flag)));
//           
// 	  if (outRange == 0){
//             aIBPint[topology][secmap].push_back(ID);
// 	  }
// 	  
// 	} while (i2<scalMaster[j][k2][0]);
// 	i=it;
//       } while (i<dotsMaster[j][k][0]);
//       k2++;
//     } while (k2<scalMaster[j][0][1]);
//     k++;
//   } while (k<dotsMaster[j][0][1]);
// }
// 
// void SeedsKira::generate_dots(){
//   
// /*generate integer values for propagators in the denominator*/
//   int length=0;
//   for (int j=0; j<kira->biggestBound+1;j++){
//     if (j==0){
//       length = 1;
//       dotsMaster[j] = new int*[length];
//       gen_int(j,dotsMaster[j],length,1);
//     }
//     else if (0<(den_max-j+1)){
//       length = (den_max-j+1);
//       dotsMaster[j] = new int*[length];
//       gen_int(j,dotsMaster[j],length,1);
//     }
//   }
// };
// 
// void SeedsKira::generate_nums()
// {
// /*generate integer values for propagators in the numerator*/
//   int length=0;
//   for (int j=0;j<kira->biggestBound+1;j++){
//     if (j==kira->integralfamily.jule)
//       length = 1;
//     else
//       length = num_max+1;
//     scalMaster[kira->biggestBound-j] = new int*[length];
//     gen_int((kira->integralfamily.jule)-(kira->biggestBound)+j,scalMaster[kira->biggestBound-j],length,0);
//   }
// };
// 
// 
// void SeedsKira::create_seeds() {
//   
//   /*generate all possible SeedsKira in the range [t,den_max+1] and [num_min,num_max]*/
//   for (int j=0;j<kira->biggestBound+1;j++){
//     
//     for (ItVint it=kira->integralfamily.allowSector[j].begin(); it!=kira->integralfamily.allowSector[j].end();it++){
// 
//       Vint secTopo;
//       pair<set<Vint>::iterator, bool> ret;
//       
//       secTopo.push_back(*it);
//       secTopo.push_back(kira->integralfamily.topology);
//       
//       ret=testSet.insert(secTopo);
//       
//       if(ret.second){
//         
//         int noIBP = 0;
// //       int symNUMS = 0;
// //       int symDOTS = 0;
// //       
// //       if(kira->integralfamily.symVec[*it].size()>0){
// //         
// //         for(vector<symmetries>::iterator symHandler = kira->integralfamily.symVec[*it].begin(); 
// //           symHandler!=kira->integralfamily.symVec[*it].end(); symHandler++){
// //           if(symHandler->symDOTS)
// //             symDOTS = 1;
// //           else
// //             symNUMS = 1;
// //         }
// //       }
// //       if(kira->integralfamily.relVec[*it].size()>0){
// //         
// //         for(vector<symmetries>::iterator symHandler = kira->integralfamily.relVec[*it].begin(); 
// //             symHandler!=kira->integralfamily.relVec[*it].end(); symHandler++){
// //           
// //           if(symHandler->symDOTS)
// //             symDOTS = 1;
// //           else
// //             symNUMS = 1;
// //         }
// //       }
//         fusion(j, *it, kira->integralfamily.topology, noIBP, 1/*symDOTS*/, 1/*symNUMS*/);
//       }
//     }
//     load_bar(j+1,kira->biggestBound+1,50,50);
//   }  
// };
// 
// void SeedsKira::create_symmetry_seeds(){
//   
//   /*generate all possible SeedsKira in the range [t,den_max+1] and [num_min,num_max]*/
//   vector<symmetries> lastFusion;
//   vector<symmetries> lastFusionB;
//   for (int j = 0; j < kira->biggestBound+1; j++){
//     for (ItVint it=kira->integralfamily.allowSector[j].begin(); it!=kira->integralfamily.allowSector[j].end();it++){
//       for (ItSYM itt=kira->integralfamily.symVec[*it].begin(); itt!=kira->integralfamily.symVec[*it].end();itt++){
// //         int symNUMS = 0;
// //         int symDOTS = 0;
// //         
// //         if(itt->symDOTS)
// //           symDOTS = 1;
// //         else
// //           symNUMS = 1;
//                 
//         Vint secTopo;
//         pair<set<Vint>::iterator, bool> ret;
//         
//         secTopo.push_back((*itt).sector);
//         secTopo.push_back((*itt).topology);
//         
//         ret=testSet.insert(secTopo);
//         
//         if(ret.second){
//           
//           fusion((*itt).nOfProps, (*itt).sector, (*itt).topology, 0, 1/*symDOTS*/, 1/*symNUMS*/);
// 	
//           lastFusion.push_back(*itt);
//         }
// 	break;
//       }
//     }
//     load_bar(j+1,kira->biggestBound+1,50,50);
//   }
// 
//   lastFusionB=lastFusion;
// 
//   for (vector<symmetries>::iterator it = lastFusionB.begin(); it < lastFusionB.end(); it++) {
//     
//     string topoName = kira->collectReductions[(*it).topology];
//     
//     for (ItSYM itt=kira->topology[ topoName ].symVec[(*it).sector].begin(); itt!=kira->topology[ topoName ].symVec[(*it).sector].end();itt++){
// //       int symNUMS = 0;
// //       int symDOTS = 0;
// //         
// //       if(itt->symDOTS)
// //         symDOTS = 1;
// //       else
// //         symNUMS = 1;
//       Vint secTopo;
//       pair<set<Vint>::iterator, bool> ret;
//       
//       secTopo.push_back((*itt).sector);
//       secTopo.push_back((*itt).topology);
//       
//       ret=testSet.insert(secTopo);
//       
//       if(ret.second){
//        
//         fusion((*itt).nOfProps, (*itt).sector, (*itt).topology, 0, 1/*symDOTS*/, 1/*symNUMS*/);
//         
//         lastFusion.push_back(*itt);
//       }
//       break;
//     }
//   }
//   
//   //  This routine generates IBP for lower sectors for the collected sectors in lastFusion. 
//   for (vector<symmetries>::iterator it = lastFusion.begin(); it < lastFusion.end(); it++) {
// 
//     if ((*it).nOfProps == 0)
//       continue;
//     
//     string topoName = kira->collectReductions[(*it).topology];
//     
//     for(int propsIT = 0; propsIT < (*it).nOfProps; propsIT++){
//       const vector<int> *mask = &(kira->topology[ topoName ].mask[propsIT]);
//       
//       if(mask->size() == 0 ) 
//         continue;
//       
//       for (unsigned itt = 0; itt < mask->size(); itt++) {
// 
//         if( ( (*mask)[itt] & (*it).sector ) != (*mask)[itt] )
//           continue;
//         Vint secTopo;
//         pair<set<Vint>::iterator, bool> ret;
//         
//         secTopo.push_back((*mask)[itt]);
//         secTopo.push_back((*it).topology);
//         
//         ret=testSet.insert(secTopo);
//         
//         if(ret.second){
//           fusion(propsIT, (*mask)[itt], (*it).topology, 0, 1, 1);
//         }
//       }
//     }
//   }
// } 
// 
// int SeedsKira::save() {
//   
//   Clock clock;
//   
//   int incrementID = 0;
//   
//   mkdir((kira->outputDir+"/results/"+kira->integralfamily.name).c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
//   
//   ofstream id2int;
//   if(kira->dataFile == 1)
//     id2int.open((kira->outputDir+"/results/"+kira->integralfamily.name+"/id2int").c_str());
//   
//   logger << "\n*****Write seeds to hard disk***************************\n";
//   
//   DataBase database((kira->outputDir+"/results/"+kira->integralfamily.name+"/kira.db").c_str());
//   if(kira->dataBase == 1){
//     logger << (kira->outputDir+"/results/"+kira->integralfamily.name+"/kira.db").c_str() << "\n";
//     database.create_integral_table(kira->integralfamily.jule);
//     database.prepare_integral();
//     database.begin_transaction();  
//   }
//   for(int i = 0; i < nOfTopology; i++){
//     
//     for(int j = 0; j < nSector[i]; j++){
//       for  (unsigned it = 0; it < aIBPint[i][j].size(); it++) {
//         SEEDIntegral integralSeed(kira->integralfamily.jule);
//         
//         auto iglback = pyred::Integral(aIBPint[i][j][it]);
//         auto property = iglback.properties(aIBPint[i][j][it]);
//         integralSeed.id = aIBPint[i][j][it];
//         
//         for(int itt = 0; itt < kira->integralfamily.jule; itt++){
//           integralSeed.indices[itt] = iglback.m_powers[itt];
//         }
//         integralSeed.characteristics[SECTOR] = property.sector;
//         integralSeed.characteristics[TOPOLOGY] = property.topology;
//         integralSeed.characteristics[DENCOUNT] = property.lines;
//         integralSeed.characteristics[DOTS] = property.dots;
//         integralSeed.characteristics[NUM] = property.sps;
//         integralSeed.flag2 = 0;
//         
// 	if(kira->dataFile == 1)
//           id2int << "- [" << integralSeed << "]" << endl;
// 	if(kira->dataBase == 1)
// 	  database.bind_integral(integralSeed, kira->integralfamily.jule);
// 	
//         auto mapContent = kira->mastersMap.find(integralSeed.id);
//     
//         if(mapContent != kira->mastersMap.end()){
//           integralSeed.id=mapContent->second;
//         }
//         
//         incrementID++;
//       }
//       
//     }
//   }
//   
//   if(kira->dataFile == 1)
//     id2int.close();
//   
//   if(kira->dataBase == 1)
//     database.commit_transaction();
//   
//   logger << "\n( "<<clock.eval_time()<<" s )\n";
//   
//   if(incrementID==0) {
//     logger << "No seeds were generated.\n"; 
//     return 0;
//   }
//   return 1;
// }
// 
// void SeedsKira::prepare_seeds() {
//   
//   Clock clock;
//   
//   logger << "\n*****Generate seed integrals*******************************\n";
//   
//   dotsMaster = new int**[kira->biggestBound+1];
//   scalMaster = new int**[kira->biggestBound+1];
//   
//   generate_dots();
//   generate_nums();
//   create_seeds();
// 
//   
//   
//   /*cleanup*/
//   for(int i = 0; i < kira->biggestBound+1; ++i){
//     for(int j = 0, sec = scalMaster[i][0][1]; j < sec; ++j){
//       delete [] scalMaster[i][j];
//     }
//     delete [] scalMaster[i];
//   }
//   delete [] scalMaster;
//   
//   for(int i = 0; i < kira->biggestBound+1; ++i){
//     for(int j = 0, sec = dotsMaster[i][0][1]; j < sec; ++j){
//       delete [] dotsMaster[i][j];
//     }
//     delete [] dotsMaster[i];
//   }
//   delete [] dotsMaster;
//   
//   
//   
//   
//   logger << "\n*****Generate seed symmetry integrals**********************\n";
// 
//   if(num_max>1)
//     num_max=num_max_sym; //num_max_sym = 2;
//   
//   dotsMaster = new int**[kira->biggestBound+1];
//   scalMaster = new int**[kira->biggestBound+1];
//   
//   
//   generate_dots();
//   generate_nums();
//   create_symmetry_seeds();
// 
//   
//   /*cleanup*/
//   for(int i = 0; i < kira->biggestBound+1; ++i){
//     for(int j = 0, sec = scalMaster[i][0][1]; j < sec; ++j){
//       delete [] scalMaster[i][j];
//     }
//     delete [] scalMaster[i];
//   }
//   delete [] scalMaster;
//   
//   for(int i = 0; i < kira->biggestBound+1; ++i){
//     for(int j = 0, sec = dotsMaster[i][0][1]; j < sec; ++j){
//       delete [] dotsMaster[i][j];
//     }
//     delete [] dotsMaster[i];
//   }
//   delete [] dotsMaster;
//   
//   /*sort new generated seeds*/
//   sort_rules sorts(kira->integralfamily.jule);
//   equal_rules equals(kira->integralfamily.jule);
//   for(int i = 0; i < nOfTopology; i++){
//     
//     for(int j = 0; j < nSector[i]; j++){
//       std::sort(aIBPint[i][j].begin(),aIBPint[i][j].end());
//       aIBPint[i][j].resize(distance(aIBPint[i][j].begin(),
// 				unique(aIBPint[i][j].begin(), aIBPint[i][j].end())) );
//     }
//   }
//   
//   logger << "\n( "<<clock.eval_time()<<" s )\n";
//   
// }
