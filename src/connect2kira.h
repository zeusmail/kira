/* This is the wrapper to the program FERMAT
 * http://www.bway.net/~lewis
 * This file is a part of the program gateToFermat.
 * Copyright (C) Mikhail Tentyukov <tentukov@physik.uni-bielefeld.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version, or (at
 * your option) any later version as published by the Free Software 
 * Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * Modified by Johann Usovitsch April, 2017
*/

#ifndef CONNECT2KIRA_H
#define CONNECT2KIRA_H
#include "ginac/ginac.h"
#include "integral.h"
#include <string>
#include <vector>

#include <signal.h>
#define DELTA_OUT 134217728
// 268435456
// 536870912
// 1073741824
// 1024
#define FROMFERMATBUFSIZE 2048

class Connect2Kira {
 public:
 Connect2Kira() : g_to(0), g_from(0) {};
  void pipe_kira();
  void close_pipe();
  void setup(std::vector<std::string> & args);

  ~Connect2Kira(){
    for (size_t i = 0; i < argvLength-1; i++){
      delete [] argv[i];
    }
    delete [] argv;
  }

protected:
  unsigned int argvLength;
  char **argv;
  int fdin[2], fdout[2];
  size_t length;
  pid_t  g_childpid;
  int status;
  FILE *g_to, *g_from;
};

class Perl2Kira : public Connect2Kira {
 public:
  Perl2Kira(const std::vector<GiNaC::possymbol> &invar,GiNaC::ex massONE,
	    const std::vector<int>& invarDIM,int DIM);
  Perl2Kira();
  ~Perl2Kira(){};
  void read_pipe(std::string& output);
  void put_pipe(std::string& input);
};

/* These functions are originaly taken from the program gateToFermat
 * And here is the original comment:
 * This is the wrapper to the program FERMAT
 * http://www.bway.net/~lewis
 * This file is a part of the program gateToFermat.
 * Copyright (C) Mikhail Tentyukov <tentukov@physik.uni-bielefeld.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
*/
class Fermat : public Connect2Kira {
public:
  Fermat(){
    g_fullout=NULL;
    g_stopout=NULL;
    g_baseout=NULL;
  };
  ~Fermat();
  void start_fermat(std::string &fermatPath,char *pvars);
  void set_variable(char *pvars);
  void unset_variable(char *pvars);
  void set_numeric (char* pvars, int numeric);
  void unset_numeric (char* pvars);
  void fermat_collect(char *buffer);
  int fermat_calc(int optional = 0);
  /*The output buffer (baseout)*/
  char *g_baseout;
private:
  char* pvars;
  void close_calc(int mustCleanup);
  /*The inline function places one char to the output buffer with possible 
  expansion of the buffer:*/
  void add_to_out(char ch);
/*reads the stream 'from' up to the line 'terminator' (only 'thesize' first 
  characters are compared):*/
  void read_up(char *terminator, int thesize);
  /*Starts Fermat and makes some initializations:*/
  char g_fbuf[FROMFERMATBUFSIZE];
  /*stopout points to the end of allocated space, fullout points to 
  the end of space filled by an actual content:*/
  char *g_fullout, *g_stopout;
};

#endif // CONNECT2KIRA_H
