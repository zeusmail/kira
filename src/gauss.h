/*
Copyright (C) 2017-2019 Philipp Maierhoefer

This file is part of pyRed.

pyRed is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

pyRed is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pyRed.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PYRED_GAUSS_H
#define PYRED_GAUSS_H

#include <cassert>
#include <cstddef>
#include <string>
#include <iostream>
#include <memory>
#include <utility>
#include <vector>
#include <tuple>
#include <unordered_map>
#include <algorithm> // max, min, sort, copy, min_element
#include <iterator> // back_inserter
#include <functional> // reference_wrapper
#include <stdexcept>

#include "config.h"
#include "defs.h"
#include "coeff_helper.h"
#include "parser.h" // parse_icpair
#include "keyvaluedb.h"
#ifdef KIRAFIREFLY
#include <ShuntingYardParser.hpp>
#ifdef FFDEV
#include <FFIntVec.hpp>
#endif
#endif

namespace pyred {

template<typename Coeff>
class Equation;

template<typename Coeff>
class EquationSolver;

template<typename Coeff>
using sol_map = std::unordered_map<
  intid, std::reference_wrapper<Equation<Coeff>>>;

template<typename Coeff>
bool cmp_icpair(const icpair<Coeff>& a, const icpair<Coeff>& b) {
  /*
  operator< to compare integral-coefficient pairs by integral.
  Makes std::sort place higher integrals first.
  */
  return a.first > b.first;
}

template<typename Coeff>
bool cmp_eqn(const Equation<Coeff>& a, const Equation<Coeff>& b) {
  /*
  Compare Equations by
  * highest integral: lower first
  * length: shorter first
  * if same highest integral and same length:
    first if lower integrals following
  * if all integrals the same:
    lower equation number first.
  * if an equation is empty, place it last.
  Makes std::sort place lower equations first.
  */
  if (a.empty()) return false;
  if (b.empty()) return true;
  if (a.front().first != b.front().first) {
    return a.front().first < b.front().first;
  }
  if (a.size() != b.size()) return a.size() < b.size();
  for (std::size_t i = 1; i != a.size(); ++i) {
    if (a.eq[i].first != b.eq[i].first) {
      return a.eq[i].first < b.eq[i].first;
    }
  }
  return a.eqnum < b.eqnum;
}

template<typename Coeff>
bool cmp_eqn_vec(const wi_equation<Coeff>& a, const wi_equation<Coeff>& b) {
  /*
  Compare wi_equation (equations as vectors).
  Same as comparing Equation objects, but without an equation number.
  I.e. the ordering is not unique.
  */
  if (a.empty()) return false;
  if (b.empty()) return true;
  if (a.front().first != b.front().first) {
    return a.front().first < b.front().first;
  }
  if (a.size() != b.size()) return a.size() < b.size();
  for (std::size_t i = 1; i != a.size(); ++i) {
    if (a[i].first != b[i].first) {
      return a[i].first < b[i].first;
    }
  }
  return false;
}

template<typename Coeff>
bool cmp_eqn_psolve(const Equation<Coeff>& a, const Equation<Coeff>& b) {
  /*
  Compare Equations by
  * highest integral: lower first
  * length: longer first
  * if same highest integral and same length:
    first if higher integrals following
  * if all integrals the same:
    lower equation number first.
  * if an equation is empty, place it last.
  Makes std::sort place lower equations first.
  */
  if (a.empty()) {return false;}
  if (b.empty()) {return true;}
  if (a.front().first < b.front().first) {return true;}
  if (a.front().first > b.front().first) {return false;}
  if (a.size() < b.size()) {return false;}
  if (a.size() > b.size()) {return true;}
  for (std::size_t i = 1; i != a.size(); ++i) {
    if (a.eq[i].first < b.eq[i].first) {return false;}
    if (a.eq[i].first > b.eq[i].first) {return true;}
  }
  return a.eqnum > b.eqnum;
}

template<typename Coeff>
bool cmp_eqn_inscount(const std::unique_ptr<EquationSolver<Coeff>>& a,
                      const std::unique_ptr<EquationSolver<Coeff>>& b) {
  if (a->fwd_insertions != b->fwd_insertions) {
    return a->fwd_insertions > b->fwd_insertions;
  }
  if (a->origlength != b->origlength) {return a->origlength > b->origlength;}
  return a->eqnum > b->eqnum;
}

template<typename Coeff>
struct cmp_eqn_heap {
  bool operator()(const std::shared_ptr<EquationSolver<Coeff>>& a,
                         const std::shared_ptr<EquationSolver<Coeff>>& b) {
//   bool operator()(const std::unique_ptr<EquationSolver<Coeff>>& a,
//                          const std::unique_ptr<EquationSolver<Coeff>>& b) {
    if (a->front().first < b->front().first) {return true;}
    if (a->front().first > b->front().first) {return false;}
    if (a->fwd_insertions != b->fwd_insertions) {
      return a->fwd_insertions > b->fwd_insertions;
    }
    if (a->origlength != b->origlength) {return a->origlength > b->origlength;}
    if (a->fwd_insertions == 0) {
      for (std::size_t i = 1; i != a->eq.size(); ++i) {
        if (a->eq[i].first < b->eq[i].first) {return false;}
        if (a->eq[i].first > b->eq[i].first) {return true;}
      }
    }
    return a->eqnum > b->eqnum;
  }
};


template<typename Coeff>
class Equation
{
public:
  // If it is a solution, the first element is (integral, -1).
  icpairs<Coeff> eq;
  // equation number (should start with 1).
  // could use eqnum = 0 if unassigned
  intid eqnum;
  // all constructors set eqnum
  Equation() = delete;
  Equation(intid eqnum);
  template<typename T>
  Equation(const T& eqin, intid eqnum);
  Equation(icpairs<Coeff>&& eqin, intid eqnum);
  Equation(EquationSolver<Coeff>&& sol);
  void move_from(EquationSolver<Coeff>&& sol);
  intid solve(const sol_map<Coeff>& sols, bool insertall,
              std::unique_ptr<keyvaluedb::KeyValueDB> &);
  bool empty() const {return eq.empty();}
  std::size_t size() const {return eq.size();}
  void reserve(std::size_t new_cap) {eq.reserve(new_cap);}
  icpair<Coeff>& at(std::size_t pos) {return eq.at(pos);}
  const icpair<Coeff>& at(std::size_t pos) const {return eq.at(pos);}
  icpair<Coeff>& front() {return eq.front();}
  const icpair<Coeff>& front() const {return eq.front();}
  icpair<Coeff>& back() {return eq.back();}
  const icpair<Coeff>& back() const {return eq.back();}
  typename icpairs<Coeff>::iterator begin() {return eq.begin();}
  typename icpairs<Coeff>::const_iterator cbegin() const {
    return eq.cbegin();
  }
  typename icpairs<Coeff>::iterator end() {return eq.end();}
  typename icpairs<Coeff>::const_iterator cend() const {
    return eq.cend();
  }
  void clear_eq() {eq.clear();}
  std::vector<intid> get_insertions(std::unique_ptr<keyvaluedb::KeyValueDB> &);
  void set_insertions(std::unique_ptr<keyvaluedb::KeyValueDB> &,
                      const std::vector<intid> &) const;
  void clear_insertions(std::unique_ptr<keyvaluedb::KeyValueDB> &);
};


template<typename Coeff>
class EquationSolver
{
  friend struct cmp_eqn_heap<Coeff>;
private:
  icpairs<Coeff> eq;
public:
  intid eqnum;
  // temporary result
  icpairs<Coeff> tmpeq;
  intid origlength;
  // Number of inserted equations before the equations is solved.
  intid fwd_insertions{0};
  // solutions to insert as (iterator, end, normfactor)
  std::vector<
    std::tuple<typename icpairs<Coeff>::const_iterator,
               typename icpairs<Coeff>::const_iterator,
               Coeff>
  > neededsols;
  std::vector<intid> m_inserted;
  // highest integral in the solutions which has not yet been treated
  intid hi_insols;
  EquationSolver(Equation<Coeff>&);
  void insert(const Equation<Coeff>&);
  bool proceed();
  bool compactify();
  void normalise();
  bool empty() {return tmpeq.empty();}
  icpair<Coeff>& front() {return tmpeq.front();}
};

template<typename Coeff>
class SystemOfEqs
{
public:
  std::vector<Equation<Coeff>> sys;
  SystemOfEqs() = default;
  SystemOfEqs(std::vector<eqdata>& eqs);
#ifdef KIRAFIREFLY
  SystemOfEqs(const std::vector<eqdata>& eqs,
              const std::vector<std::pair<std::string, uint64_t>>& randvars);

  SystemOfEqs(const std::vector<std::pair<firefly::ShuntingYardParser, std::vector<uint64_t>>>& systme,
              const std::vector<firefly::FFInt>& values);
#ifdef FFDEV
  SystemOfEqs(const std::vector<std::pair<firefly::ShuntingYardParser, std::vector<uint64_t>>>& syseme,
              const std::vector<std::vector<firefly::FFInt>>& values);
#endif
#endif
  void reserve(std::size_t sz);
  Equation<Coeff> & add(wi_equation<Coeff>&& eqdata, intid eqnum) {
    sys.emplace_back(std::move(eqdata), eqnum);
    return sys.back();
  }
  Equation<Coeff> & add(Equation<Coeff>&& eq) {
    sys.push_back(std::move(eq));
    return sys.back();
  }
  void sort();
  void sort(typename std::vector<Equation<Coeff>>::iterator,
            typename std::vector<Equation<Coeff>>::iterator);
  intid solve(bool insertall = true);
  intid psolve_map(
    intid = CoeffHelper::noint,
    std::size_t max_solversize = std::numeric_limits<std::size_t>::max());
//   intid psolve_heap();
  Equation<Coeff>& operator[](std::size_t pos) {return sys[pos];}
  std::size_t size() const {return sys.size();}
  bool empty() const {return sys.empty();}
  std::size_t capacity() const {return sys.capacity();}
  typename std::vector<Equation<Coeff>>::iterator begin() {return sys.begin();}
  typename std::vector<Equation<Coeff>>::iterator end() {return sys.end();}
  void clear() {sys.clear();}
  std::size_t truncate();
  std::vector<intid> independent();
  std::unique_ptr<keyvaluedb::KeyValueDB> &get_db() {return m_db;}
  void setup_insertions_db(std::size_t);
private:
  std::unique_ptr<keyvaluedb::KeyValueDB> m_db;
};

/************
 * Equation *
 ************/

template<typename Coeff>
Equation<Coeff>::Equation(intid eqnum): eqnum(eqnum) {}

template<typename Coeff>
template<typename T>
Equation<Coeff>::Equation(const T& eqin, intid eqnum): eqnum(eqnum) {
  reserve(eqin.size());
  std::copy(eqin.cbegin(), eqin.cend(), std::back_inserter(eq));
  std::sort(begin(), end(), cmp_icpair<Coeff>);
}

template<typename Coeff>
Equation<Coeff>::Equation(icpairs<Coeff>&& eqin, intid eqnum)
  : eq(std::move(eqin)), eqnum(eqnum) {
}

template<typename Coeff>
Equation<Coeff>::Equation(EquationSolver<Coeff>&& sol)
: eqnum{sol.eqnum}, eq{std::move(sol.tmpeq)} {}

template<typename Coeff>
void Equation<Coeff>::move_from(EquationSolver<Coeff>&& sol) {
  // Does not copy the list of inserted equations.
  // They are retained, however, when the moved equation belongs
  // to the same SystemOfEqs.
  eq = std::move(sol.tmpeq);
  eqnum = sol.eqnum;
}

template<typename Coeff>
intid Equation<Coeff>::solve(const sol_map<Coeff>& sols,
                             bool insertall,
                             std::unique_ptr<keyvaluedb::KeyValueDB> &db) {
  auto solver = EquationSolver<Coeff>(*this);
  bool untreated = true;
  bool unnormalised = true;
  while (untreated) {
    if (unnormalised || insertall) {
      auto sol = sols.find(solver.tmpeq.back().first);
      if (sol != sols.cend()) {
        solver.insert(sol->second.get());
        if (unnormalised) {
          ++solver.fwd_insertions;
        }
      }
      else if (unnormalised) {
        solver.normalise();
        unnormalised = false;
      }
    }
    untreated = solver.proceed();
  }
  eq = std::move(solver.tmpeq);
  if (eq.empty()) {
    solver.fwd_insertions = 0;
    clear_insertions(db);
  }
  else {
    auto ins = get_insertions(db);
    if (ins.empty()) {
      set_insertions(db, solver.m_inserted);
    }
    else {
      ins.reserve(ins.size() + solver.m_inserted.size());
      for (const auto iid: solver.m_inserted) ins.push_back(iid);
      set_insertions(db, ins);
    }
  }
  return solver.fwd_insertions;
}

template<typename Coeff>
std::vector<intid> Equation<Coeff>::get_insertions(
    std::unique_ptr<keyvaluedb::KeyValueDB> &db) {
  return db->get(eqnum,false);
}

template<typename Coeff>
void Equation<Coeff>::set_insertions(
    std::unique_ptr<keyvaluedb::KeyValueDB> &db,
    const std::vector<intid> &ins) const {
  db->put(eqnum, ins);
}

template<typename Coeff>
void Equation<Coeff>::clear_insertions(
    std::unique_ptr<keyvaluedb::KeyValueDB> &db) {
  db->remove(eqnum,false);
}

/******************
 * EquationSolver *
 ******************/

template<typename Coeff>
EquationSolver<Coeff>::EquationSolver(Equation<Coeff>& eqin)
: eq{std::move(eqin.eq)}
, eqnum{eqin.eqnum}
, origlength{static_cast<intid>(eq.size())}
{
  // Note: invalidates eqin; eqin must not be empty
  auto snd = eq.cbegin() + 1;
  if (snd != eq.cend()) {
    neededsols.emplace_back(snd, eq.cend(), Coeff(1));
    hi_insols = snd->first;
  }
  else {
    hi_insols = CoeffHelper::noint;
  }
  tmpeq.push_back(eq.front());
}

template<typename Coeff>
void EquationSolver<Coeff>::insert(const Equation<Coeff>& addsol) {
  // Add addsol to a solver.
  // addsol must be the solution of the lowest (=last) integral
  // (i.e. it has coefficient -1) in tmpeq.
  // Remember which integrals were inserted.
  m_inserted.push_back(addsol.front().first);
  auto solit = addsol.cbegin() + 1;
  if (solit != addsol.cend()) {
    neededsols.emplace_back(
      solit, addsol.cend(), tmpeq.back().second);
    if (hi_insols == CoeffHelper::noint) {
      hi_insols = solit->first;
    }
    else {
      hi_insols = std::max(hi_insols, solit->first);
    }
  }
  tmpeq.pop_back();
}

template<typename Coeff>
bool EquationSolver<Coeff>::proceed() {
  // Calculate the next non-vanishing coefficient.
  // Return true if a non-zero term was calculated, false otherwise
  // (false implies that all inserted solutions are depleted).
  tmpeq.emplace_back(hi_insols, Coeff(0));
  while (hi_insols != CoeffHelper::noint) {
    auto& lastcoeff = tmpeq.back().second;
    intid next_hi_insols{CoeffHelper::noint};
    auto sol = neededsols.begin();
    while (sol < neededsols.end()) {
      // add contribution to the current integral from all neededsols
      auto& solit = std::get<0>(*sol);
      auto& solend = std::get<1>(*sol);
      if (solit->first == hi_insols) {
        // sol contains the current integral -> insert
        lastcoeff += solit->second * std::get<2>(*sol);
        ++solit;
      }
      if (solit == solend) {
        // Nothing more to insert from this solution:
        // Remove it, place the solution from the end of neededsols here
        // (note that this might be the current solution)
        // and remove the latter from the end.
        *sol = neededsols.back();
        neededsols.pop_back();
      }
      else {
        if (next_hi_insols == CoeffHelper::noint) {
          next_hi_insols = solit->first;
        }
        else {
          next_hi_insols = std::max(next_hi_insols, solit->first);
        }
        ++sol;
      }
    }
    hi_insols = next_hi_insols;
    if (!lastcoeff) {
      tmpeq.back().first = hi_insols;
    }
    else {
      break;
    }
  }
  if (tmpeq.back().first == CoeffHelper::noint) {
    tmpeq.pop_back();
    return false;
  }
  return true;
}

template<typename Coeff>
bool EquationSolver<Coeff>::compactify() {
  // Insert all solutions from 'neededsols' until they are depleted.
  // Return true if the equation is non-empty, false otherwise.
  // As long as the equation is not normalised,
  // this can be used instead of proceed().
  while (proceed());
  neededsols.clear();
  eq.clear();
  if (tmpeq.empty()) return false;
  eq.swap(tmpeq);
  tmpeq.push_back(eq.front());
  auto snd = eq.cbegin() + 1;
  if (snd != eq.cend()) {
    neededsols.emplace_back(snd, eq.cend(), Coeff(1));
    hi_insols = snd->first;
  }
  else {
    hi_insols = CoeffHelper::noint;
  }
  return true;
}

template<typename Coeff>
void EquationSolver<Coeff>::normalise() {
  // Adjust the prefactors of the solutions so that the equation gets solved
  // for the integral in tmpeq. Set the coefficient of the integral to -1.
  auto inversefac = Coeff(-1)/tmpeq.back().second;
  tmpeq.back().second = Coeff(-1);
  for (auto& sol: neededsols) {
    std::get<2>(sol) *= inversefac;
  }
}

/***************
 * SystemOfEqs *
 ***************/

template<typename Coeff>
SystemOfEqs<Coeff>::SystemOfEqs(std::vector<eqdata>& eqs) {
  intid neqs{0};
  sys.reserve(eqs.size());
  for (const auto& eq: eqs) {
    icpairs<Coeff> tmpeq;
    tmpeq.reserve(eq.size());
    for (const auto& ic: eq) {
      tmpeq.emplace_back(ic.first, parse_coeff<Coeff>(ic.second));
    }
    sys.emplace_back(tmpeq, neqs++);
  }
}

template<typename Coeff>
void SystemOfEqs<Coeff>::reserve(std::size_t sz) {
  sys.reserve(sz);
  setup_insertions_db(sz);
}

template<typename Coeff>
void SystemOfEqs<Coeff>::sort() {
  std::sort(sys.begin(), sys.end(), cmp_eqn<Coeff>);
}

template<typename Coeff>
void SystemOfEqs<Coeff>::sort(
    typename std::vector<Equation<Coeff>>::iterator first,
    typename std::vector<Equation<Coeff>>::iterator last) {
  std::sort(first, last, cmp_eqn<Coeff>);
}

template<typename Coeff>
intid SystemOfEqs<Coeff>::solve(bool insertall) {
  sort();
  sol_map<Coeff> solmap;
  intid maxinsertions{0};
  for (auto& eq: sys) {
    auto ninsertions = eq.solve(solmap, insertall, m_db);
    maxinsertions = std::max(maxinsertions, ninsertions);
    if (!eq.empty()) {
      solmap.insert({eq.front().first, std::ref(eq)});
    }
  }
  return maxinsertions;
}

template<typename Coeff>
void SystemOfEqs<Coeff>::setup_insertions_db(std::size_t sz) {
  auto filename = Config::database_file().first;
  auto overwrite = Config::database_file().second;
  if (Config::insertion_tracer() == 0) {
    m_db = std::make_unique<keyvaluedb::KeyValueDiscard>("", 0, overwrite);
  }
  else if (Config::insertion_tracer() == 1) {
    m_db = std::make_unique<keyvaluedb::KeyValueVector>("", sz, overwrite);
  }
  else if (Config::insertion_tracer() == 2) {
    m_db = std::make_unique<keyvaluedb::KeyValueSQLite>(
      filename + ".db", sz, overwrite);
  }
#ifdef PYRED_KCDB
  else if (Config::insertion_tracer() == 3) {
    m_db = std::make_unique<keyvaluedb::KeyValueKC>(
      filename + ".kch", sz, overwrite);
  }
#endif
}

template<typename Coeff>
intid SystemOfEqs<Coeff>::psolve_map(intid insertion_limit,
                                            std::size_t max_solversize) {
  using sol_uptr_vec = std::vector<std::unique_ptr<EquationSolver<Coeff>>>;
  intid maxinsertions{0};
  if (sys.empty()) return maxinsertions;
  std::unordered_map<intid, sol_uptr_vec> solvers;
  std::sort(sys.begin(), sys.end(), cmp_eqn_psolve<Coeff>);
  auto sysit = sys.rbegin();
  auto solsysit = sys.rbegin();
  while (sysit < sys.rend() || !solvers.empty()) {
    intid hi{CoeffHelper::noint};
    if (sysit < sys.rend()) {
      hi = sysit->front().first;
    }
    intid hi_insolvers{CoeffHelper::noint};
    // Look up highest in solvers.
    for (const auto& s: solvers) {
      if (hi_insolvers == CoeffHelper::noint) {
        hi_insolvers = s.first;
      }
      else {
        hi_insolvers = std::max(hi_insolvers, s.first);
      }
    }
    typename std::unordered_map<intid,sol_uptr_vec>::iterator mapit;
    // typename sol_uptr_vec::iterator soleqit; // doesn't work with gcc 4.7
    typename std::vector<std::unique_ptr<EquationSolver<Coeff>>>::iterator
      soleqit;
    if ((hi >= hi_insolvers && hi != CoeffHelper::noint) ||
        hi_insolvers == CoeffHelper::noint) {
      // Solve the top equation from sys,
      // because other than those in solvers it has no insertions.
      auto sol = EquationSolver<Coeff>(*sysit);
      sol.normalise();
      while (sol.proceed());
      maxinsertions = std::max(maxinsertions, sol.fwd_insertions);
      solsysit->move_from(std::move(sol));
      // Create a new entry in solvers or return an iterator
      // to the existing (vector) entry.
      mapit = solvers.emplace(hi, sol_uptr_vec{}).first;
      while (++sysit < sys.rend() && sysit->front().first == hi) {
        // Insert following equations from sys with the same
        // highest integral into solsys.
        mapit->second.emplace_back(
          std::make_unique<EquationSolver<Coeff>>(*sysit));
      }
      // Solved equation was not in solvers (see below).
      soleqit = mapit->second.end();
    }
    else {
      // Choose the equation to solve from solvers.
      mapit = solvers.find(hi_insolvers);
      soleqit = std::max_element(mapit->second.begin(),
                                 mapit->second.end(),
                                 cmp_eqn_inscount<Coeff>);
      (*soleqit)->normalise();
      while ((*soleqit)->proceed());
      maxinsertions = std::max(maxinsertions, (*soleqit)->fwd_insertions);
      solsysit->move_from(std::move(*(*soleqit)));
    }
    auto& sol = *solsysit;
    ++solsysit;
    for (auto it = mapit->second.begin(); it != mapit->second.end(); ++it) {
      // Do not insert the solution into itself.
      if (it == soleqit) continue;
      // Insert solution and move non-vanishing equations
      // to their appropriate places in solvers.
      auto& eqptr = *it;
      eqptr->insert(sol);
      if (eqptr->neededsols.size() > max_solversize) {
        eqptr->compactify();
      }
      else {
        eqptr->proceed();
      }
      ++(eqptr->fwd_insertions);
      if (!eqptr->empty() && (eqptr->fwd_insertions <= insertion_limit)) {
        // Create solver entry if it doesn't exist.
        // TODO: avoid repeated look-ups of the same element.
        auto insit = solvers.emplace(eqptr->tmpeq.front().first,
                                     sol_uptr_vec{}).first;
        insit->second.emplace_back(std::move(eqptr));
      }
    }
    solvers.erase(mapit);
  }
  while (solsysit < sys.rend()) {
    solsysit->eq.clear();
    ++solsysit;
  }
  return maxinsertions;
}

// template<typename Coeff>
// intid SystemOfEqs<Coeff>::psolve_heap() {
//   intid maxinsertions{0};
//   std::priority_queue<std::unique_ptr<EquationSolver<Coeff>>,
//                       std::vector<std::shared_ptr<EquationSolver<Coeff>>>,
// //                       std::vector<std::unique_ptr<EquationSolver<Coeff>>>,
//                       cmp_eqn_heap<Coeff>> sysheap;
//   std::vector<Equation<Coeff>> solsys;
//   solsys.reserve(sys.size());
//   for (auto& eq: sys) {
//     sysheap.emplace(new EquationSolver<Coeff>(eq));
//   }
//   while (!sysheap.empty()) {
//     auto solptr = sysheap.top();
//     sysheap.pop();
//     solptr->normalise();
//     while (solptr->proceed());
//     maxinsertions = std::max(maxinsertions, solptr->fwd_insertions);
//     solsys.emplace_back(*solptr);
//     auto hi = solsys.back().front().first;
//     while (!sysheap.empty() && sysheap.top()->front().first == hi) {
//       auto eqptr = sysheap.top();
//       sysheap.pop();
//       eqptr->insert(solsys.back());
//       ++(eqptr->fwd_insertions);
//       if (eqptr->proceed()) {
//         sysheap.push(eqptr);
//       }
//     }
//   }
//   sys = std::move(solsys);
//   return maxinsertions;
// }

template<typename Coeff>
std::size_t SystemOfEqs<Coeff>::truncate() {
  sort();
  if (!sys.empty()) {
    while (sys.back().empty()) {
      sys.pop_back();
    }
  }
  return sys.size();
}

template<typename Coeff>
std::vector<intid> SystemOfEqs<Coeff>::independent() {
  auto neqs = truncate();
  std::vector<intid> eqnums;
  eqnums.reserve(neqs);
  for (const auto& eq: sys) {
    eqnums.push_back(eq.eqnum); // subtract one?
  }
  std::sort(eqnums.begin(), eqnums.end());
  return eqnums;
}

#ifdef KIRAFIREFLY
template<typename Coeff>
SystemOfEqs<Coeff>::SystemOfEqs(
    const std::vector<eqdata>& eqs,
    const std::vector<std::pair<std::string, uint64_t>>& randvars) {
  intid neqs{0};
  reserve(eqs.size());

  for (const auto& eq: eqs) {
    icpairs<Coeff> tmpeq;
    tmpeq.reserve(eq.size());
    for (const auto& ic: eq) {
      tmpeq.emplace_back(ic.first, parse_coeff<Coeff>(ic.second, randvars));
    }
    sys.emplace_back(std::move(tmpeq), neqs++);
  }

  // clear cache
  parse_coeff<Coeff>("", randvars);
}

template<typename Coeff>
SystemOfEqs<Coeff>::SystemOfEqs(
				const std::vector<std::pair<firefly::ShuntingYardParser, std::vector<uint64_t>>>& system,
				const std::vector<firefly::FFInt>& values) {
  intid neqs{0};
  reserve(system.size());

  for (const auto& eq : system) {
    icpairs<Coeff> tmp_eq;
    tmp_eq.reserve(eq.second.size());
    std::size_t i = 0;

    for (const auto& val : eq.first.evaluate_pre(values)) {
      tmp_eq.emplace_back(eq.second[i], val);
      ++i;
    }

    sys.emplace_back(std::move(tmp_eq), neqs++);
  }
}

#ifdef FFDEV
template<typename Coeff>
SystemOfEqs<Coeff>::SystemOfEqs(
                                const std::vector<std::pair<firefly::ShuntingYardParser, std::vector<uint64_t>>>& system,
                                const std::vector<std::vector<firefly::FFInt>>& values) {
  intid neqs{0};
  reserve(system.size());

  for (const auto& eq : system) {
    icpairs<Coeff> tmp_eq;
    tmp_eq.reserve(eq.second.size());

    //TODO why is the bunched parser slower?
    //    std::vector<std::vector<firefly::FFInt>> eqns = eq.first.evaluate_pre(values);
    std::size_t b_size = values.size();
    std::vector<std::vector<firefly::FFInt>> eqns(b_size);
    for(std::size_t i = 0; i != b_size; ++i){
      eqns[i] = eq.first.evaluate_pre(values[i]);
    }

    std::size_t tmp_eq_size = eq.second.size();

    for(std::size_t i = 0; i != tmp_eq_size; ++i){
      std::vector<firefly::FFInt> eqn(b_size);

      for (std::size_t j = 0; j != b_size; ++j) {
	      eqn[j] = eqns[j][i];
      }

      tmp_eq.emplace_back(eq.second[i], firefly::FFIntVec(eqn));
    }

    sys.emplace_back(std::move(tmp_eq), neqs++);
  }
}
#endif
#endif

}

#endif
