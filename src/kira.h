/* This file is a part of the program Kira.
 * Copyright (C) Johann Usovitsch <jusovitsch@googlemail.com>
 * Philipp Maierhoefer <particle@maierhoefer.net>
 * Peter Uwer <peter.uwer@physik.hu-berlin.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version , or (at
 * your option) any later version as published by the Free Software
 * Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
*/
#ifndef KIRA_H_
#define KIRA_H_
#include <fstream>
#include <iostream>
#include "ReadYamlFiles.h"
#include "integral.h"
#include "dataBase.h"
#include "connect2kira.h"
#include "interface.h"
#include <tuple>
#include <unordered_map>
#include <gzstream.h>
#include <condition_variable>

#ifdef KIRAFIREFLY
#include "black_box.h"
#endif

#define RED 20
#define SIZESYM 64


typedef std::vector< int > Vint;
typedef Vint::iterator ItVint;
typedef Vint::reverse_iterator RItVint;

typedef std::vector<std::vector<int> > VVint;
typedef VVint::iterator ItVVint;
typedef VVint::reverse_iterator RItVVint;

typedef std::vector<int*> Vintx;
typedef Vintx::iterator ItVintx;

typedef std::vector< BaseIntegral * > VG;
typedef VG::iterator ItVG;

typedef std::vector< BaseEquation * > VE;
typedef VE::iterator ItVE;

typedef std::vector< symmetries > SYM;
typedef SYM::iterator ItSYM;

typedef std::vector< std::vector < BaseIntegral * > > VVG;
typedef VVG::iterator ItVVG;

typedef std::unordered_map<std::uint64_t, std::tuple<int,int,int> > INTEGMAP;
typedef INTEGMAP::const_iterator INTEGMAPI;

typedef std::unordered_map<std::uint64_t, std::uint64_t > MASTERSMAP;
typedef MASTERSMAP::const_iterator MASTERSMAPI;

typedef std::map<std::uint64_t, std::tuple<BaseIntegral ***, int> > FWRED;
typedef FWRED::iterator FWREDI;

typedef std::unordered_map<std::uint64_t, int > RDY2F;
typedef RDY2F::iterator RDY2FI;

typedef std::map<int, BaseIntegral * > map_id_to_key;
typedef map_id_to_key::iterator iterator_id_to_key;

typedef std::list<int> Lint;
typedef std::list<int>::iterator ItLint;

class ConvertResult;
class ThreadPool;
class LeeCriterium;
class info;
class treatEq;
class Algebra;

struct tupelCharacteristics {
  int sector;
  int topology;
  int flag2;
};

class Kira {
public:
  Kira(std::string, int coreNumber/*, std::string algebra_*/, std::string pyred_config, int integralOrdering);
  friend std::ostream& operator<< (std::ostream& out, const BaseIntegral& per);
  friend std::istream& operator>> (std::istream& out, BaseIntegral& per);
  friend class ConvertResult;
  friend class BaseEquation;
//   friend class SeedsKira;
//   friend void initiate_seeds(Kira &kira, std::uint32_t nOfTopology, std::string InputMasters );
  friend void run_relations(Kira& kira_, std::uint32_t nOfTopology_);
  friend class Algebra;
  friend class Pak;
  friend class treatEq;
  // read base data and initiate these base data
  void read_config();
  void read_kinematics(int flag_user_defined_system);
  void init_kinematics();
  void read_integralfamilies(int flag_user_defined_system);
  void init_integralfamilies();
  void destroy_integralfamilies();
  void collect_reductions(Jobs & jobs);
  void collect_reductions_helper(
    std::string& itTopo,
    std::tuple<std::vector<std::string> /*topologies*/,
    std::vector<int> /*sectors*/,
    int /*rmax*/,
    int /*smax*/,
    int /*dmax*/> &itSpec);
  void get_topology_relations();
  void execute_jobs();


  static std::string treatcoeff2(const std::string& str);

  // create IBP and LI
  void create_IBP();
  void create_IBP_helper(std::vector<GiNaC::possymbol>& var);
  void create_LI();
  void reduce_scal(IBPVG &ibpp);

  void create_LEE();
  void create_LEE_vectors(std::vector<GiNaC::possymbol>& var);
  void create_LEE_vectors2(std::vector<GiNaC::possymbol>& var);

  // get zero sectors
  int find_zero_sectors();

  // symmetry relations
  void search_symmetry_relations();
  int skip_symmetry(std::string topoName, int j, int it, int itt);
  int symmetry_finder(int it, int itt, std::string topoName,int j, int array2[], int array3[], int flag, unsigned klop);
  int symmetry_finder_reverse(int it, int itt, std::string topoName,int j, int array2[], int array3[], int flag);
  int prepare_symmetry();
  void symmetry_relations(std::string topoName);
  int test(GiNaC::lst &matr);
  int testProps(GiNaC::lst &matr);
  void write_symmetries(const std::string otputName, SYM symVec[]);
  int read_symmetries(const std::string inputName, SYM symVec[]);
  void print_relations(const std::string name, SYM *&symVec, int itC);
  void print_relations_reverse(const std::string name, SYM *&symVec, int itC);

  // setup a system of equations
  void read_line(std::string fileMasters,std::vector<std::pair<std::string,std::vector<int> > >& arraySeed);
  void preferred_masters(std::string fileMasters);
  void print_equation(BaseIntegral*& integral,ogzstream& output,std::ofstream& outputX, std::tuple<int,int/*,unsigned,int*/>& printControl,std::tuple<std::string, std::string>& dest);
  void print_equationSW(BaseIntegral*& integral,ogzstream& output,std::ofstream& outputX, std::tuple<int,int,unsigned,int>& printControl,std::tuple<std::string, std::string>& dest,int &rememberID);
  void print_equation2(BaseIntegral*& integral,ogzstream& output);
  void write_init_system(std::vector<BaseEquation*>& setUpEq,const std::string & dest, int secNUM,const std::string & add);
  void write_pyred(std::vector<std::vector<std::pair<std::uint64_t,std::string> > > & eqs,/*
  std::vector<std::vector<int > > & arraySeed,*/
  std::vector<pyred::intid> & independent_eqnums);
  std::vector<std::vector<int > > read_id2int();

  void write_triangular(/*unsigned sectorNumber, int topology,*/std::unordered_map<std::uint64_t, std::tuple<BaseIntegral ***, int> >& forwardRed, std::vector <std::uint64_t>& reduce_please, int flagy);
  void write_triangularSW(unsigned sectorNumber, int topology, std::unordered_map<std::uint64_t, std::tuple<BaseIntegral ***, int> >& forwardRed, std::vector <std::uint64_t>& reduce_please, int flagy);

  void skip_integral(igzstream& input, int eqLength);

  void trace_integral(igzstream& input, int eqLength);
  void read_integral(BaseIntegral*& integral, igzstream& input, int eqlength);
  template<typename T>
  void read_integral2(T& input, int Eqlength,
		      std::unordered_map<std::uint64_t, std::tuple<BaseIntegral ***, int> >& forwardRed,
		      std::unordered_map<std::uint64_t,int>& rdy2F);
//   void load_pyred(std::vector<std::vector<std::pair<std::uint64_t,std::string> > > & eqs );
//   void load_pyred_on_the_fly(std::vector<std::uint64_t>& mandatory, std::vector<std::uint64_t>& optional);
  int load_triangular(int flagOTF, unsigned k, int sectorNumber,
			  std::unordered_map<std::uint64_t, std::tuple<BaseIntegral ***, int> >& forwardRed, std::vector <std::uint64_t>& reduce_please,
			  std::unordered_map<std::uint64_t,int>& rdy2F);
  int load_triangularVER(unsigned k, int sectorNumber,
			  std::unordered_map<std::uint64_t, std::tuple<BaseIntegral ***, int> >& forwardRed, std::vector <std::uint64_t>& reduce_please,
			  std::unordered_map<std::uint64_t,int>& rdy2F);

  void check_reduced_integrals(uint32_t sectorNumber, uint32_t k,
			       DataBase databaseEQ[]);
  void load_all_integrals(uint32_t sectorNumber, uint32_t k,
			  DataBase databaseEQ[],
			  std::vector <std::pair<uint32_t,uint32_t> >& setofS,
			  std::uint32_t& reducedReihen, std::uint32_t& skipReihen);
  int load_back_substitution();

  void initiate_system_of_equations(int nOfTopology);
  int setup_ibp_li_eq(std::vector<BaseEquation*>& setUpEq, VE &identities, std::vector<std::uint64_t>& weights, std::string a,int topology);
  int setup_sym_eq(std::vector<BaseEquation*>& setUpEq, std::string a, int topologyNUM, int secNUM);
  int look_up_symmetry(std::vector<BaseEquation*>& setUpEq, IBPIntegral *&r, SYM *&sym, int nOfTopology);

  //select equations
  void select_spec_helper(int itC, std::vector<pyred::SeedSpec>& initiateMAN, std::tuple<std::vector<std::string> /*topologies*/,
  std::vector<int> /*sectors*/,
  int /*rmax*/,
  int /*smax*/,
  int /*dmax*/>& itSpec, int& countCHOICE);
  void select_equations(std::vector<std::uint64_t>& mandatory, std::vector<std::uint64_t>& optional,int itC, Jobs& jobs, int flag_user_defined_system);
  void select_initial_integrals(std::vector<pyred::SeedSpec>& initiateSOE);
  void complement_initial_integrals(std::vector<pyred::SeedSpec>& complementSOE);

  // use pyred
  void write_seeds_to_disk(std::vector<std::uint64_t>& mandatory);
  void generate_SOE(int flagOTF, std::vector<std::uint64_t> &selected_integrals);
  void run_pyred(std::vector<std::uint64_t>& mandatory, std::vector<std::uint64_t>& optional,pyred::System& sys);
//   void get_reduced_eqs(pyred::System & sys);
  void record_masters(std::pair<std::vector<pyred::intid>,std::vector<std::uint64_t> >& subsys);

  // run reduction
  std::vector<std::string> collectReductions;
  void initiate_fermat(int kira2math);
  void destroy_fermat();
  void complete_triangular(int flagOTF, std::vector<std::uint64_t>& mandatory);
  void complete_triangularSW(int flagOTF, std::vector<std::uint64_t>& mandatory);
  int insert_equation( BaseIntegral *&EqPtr, BaseIntegral *&insertEqPtr);
//   void run_reduction( std::map<std::uint64_t, std::tuple<BaseIntegral ***, int> >& forwardRed,
// 		      std::vector <std::uint64_t>& reduce_please,
// 		      std::unordered_map<std::uint64_t,int>& rdy2F, std::set<std::uint64_t>& setMandatory);

  void run_reduction( std::unordered_map<std::uint64_t, std::tuple<BaseIntegral ***, int> >& forwardRed,
		      std::vector <std::uint64_t>& reduce_please,
		      std::unordered_map<std::uint64_t,int>& rdy2F, std::set<std::uint64_t>& setMandatory, std::tuple<int,int>& printControl);

  void parallel_pool( std::unordered_map<std::uint64_t, std::tuple<BaseIntegral ***, int> >& forwardRed,
			  std::vector <std::uint64_t>& reduce_please_back_up,
			  int &red_1,
			  std::unordered_map<std::uint64_t,int>& rdy2F, std::set<std::uint64_t>& setMandatory, std::uint64_t it, std::uint32_t& information);

  std::pair<int,int> get_power(std::string& stringofinterest);

  void init_numbers(  std::vector<int>& numbersINIT, std::vector<std::vector<std::string> >& numbersDenStrVec);

  int get_power_level(std::string& numeratorPart);

  std::pair<std::string,std::string> normsample(std::string stringofinterest,
						int& valueForD, int& degree,
						int& powerLevel1, int& powerLevel2);

  std::vector<int> generate_sample_points(std::pair<std::string,std::string>& normsampleVar, int degree);

  std::vector<int> check_sample_points(std::pair<std::string,std::string>& normsampleVar, std::vector<int>& points);

  std::tuple<std::vector<std::string>,std::vector<std::string>, std::vector<int> >
  sample(std::vector<int>& numbersINIT, std::string& stringofinterest,
	 std::pair<std::string,std::string>& normsampleVar, std::uint64_t& tmp1, std::uint64_t& tmp2);

  std::tuple<std::vector<std::string>, std::vector<std::string>, std::vector<int> >
sample(std::vector<int>& numbersINIT, std::list<std::string>& ptrToString,
	     std::pair<std::string,std::string>& normsampleVar, uint64_t& tmp1, uint64_t& tmp2);

  std::vector<std::string> normalize(std::vector<int>& numbersINIT, int valueForD, std::vector<std::string>& numerator, std::string& numNormalization);

  std::pair<std::string, int> reconstruct_function(std::vector<int>& numbersINIT, std::vector<std::vector<std::string> >& numbersDenStrVec,
				   std::vector<std::string>& numerator,
				   int& powerLevel);

  std::string reconstruct_final(std::string& reconstrNum, std::string& reconstrDen);
  std::string reconstruct_final(std::list<std::string>& ptrToString);
  void sort_equation(BaseIntegral *&EqPtr);

  int complete_reduction();

  int get_number_of_masterintegrals(unsigned it, std::vector<std::uint64_t>& lriMI,int **IDarray);
  unsigned addup_common_masters(unsigned it, unsigned j, int **IDarray,
			    BaseIntegral newEqPtr[],std::vector<std::list<std::string> > &listPtrToString);
  unsigned addup_final_masters(unsigned it, unsigned j, int **IDarray,
				BaseIntegral newEqPtr[], std::vector<std::list<std::string> >  &listPtrToString);
  void back_subs();
  void write_result(BaseIntegral *&EqPtr,int i_Integrals,std::ofstream &output);
  void clean_back_subs();

  // output mathematica
  void kira_mathematica(std::string,int massRC);

#ifdef KIRAFIREFLY
  void finite_fields_reconstruction(int flagOTF, const std::vector<uint64_t>& mandatory_vec, const uint32_t mode_);
  void load_ff_system(int flagOTF);
  void write_to_database();
#endif

private:
  std::string kiraMode;
  static Fermat *fermat;
  ThreadPool *pool, *pool2, *pool3, *pool4, *pool5, *poolBS;
  std::uint64_t core_mask;
  std::stack<int> idFermats, idCombine;
  int coreNumber/*, algebra*/, integralOrdering, sectorOrdering;
  std::string pyred_config;
  std::string outputDir;
  std::string inputDir;

  int OTF, dataBase, dataFile, serialWrite, skipPyred, writeNumericalSystem, pyredDatabase, magicRelations, conditionalSystem, conditionalMemory;
  unsigned heuristic, reconstFlag, termNumber, algebraicReconstruction;
  bool LIflag;
  unsigned maxParallelSectors;

  int calculate_coefficient(std::list<std::string>& ptrToString);
  int calculate_coefficient_term(std::list<std::string>& ptrToString, int idComb);
  int calculate_coefficient_term2(std::list<std::string> ptrToString, std::string& interpVar, int numbers, std::string& result, int idComb);
  std::string jobName,specialName;

  // maps
  unsigned numberOfEq, numberOfEqXXX;
  BaseIntegral **systemEq;
  std::unordered_map<std::uint64_t,std::uint64_t> mastersMap, mastersReMap;
//   std::vector<BaseEquation*> setUpEq;
//   std::vector<BaseEquation> setUpEq2;
//   BaseEquation** setUpEq;

  //DGL
  void generate_dgl();
  std::vector<std::tuple<std::string,int,std::vector<int>,std::string> >  read_seeds_dgl(std::string& itFile);
  void insert_seeds2DGL(std::vector<std::tuple<std::string, int, std::vector<int>, std::string> >& seedsDGL);

  // zero sectors/symmetries/shifts
  std::pair<int,GiNaC::ex>  test_quadratic(GiNaC::ex & start);

  GiNaC::lst suby;
  std::vector <std::tuple<GiNaC::lst,GiNaC::lst, int, GiNaC::ex, 				std::vector<std::string> > > externalTransf;
  GiNaC::lst invariants4sym, invariants4symRev;
  GiNaC::lst invariantsReplacement, invariantsReplacementRev, invariantsList;
  std::vector<GiNaC::possymbol> invariantsPlaceholder, symbolInvariants;
  int controlSymmetries;
  std::vector<std::string> invarMap;

  // initiate kira, base data
  Integral_F integralfamily;
  std::map<std::string,Integral_F> topology;
  std::string fermatPath;
  int numFlag, biggestBound, realSector;
  std::vector<int> num, numMin, den, denTPlus;
  std::vector<GiNaC::possymbol> externalVar, allVar, invar;


  std::vector<std::string> reductVar;
  std::string interpVar;
  std::vector<int> invarDim;
  std::vector<std::string> invarStr, invarSol;
  int massSet2OneDim;
  GiNaC::ex dimension, massSet2One;
  GiNaC::lst kinematic, kinematicR, kinematic2, kinematicOld, specialKinematics,
  momentConservation, kinematicShift, kinematicShiftB, kinematicReverse,
  mass2One, kinematicShiftR, unknownsExt;
  std::vector<GiNaC::possymbol> bSsymbols;

  GiNaC::ex mom_uno;
  GiNaC::possymbol *bS;
  GiNaC::symtab GiNaCSymbols;

#ifdef KIRAFIREFLY
  uint32_t reconstruction_mode = 0; // 0: full reconstruction, 1: only back substitution
  std::vector<std::string> symbols;
  std::string replace_by_one;

  BlackBoxKira* bb;
  firefly::Reconstructor* reconst;

  DataBase* database_reconst;
  bool first = true;
  std::unordered_map<uint64_t, std::pair<int, int>> integral_data; // sector, flag2
#endif

  int onlyBacksubstitution, onlyTriangular, onlyPyred,
      onlyInitiate, startJob;


  unsigned reihen, totalReihen;
  BaseIntegral ***allEq;
  std::vector<std::uint64_t> masterVectorSkip;
  std::multimap<std::string,std::vector<int> > selectMastersReduction;

  unsigned *length, *rdy2P;
  std::unordered_map<std::uint64_t,unsigned> occurrence;
  std::vector <unsigned> reduct2StartHere;
  std::uint64_t *last_reduce;
  std::unordered_map<std::uint64_t, unsigned> reverseLastReduce;
  unsigned eqnum;
};

class info{
public:
  int dots;
  int nums;
  int sector;
  int topology;
};

class kiraOutput {
public:
  std::string str[12];
  kiraOutput(int choice){
    switch (choice){
      case 0:
	str[0] = "FORM";
	str[1] = ".inc";
	str[2] = "";
	str[3] = "id ";
	str[4] =  " + ";
	str[5] = "(";
	str[6] =";\n\n";
	str[7] = ") = \n";
	str[8] = ")*(";
	str[9] =")\n";
	str[10] ="0\n";
	str[11] =";";
	break;
      case 1:
	str[0] = "Mathematica";
	str[1] = ".m";
	str[2] = "{\n";
	str[3] = "";
	str[4] =  " + ";
	str[5] = "[";
	str[6] =",\n";
	str[7] = "] -> \n";
	str[8] = "]*(";
	str[9] =")\n";
	str[10] ="0\n";
	str[11] ="}";
	break;
      case 2:
	str[0] = "Kira";
	str[1] = ".kira";
	str[2] = "";
	str[3] = "";
	str[4] = "+";
	str[5] = "[";
	str[6] ="\n";
	str[7] = "]=\n";
	str[8] = "]*(";
	str[9] =")\n";
	str[10] ="0\n";
	str[11] ="";
	break;
    }
  };
};

class Algebra{
public:
  Algebra(){};
  Algebra(Kira *kira_, std::string& interpVar_, std::vector<std::string>& reductVar_,
    std::vector<std::pair<std::string,int> >& numericVar_, int level);

  Kira *kira;
  std::string interpVar;
  std::vector<std::string> reductVar;
  std::vector<std::pair<std::string,int> > numericVar;
  uint32_t depthLevel;
  int poolLevel;

  std::string reconstruct_final(std::string& stringofinterest1, std::string& stringofinterest2);

  std::pair<std::string, std::string> normsample(
    std::string stringofinterest, int& valueForD, int& degree,			     int& powerLevel1, int& powerLevel2);

  std::tuple<std::vector<std::string>, std::vector<std::string>, std::vector<int> > sample(std::vector<int>& numbersINIT, std::string& stringofinterest, 			  std::pair<std::string,std::string>& normsampleVar, uint64_t& tmp1,
    uint64_t& tmp2);

  std::vector<std::string> normalize(std::vector<int>& numbersINIT, int valueForD,
    std::vector<std::string>& numerator, std::string& numNormalization);

  std::pair<std::string, int> reconstruct_function(std::vector<int>& numbersINIT,
    std::vector<std::vector<std::string> >& numbersDenStrVec,
    std::vector<std::string>& numerator, int& powerLevel);

  std::vector<int> check_sample_points(std::pair<std::string,std::string>& normsampleVar, std::vector<int>& points);

  std::vector<int> generate_sample_points(std::pair<std::string,std::string>&
    normsampleVar, int degree);

  int get_power_level(std::string& numeratorPart);

  void init_numbers( std::vector<int>& numbersINIT,
    std::vector<std::vector<std::string> >& numbersDenStrVec);

  std::pair<int,int> get_power(std::string& stringofinterest);

};

class ConvertResult{
public:
  ConvertResult(){};
  ConvertResult(Kira& kira, std::string topologyName, int topologyNumber, std::string &inputName,const  std::string& inputDir, std::vector<std::uint64_t> & idOfSeed);
  ConvertResult(Kira& kira, std::string topologyName, int topologyNumber,const  std::string& inputDir, std::vector<pyred::Integral>& listOfIntegrals, std::vector<std::uint64_t> & idOfSeed, std::string &inputName);
  ~ConvertResult();
  void look_up_seeds();
  void get_integral_id(std::vector<std::uint64_t> & idOfSeed);
  void get_integral_id_pyred(std::vector<std::uint64_t> & idOfSeed);
  int skip_integral(std::vector<int>& seed);
  void reconstruct_mass( Kira& kira, std::vector<DBintegral>& integralV );
  void prepare_FORM(std::vector<DBintegral>& integralV);

  int output(Kira& kira, int massReconstruction, std::vector<std::uint64_t>& idOfSeed, int choice);
  DataBase *database;
  std::string topologyName;
  int topologyNumber;
  std::string inputDir, inputName;

  std::ofstream Output;
  std::ifstream seedsInput;
  std::ifstream trivialSectorInput;
  std::vector<int> trivSV;
  std::vector<std::vector<int> > arraySeed;
  std::vector<std::vector<int> > zeroIntegrals;
};

class ThreadPool;

// our worker thread objects
class Worker {
public:
    Worker(ThreadPool &s) : pool(s) { }
    void operator()();
private:
    friend class Kira;
    ThreadPool &pool;
};

// the actual thread pool
class ThreadPool {
public:
    ThreadPool()  :  stop(false){};
    void initialize(uint32_t);
    template<class F>
    void enqueue(const F &f);
    ~ThreadPool();
private:
    friend class Worker;
    friend class Kira;

    // need to keep track of threads so we can join them
    std::vector<std::thread > workers;

    // the task queue
    std::deque<std::function<void()> > tasks;

    // synchronization
    std::mutex queue_mutex;
    std::condition_variable condition;
    bool stop;
};

#endif // KIRA_H_
