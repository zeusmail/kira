/* This file is a part of the program Kira.
 * Copyright (C) Johann Usovitsch <jusovitsch@googlemail.com>
 * Philipp Maierhoefer <particle@maierhoefer.net>
 * Peter Uwer <peter.uwer@physik.hu-berlin.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version , or (at
 * your option) any later version as published by the Free Software 
 * Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
*/
#ifndef CREATESEEDS_H
#define CREATESEEDS_H
#include "kira.h"
#include <tuple>

class SeedsKira {
public:
  SeedsKira(Kira& kira, int nOfTopology);
  ~SeedsKira();
  void gen_int(int n, int **array, int maxI, int option);
  void fusion(int j, int iS2, int topology, int noIBP, int symDOTS, int symNUMS);
  void prepare_seeds();
  void generate_dots();
  void generate_nums();
  void create_seeds();
  void create_symmetry_seeds();
  int save();
  Kira * kira;
  int den_max;
  int num_max, num_max_sym;
  int nOfTopology;
  std::vector<std::uint64_t> **aIBPint;
  int*** dotsMaster, ***scalMaster;
  int *nSector, **mappingSector, **mappingSectorReverse;
  std::set<std::vector<int> > testSet;
  std::unordered_map<std::uint64_t, std::tuple<int, int, int> > integralMap; //sector, topology, flag2
};

void initiate_seeds(Kira& kira, std::size_t nOfTopology);
void run_relations(Kira& kira_, std::uint32_t nOfTopology_);
void get_properties(std::uint64_t id, std::tuple<std::string,unsigned,unsigned>& integral);
#endif
