
.. sectnum::

=============================================
 Kira - A Feynman Integral Reduction Program
=============================================

.. --------
..  Readme
.. --------

.. contents:: Table of Contents
..    :depth: 2

Release notes
=============

See `ChangeLog <https://gitlab.com/kira-pyred/kira/blob/master/ChangeLog>`_.

Installation
============

Kira can either be built using the ``Autotools`` build system or the ``Meson`` build system. We recommend ``Meson`` if a sufficientlt recent version is available on your system (see section 1.4).

Prerequisites
-------------

Compiler requirements: ``gcc`` version 5 or later, or ``clang`` version 3.4 or later.

Kira requires the following packages to be installed on the system: ``GiNaC`` (which itself requires ``CLN``), ``yaml-cpp``, ``zlib``. In addition, the program ``Fermat`` is required to run Kira.

Please note that ``GiNaC``, ``CLN`` and ``yaml-cpp`` must be compiled with the same compiler which is used to compile Kira. Otherwise the linking step will most likely fail. If you are using the system compiler, you can usually install these packages via your system's package manager. However, if you are using a different compiler, this means in practice that you also have to install these packages from source and set the environment variables ``C_PATH``, ``LD_LIBRARY_PATH`` and ``PKG_CONFIG_PATH`` according to the installation prefix. See section 1.5 for more details.

Autotools
---------
If you obtained ``Kira`` from the ``Git`` repository, you first need to run::

   autoreconf -i

Then compile and install with::

   ./configure --prefix=/install/path
   make
   make install

where the optional ``--prefix`` argument sets the installation prefix.

Meson
-----

Since Kira 1.2 one may optionally use the ``Meson`` build system (http://mesonbuild.com) instead of ``Autotools`` to build Kira.::

   meson --prefix=/install/path builddir
   cd builddir
   ninja
   ninja install

where ``builddir`` is the build directory. Specifying the installation prefix with ``--prefix`` is optional.

Obtaining Meson and Ninja
-------------------------

Old version of Meson might not work. We recommend Meson 0.46 or later. If no sufficiently recent version of Meson is available from your distribution's package repository, the latest version can be installed with ``pip3 install meson`` (system-wide) or with ``pip3 install --user meson`` in the user's home directory. ``Python 3.5`` or later is required. The Ninja binary can be downladed from https://ninja-build.org.

Specifying a compier and a non-default install location
-------------------------------------------------------

If your are using a compiler which is not your system compiler, in general you will also need to compile the dependencies with that compiler. If the dependencies are not installed in a default location, the search paths must be set accordingly.

Set the compiler you want to use (here: ``g++-X`` for ``C++`` and ``gcc-X`` for ``C``) and the installation prefix::

   export CXX=g++-X
   export CC=gcc-X
   PREFIX=/install/prefix
   export LD_LIBRARY_PATH=$PREFIX/lib64:$PREFIX/lib:$LD_LIBRARY_PATH
   export CPATH=$PREFIX/include:$CPATH
   export PKG_CONFIG_PATH=$PREFIX/lib64/pkgconfig:$PREFIX/lib/pkgconfig:$PKG_CONFIG_PATH

To compile ``CLN`` with this compiler and install it to the location specified by ``$PREFIX``, extract the ``CLN`` source package, change into the ``CLN`` directory and run::

   ./configure --prefix=$PREFIX
   make
   make install

Extract the ``GiNaC`` source package, change into the ``GiNaC`` directory and run::

   ./configure --prefix=$PREFIX
   make
   make install

Extract the ``yaml-cpp`` source package, change into the ``yaml-cpp`` directory and run::

   mkdir build
   cd build
   cmake -DCMAKE_INSTALL_PREFIX=$PREFIX -DBUILD_SHARED_LIBS=ON ..
   make
   make install

Change into the ``Kira`` directory and run (of course, ``autotools`` may be used as well)::

   meson --prefix=$PREFIX
   cd builddir
   ninja
   ninja install

To run ``Kira``, ``LD_LIBRARY_PATH`` must be set, so that the same libraries are linked at startup as were linked at compile time. Then run::

   $PREFIX/bin/kira

Optionally, you may set the ``PATH`` environment variable so that the full path is not required when starting kira.::

   PATH=$PREFIX/bin:$PATH
   kira

It may be convenient to set and export ``LD_LIBRARY_PATH`` and ``PATH`` in your shell configuration (e.g. ``~/.bashrc`` if you are using ``Bash``).
