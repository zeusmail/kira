/* This file is a part of the program Kira.
 * Copyright (C) Johann Usovitsch <jusovitsch@googlemail.com>
 * Philipp Maierhoefer <particle@maierhoefer.net>
 * Peter Uwer <peter.uwer@physik.hu-berlin.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version , or (at
 * your option) any later version as published by the Free Software 
 * Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
*/
#include <config.h>
#include <getopt.h>
#include "kira.h"
#include "tools.h"

using namespace std;

static Loginfo & logger = Loginfo::instance();

class ParseOptions {
public:
  ParseOptions(int argc, char *argv[]){

    silent_flag = 0;
//     algebra_flag = -1;
    pyred_config = "";
//     algebra_config = "";
    parallel = 1;
    integralOrdering = 9;
    int c;

    static struct option long_options[] = {
      {"help",    no_argument, 0, 'h'},
      {"version", no_argument, 0, 'v'},
      {"silent",  no_argument, &silent_flag, 1},
//       {"algebra", optional_argument, 0, 'a'},
//       {"algebra", required_argument, 0/*&algebra_flag*/, 'a'},
//       {"algebra", no_argument, &algebra_flag, 1},
      {"parallel", required_argument, 0, 'p'},
      {"integral_ordering", required_argument, 0, 'i'},
      {"pyred_config", required_argument, 0, 'c'},
      {0, 0, 0, 0} // mark end of array
    };
    
//     static struct option long_options_overload[] = {
//       {"algebra", required_argument, 0, 'a'},
//       {0, 0, 0, 0} // mark end of array
//     };

    while (1) {
      {
	int option_index=0;
	std::string tmpOptarg;
// 	opterr = 0;
	c = getopt_long_only(argc, argv, "hv:a:p:i:c:", long_options, &option_index);
	
	/* Detect the end of the options. */
	if (c == -1)
	  break;
	
	switch (c){
	case 0:
	  /* If this option set a flag, do nothing else now. */
	  if (long_options[option_index].flag != 0)
	    break;
	  printf ("option %s", long_options[option_index].name);
	  if (optarg)
	    printf (" with arg %s", optarg);
	  printf ("\n");
	  break;
	case 'h':
	  PrintUsage();
	  exit(0);
	case 'v':
	  logger << "Kira version: " << VERSION << "\n";
	  exit(0);
// 	case 'a':
// 	  if(optarg){
// 	    tmpOptarg = optarg;
// 	    if(tmpOptarg == "true" || tmpOptarg == "false" || tmpOptarg == "auto")
// 	      algebra_config += optarg;
// 	    else{
// 	      logger << "The option --algebra is set with a wrong argument.";
// 	      logger << "\n";
// 	      logger << "The allowed options are auto, false, true or no argument.";
// 	      logger << "\n";
// 	      logger << "No argument will set the option --algebra to auto.";
// 	      logger << "\n";
// 	      exit(-1);
// 	    }
// 	  }
// 	  else
// 	    algebra_config += "auto";
// 	  logger << "The option --algebra is set to: " << algebra_config << "\n";
// 	  break;
	case 'p':
	  parallel = atoi(optarg);
	  if(0 < parallel)
	    logger << "Kira will start "<<parallel <<" different Fermat jobs in parallel."<< "\n";
	  else{
	    logger << "The option parallel is invalid."<< "\n";
	    exit(-1);
	  }
	  break;
	case 'i':
	  integralOrdering = atoi(optarg);
	  if(1 > integralOrdering || integralOrdering > 8){
	    logger << "The option integral_ordering is invalid."<< "\n";
	    exit(-1);
	  }
	  break;
	case 'c':
	  pyred_config += optarg;
	  break;
	case '?':
	default:
	  PrintUsage();
	  abort ();
	}
      }
//       {
// 	int option_index=0;
// 
// 	c = getopt_long_only(argc, argv, ":a:", long_options_overload, &option_index);
// 
// 	/* Detect the end of the options. */
// 	if (c == -1)
// 	  break;
// 	
// 	switch (c){
// 	case 0:
// 	  /* If this option set a flag, do nothing else now. */
// 	  if (long_options[option_index].flag != 0)
// 	    break;
// 	  printf ("option %s", long_options[option_index].name);
// 	  if (optarg)
// 	    printf (" with arg %s", optarg);
// 	  printf ("\n");
// 	  break;
// 	case 'a':
// 	  pyred_config += optarg;
// 	  break;
// 	case 'c':
// 	  pyred_config += optarg;
// 	  break;
// 	case '?':
// 	default:
// 	  PrintUsage();
// 	  abort ();
// 	}
//       }
    }
    
    /* Print any remaining command line arguments (not options). */
    if ( optind != (argc-1) ){
      logger << "\n" << "\n";
      logger << "Unexpected number of command line arguments,\n";
      logger << "expecting the name of the job file + options.\n";
      PrintUsage();
      logger << "Quit program.\n";
      logger << "\n" << "\n";
      exit(-1);
    }

    if ( 0 == file_exists(argv[optind]) ){
      logger << "\n" << "\n";
      logger << "Can not find job file: " << job_name << "\n";
      logger << "Quit program.\n";
      logger << "\n" << "\n";
      exit(-1);
    }

    job_name = argv[optind];

    if (silent_flag){
      logger.silent_cout_buf();
    }
  }

  ~ParseOptions(){
    if (silent_flag){
      logger.restore_silence(); // restore the original stream buffer
    }
  }
  
  int getCores(){
    return(parallel);
  }
  
  int get_integral_ordering(){
    return(integralOrdering);
  }
  
  std::string getPyred(){
    return(pyred_config);
  }
  
//   std::string getAlgebra(){
//     return(algebra_config);
//   }
  
  string getJobname(){
    return(job_name);
  }

  void PrintUsage(){
    logger << "\n";
    logger << "Usage:" <<"\n";
    logger << "  kira FILE        Runs the jobs specified in ";
    logger << "the job file FILE." << "\n";
    logger << "                   The project directory is set"
	 << " to the working directory." << "\n";
    logger << " Options:" << "\n";
    logger << "   --version    Prints the Kira version."<<"\n";
    logger << "   --help       Prints this help."<<"\n";
    logger << "   --silent     Switch to silent mode." << "\n";
//     logger << "   --algebra    This option is always recommended if one";
//     logger << "\n";
//     logger << "                expects coefficients depending on more";
//     logger << "\n";
//     logger << "                than three variables.";
//     logger << "\n";
    logger << "   --parallel=n Kira will invoke n times Fermat.";
    logger << "\n";
    logger << "   --integral_ordering = n Kira will use n th integral ordering.";
    logger << "\n";
    logger << "                You can choose between 1,2,3,4,...,8.";
    logger << "\n";
//    logger << "   --numerical=n Kira will choose n th numerical solver.";
//    logger << "\n";
 //   logger << "                Allowed values for n are 0,1,2.";
    logger << "\n"<<"\n";
  }

private:
  int silent_flag;
//   , algebra_flag;
  string job_name;
  ofstream fout;
  int parallel;
  int integralOrdering;
  std::string pyred_config;
//   std::string algebra_config;
  ParseOptions(){};
};

int main(int argc, char *argv[]){
  
  Clock clock;
  
  logger.set_logfile("kira.log");
  
  ParseOptions parseoptions(argc, argv);
  
  Kira kira(parseoptions.getJobname(),parseoptions.getCores()/*,parseoptions.getAlgebra()*/,parseoptions.getPyred(), parseoptions.get_integral_ordering());
  kira.read_config();
  kira.execute_jobs();
  
  logger << "Total time:\n( " << clock.eval_time() << " s )\n";
  
  return 0;

}
