#pragma once

#include "interface.h"

#include <Reconstructor.hpp>
#include <ShuntingYardParser.hpp>

//TODO make bunched evaluation faster. Currently too slow to be useful. Although the per thread performance is better, the overhead is still too large for medium sized interpolations (tested up to deg <= 80).
class BlackBoxKira : public firefly::BlackBoxBase {
public:
 BlackBoxKira(std::vector<std::string> symbols_, const std::vector<uint64_t>& mandatory_vec, uint32_t bunch_size_ = 1) : symbols(symbols_) {
    for (const auto id : mandatory_vec) {
      mandatory.insert(mandatory.end(), id);
    }

    bunch_size = bunch_size_;
  }

  virtual ~BlackBoxKira() = default;

  virtual std::vector<firefly::FFInt> operator()(const std::vector<firefly::FFInt>& values) {
    //std::vector<std::pair<std::string, uint64_t>> randvars;

    /*for (uint32_t i = 0; i != symbols.size(); ++i) {
      randvars.emplace_back(std::make_pair(symbols[i], values[i].n));
      }*/

    std::vector<std::pair<uint64_t, uint64_t>> assignment_tmp {};
    std::unordered_map<uint64_t, int> equation_lengths_tmp {};
    std::vector<double> times {};

    std::vector<firefly::FFInt> result = solve(values,
					       mandatory,
					       assignment_tmp,
					       equation_lengths_tmp,
					       times);

    {
      std::unique_lock<std::mutex> lock(mut);

      ++iteration;

      if (iteration == 1) {
        assignment = std::move(assignment_tmp);
        equation_lengths = std::move(equation_lengths_tmp);

        parse_average = times[0];
        forward_average = times[1];
        back_average = times[2];
      } else {
        parse_average = (parse_average * (iteration - 1) + times[0]) / iteration;
        forward_average = (forward_average * (iteration - 1) + times[1]) / iteration;
        back_average = (back_average * (iteration - 1) + times[2]) / iteration;
      }
    }

    return result;
  }

#ifdef FFDEV
  virtual std::vector<std::vector<firefly::FFInt>> operator()(const std::vector<std::vector<firefly::FFInt>>& values) {
    std::vector<std::pair<uint64_t, uint64_t>> assignment_tmp {};
    std::unordered_map<uint64_t, int> equation_lengths_tmp {};
    std::vector<double> times {};

    std::vector<std::vector<firefly::FFInt>> result = solve(values,
							    mandatory,
							    assignment_tmp,
							    equation_lengths_tmp,
							    times);

    {
      std::unique_lock<std::mutex> lock(mut);
      
      ++iteration;
      
      if (iteration == 1) {
        assignment = std::move(assignment_tmp);
        equation_lengths = std::move(equation_lengths_tmp);
	
        parse_average = times[0];
        forward_average = times[1];
        back_average = times[2];
      } else {
        parse_average = (parse_average * (iteration - 1) + times[0]) / iteration;
        forward_average = (forward_average * (iteration - 1) + times[1]) / iteration;
        back_average = (back_average * (iteration - 1) + times[2]) / iteration;
      }
    }
    
    return result;
  }
#endif
  
  virtual void prime_changed() {
    for(auto& el : system) {
      el.first.precompute_tokens();
    }
  }

  void add_eqn(const firefly::ShuntingYardParser& par, const std::vector<uint64_t>& ids) {
    system.emplace_back(std::make_pair(par, ids));
  }

private:
  friend class Kira;

  std::vector<std::string> symbols;
  std::vector<std::pair<firefly::ShuntingYardParser, std::vector<uint64_t>>> system;
  std::set<uint64_t> mandatory;
  std::vector<std::pair<uint64_t, uint64_t>> assignment;
  std::unordered_map<uint64_t, int> equation_lengths;
  std::mutex mut;
  uint32_t iteration = 0;
  double parse_average = 0;
  double forward_average = 0;
  double back_average = 0;
  uint32_t bunch_size = 1;

  std::vector<firefly::FFInt> solve(const std::vector<firefly::FFInt>& values,
				    const std::set<uint64_t>& mandatory,
				    std::vector<std::pair<uint64_t, uint64_t>>& assignment,
				    std::unordered_map<uint64_t, int>& equation_lengths,
				    std::vector<double>& times) {
    auto time0 = std::chrono::high_resolution_clock::now();

    auto numsys = pyred::SystemOfEqs<firefly::FFInt>(system, values);

    auto time1 = std::chrono::high_resolution_clock::now();

    numsys.solve();
    numsys.truncate();

    auto time2 = std::chrono::high_resolution_clock::now();

    std::vector<firefly::FFInt> result = backward(numsys, mandatory, assignment, equation_lengths);

    auto time3 = std::chrono::high_resolution_clock::now();

    times = {std::chrono::duration<double>(time1 - time0).count(), std::chrono::duration<double>(time2 - time1).count(), std::chrono::duration<double>(time3 - time2).count()};

    return result;
  }

#ifdef FFDEV
  std::vector<std::vector<firefly::FFInt>> solve(const std::vector<std::vector<firefly::FFInt>>& values,
                                    const std::set<uint64_t>& mandatory,
                                    std::vector<std::pair<uint64_t, uint64_t>>& assignment,
                                    std::unordered_map<uint64_t, int>& equation_lengths,
                                    std::vector<double>& times) {
    auto time0 = std::chrono::high_resolution_clock::now();

    auto numsys = pyred::SystemOfEqs<firefly::FFIntVec>(system, values);

    auto time1 = std::chrono::high_resolution_clock::now();

    numsys.solve();
    numsys.truncate();

    auto time2 = std::chrono::high_resolution_clock::now();

    std::vector<std::vector<firefly::FFInt>> result = backward(numsys, mandatory, assignment, equation_lengths);

    auto time3 = std::chrono::high_resolution_clock::now();

    times = {std::chrono::duration<double>(time1 - time0).count(), std::chrono::duration<double>(time2 - time1).count(), std::chrono::duration<double>(time3 - time2).count()};

    return result;
  }

#endif
  std::vector<firefly::FFInt> backward(pyred::SystemOfEqs<firefly::FFInt>& numsys,
				       const std::set<uint64_t>& mandatory,
				       std::vector<std::pair<uint64_t, uint64_t>>& assignment,
				       std::unordered_map<uint64_t, int>& equation_lengths) {
    if (!mandatory.empty()) {
      std::set<uint64_t> mandatory_tmp = mandatory;
      std::vector<pyred::Equation<firefly::FFInt>> new_sys_reverse;
      
      for (auto it = numsys.sys.rbegin(); it != numsys.sys.rend(); ++it) {
	auto found = mandatory_tmp.find(it->front().first);
	
	if (found != mandatory_tmp.end()) {
          for (auto itt = ++(it->eq.begin()); itt != it->eq.end(); ++itt) {
            mandatory_tmp.insert(itt->first);
          }
	  
	  new_sys_reverse.emplace_back(std::move(*it));
	}
      }
      
      numsys.sys.clear();
      numsys.sys.shrink_to_fit();
      numsys.sys.reserve(new_sys_reverse.size());
      
      for (auto it = new_sys_reverse.rbegin(); it != new_sys_reverse.rend(); ++it) {
	numsys.sys.emplace_back(std::move(*it));
      }
    }
    
    numsys.solve();
    
    std::vector<firefly::FFInt> result {};
    
    if (!mandatory.empty()) {
      for (auto it = numsys.sys.rbegin(); it != numsys.sys.rend(); ++it) {
	auto found = mandatory.find(it->front().first);
	
	if (found != mandatory.end()) {
	  equation_lengths.emplace(std::make_pair(it->front().first, it->size()));
	  
	  for (auto itt = ++(it->eq.begin()); itt != it->eq.end(); ++itt) {
            assignment.emplace_back(std::make_pair(it->front().first, itt->first));
            result.emplace_back(itt->second);
	  }
	}
	
	it->eq.clear();
      }
    } else {
      for (auto it = numsys.sys.rbegin(); it != numsys.sys.rend(); ++it) {
	equation_lengths.emplace(std::make_pair(it->front().first, it->size()));
	
        for (auto itt = ++(it->eq.begin()); itt != it->eq.end(); ++itt) {
          assignment.emplace_back(std::make_pair(it->front().first, itt->first));
          result.emplace_back(itt->second);
        }
	
	it->eq.clear();
      }
    }
    
    numsys.sys.clear();
    
    return result;
  }

#ifdef FFDEV
  std::vector<std::vector<firefly::FFInt>> backward(pyred::SystemOfEqs<firefly::FFIntVec>& numsys,
						    const std::set<uint64_t>& mandatory,
						    std::vector<std::pair<uint64_t, uint64_t>>& assignment,
						    std::unordered_map<uint64_t, int>& equation_lengths) {
    if (!mandatory.empty()) {
      std::set<uint64_t> mandatory_tmp = mandatory;
      std::vector<pyred::Equation<firefly::FFIntVec>> new_sys_reverse;
      
      for (auto it = numsys.sys.rbegin(); it != numsys.sys.rend(); ++it) {
	auto found = mandatory_tmp.find(it->front().first);
	
	if (found != mandatory_tmp.end()) {
          for (auto itt = ++(it->eq.begin()); itt != it->eq.end(); ++itt) {
            mandatory_tmp.insert(itt->first);
          }
	  
	  new_sys_reverse.emplace_back(std::move(*it));
	}
      }
      
      numsys.sys.clear();
      numsys.sys.shrink_to_fit();
      numsys.sys.reserve(new_sys_reverse.size());
      
      for (auto it = new_sys_reverse.rbegin(); it != new_sys_reverse.rend(); ++it) {
	numsys.sys.emplace_back(std::move(*it));
      }
    }
    
    numsys.solve();
    
    std::vector<std::vector<firefly::FFInt>> result (bunch_size);
    
    if (!mandatory.empty()) {
      for (auto it = numsys.sys.rbegin(); it != numsys.sys.rend(); ++it) {
	auto found = mandatory.find(it->front().first);
	
	if (found != mandatory.end()) {
	  equation_lengths.emplace(std::make_pair(it->front().first, it->size()));
	  
	  for (auto itt = ++(it->eq.begin()); itt != it->eq.end(); ++itt) {
            assignment.emplace_back(std::make_pair(it->front().first, itt->first));
	    for(std::size_t i = 0; i != bunch_size; ++i){
	      result[i].emplace_back(itt->second.vec[i]);
	    }
	  }
	}
	
	it->eq.clear();
      }
    } else {
      for (auto it = numsys.sys.rbegin(); it != numsys.sys.rend(); ++it) {
	equation_lengths.emplace(std::make_pair(it->front().first, it->size()));
	
        for (auto itt = ++(it->eq.begin()); itt != it->eq.end(); ++itt) {
          assignment.emplace_back(std::make_pair(it->front().first, itt->first));
	  for(std::size_t i = 0; i != bunch_size; ++i){
	    result[i].emplace_back(itt->second.vec[i]);
	  }
        }
	
	it->eq.clear();
      }
    }
    
    numsys.sys.clear();
    
    return result;
  }
#endif
};
