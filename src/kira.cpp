/* This file is a part of the program Kira.
 * Copyright (C) Johann Usovitsch <jusovitsch@googlemail.com>
 * Philipp Maierhoefer <particle@maierhoefer.net>
 * Peter Uwer <peter.uwer@physik.hu-berlin.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version , or (at
 * your option) any later version as published by the Free Software 
 * Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
*/

//jule is the number of propagators
#include "yaml-cpp/yaml.h"
#include "sort_rules.h"
#include <fstream>
#include "kira.h"
#include "integral.h"
#include "ReadYamlFiles.h"
#include "tools.h"
#include "sqlite3/sqlite3.h"
#include "dataBase.h"
#include <sys/stat.h>
#include "interface.h"
#include <pthread.h>

#include <gzstream.h>

#include "integrals.h"

#include <algorithm>

using namespace pyred;
  
using namespace std;
using namespace GiNaC;
using namespace YAML;

static Loginfo & logger = Loginfo::instance();

Kira::Kira (string name, int coreNumber_/*,std::string algebra_*/, string pyred_config_, int integralOrdering_) {
  
  jobName = name;
  coreNumber = coreNumber_;
  
  pyred_config = pyred_config_;
  integralOrdering = integralOrdering_;
};

void Kira::select_initial_integrals(std::vector<SeedSpec>& initiateSOE){
  
  pyred::Config::symlimits(std::numeric_limits<int>::max(),std::numeric_limits<int>::max());


  for(auto coutI: integralfamily.reductSpec){
    
    for(auto itSector: get<0>(coutI)){
      
      int dmax = -1;
      
      if(numeric_limits<int>::max() != get<3>(coutI))
	dmax = get<3>(coutI);
      
      initiateSOE.push_back(Topology::id_to_topo(integralfamily.topology)->seed_spec(itSector, get<1>(coutI), get<2>(coutI), dmax, -1, SeedSpec::Recursive::full));
      
    }
  }
  
  for(auto coutI: integralfamily.reductSpec){

    for(auto itSector: get<0>(coutI)){

      for(int it = 0; it < (1<<integralfamily.jule)+1; it++){

	if((itSector & it) == it){

	  if(integralfamily.symVec[it].size() != 0){

	    int num = get<2>(coutI);
	    int dmax = -1;

// 	    if(get<2>(coutI) > 2)
// 	      num = 2;

	    if(numeric_limits<int>::max() != get<3>(coutI))
	      dmax = get<3>(coutI);

	    initiateSOE.push_back(Topology::id_to_topo(integralfamily.symVec[it][0].topology)->seed_spec(integralfamily.symVec[it][0].sector, get<1>(coutI), num, dmax, -1));
	  }
	}
      }
    }
  }
}

void Kira::complement_initial_integrals(std::vector<SeedSpec>& complementSOE){


//   for(unsigned itC = 0; itC < integralfamily.topology+1; itC++) {
    for(auto coutI: integralfamily.reductSpec){

      for(auto itSector: get<0>(coutI)){

        for(int it = 0; it < (1<<integralfamily.jule)+1; it++){

          if((itSector & it) == it){

            if(integralfamily.symVec[it].size() != 0 && integralfamily.symVec[it][0].symDOTS == 0){

              int num = get<2>(coutI);
              int dmax = -1;

//   	      if(get<2>(coutI) > 2)
//                 num = 2;

              if(numeric_limits<int>::max() != get<3>(coutI))
                dmax = get<3>(coutI);

              complementSOE.push_back(Topology::id_to_topo(integralfamily.topology)->seed_spec(it, get<1>(coutI), num, dmax, -1, SeedSpec::Recursive::dotsp));
  //             cout << integralfamily.topology << " " << it << " : " << integralfamily.symVec[it][0].sector  << " : " << integralfamily.symVec[it][0].symDOTS << endl;

            }
          }
        }
      }
    }
//   }
}


bool sort_rules7(eqdata& l, eqdata& r) {
  
  if( l[0].first < r[0].first )
    return true;
  else if( l[0].first > r[0].first )
    return false;
  
  unsigned length = 0;
  if(l.size() < r.size())
    length = l.size();
  else
    length = r.size();
  
  if(length > 1){
    if( l[0].first < r[0].first )
      return true;
    else if ( l[0].first > r[0].first )
      return false;
  }
  
  
  if(l.size() < r.size())
    return true;
  else if( l.size() > r.size() )
    return false;
  
  for(unsigned it = 0; it < l.size(); it++){
    if( l[it].first < r[it].first )
      return true;
    else if ( l[it].first > r[it].first )
      return false;
  }
  
  return false;
  
};

void Kira::write_seeds_to_disk(vector<std::uint64_t>& mandatory){
  
  if(dataFile == 1){
    
    ofstream id2int;
  
    id2int.open((outputDir+"/results/"+integralfamily.name+"/id2int").c_str());
  
    logger << "\n*****Write seeds to hard disk***************************\n";
    
    for(auto ItSeed = mandatory.begin(); ItSeed != mandatory.end(); ItSeed++){        
      
      auto iglback = pyred::Integral(*ItSeed);
      
      auto property = iglback.properties(*ItSeed);
      
      id2int << "- [" << *ItSeed << ",";
      for(int itt = 0; itt < integralfamily.jule; itt++)
	id2int << iglback.m_powers[itt] << ",";
      id2int << property.sector << ",";
      id2int << property.topology << ",";
      id2int << property.lines << ",";
      id2int << property.dots << ",";
      id2int << property.sps << ",";
      id2int << "0";
      id2int << "]\n";
    }
  
    id2int.close();
  }
}


void copyStream(string & file, string & content){
  rename(file.c_str(),(file+"B").c_str());
  
  igzstream inputGZ((file+"B").c_str());
  
  int iterFILE;

  for(iterFILE=0 ; inputGZ.eof()!=true ; iterFILE++) 
      content += inputGZ.get();

  iterFILE--;
  content.erase(content.end()-1);
  
  inputGZ.close();
  remove((file+"B").c_str());
	  
}


class EquationGetter {
private:
  // Using a (copyable) pointer here is essential, because when an instance
  // of EquationGetter is bound to a std::function object, the EquationGetter
  // is copied. I.e. if we had a std::vector<eqdata> member instead,
  // the equations would be inserted into a copy of the system which is not
  // accessible through the EquationGetter instance.
  std::shared_ptr<eqdata> eq;
  std::shared_ptr<ogzstream> output;
//   std::shared_ptr<ofstream> TMPfile;
  std::shared_ptr<uint64_t> countT;
  std::string outputDir;
  std::string integralfamilyName; 
  std::vector<std::string> collectReductions;
  std::unordered_map<std::uint64_t,std::uint64_t> mastersMap;
  std::tuple<unsigned,unsigned> printControl;
  std::string collectReductions2;
public:
  
  EquationGetter(std::string& outputDir_, 
		std::string& integralfamilyName_,
		std::vector<std::string>& collectReductions_,
		std::unordered_map<std::uint64_t,std::uint64_t>& mastersMap_
		): 
		eq{std::make_shared<eqdata>()},
		output{std::make_shared<ogzstream>()},
// 		TMPfile{std::make_shared<ofstream>()},
		countT{std::make_shared<uint64_t>()},
		outputDir{outputDir_},
		integralfamilyName{integralfamilyName_},
		collectReductions{collectReductions_},
		mastersMap{mastersMap_}
  {
    get<0>(printControl) = numeric_limits<unsigned>::max();
    get<1>(printControl) = numeric_limits<unsigned>::max();
    (*countT) = 0;
  }
  
  eqdata &getEquations() {return *eq;}
  
  void operator()(eqdata &&eq_) {
    (*eq)=std::move(eq_);
    
    for (std::size_t i = 0; i != (*eq).size(); ++i) {
      
      for (std::size_t j = i+1; j != (*eq).size(); ++j) {
	
	if(((*eq).begin()+i)->first < ((*eq).begin()+j)->first){
	  
	  iter_swap((*eq).begin()+i,(*eq).begin()+j);
	}
      }
    }
    std::tuple<std::string,unsigned,unsigned> integral;
    get_properties((*eq)[0].first, integral);
    
    if (get<2>(integral) != get<0>(printControl) || get<1>(integral) != get<1>(printControl)){
      
      get<1>(printControl) = get<1>(integral);
      get<0>(printControl) = get<2>(integral);
      
      (*output).close();
      
      
      if(file_exists((outputDir+"/tmp/"+integralfamilyName+"/SYSTEM_"+collectReductions[get<1>(integral)]+"_"+to_string(get<2>(integral))+".gz").c_str())){
	
	string content = "";
	
	string fileName=(outputDir+"/tmp/"+integralfamilyName+"/SYSTEM_"+collectReductions[get<1>(integral)]+"_"+to_string(get<2>(integral))+".gz");
	
	copyStream(fileName,content);
	
	(*output).open ((outputDir+"/tmp/"+integralfamilyName+"/SYSTEM_"+collectReductions[get<1>(integral)]+"_"+to_string(get<2>(integral))+".gz"	).c_str());
	
	(*output) << content;
	
      }
      else{
	
	(*output).open ((outputDir+"/tmp/"+integralfamilyName+"/SYSTEM_"+collectReductions[get<1>(integral)]+"_"+to_string(get<2>(integral))+".gz").c_str());
      }
      
//       (*TMPfile).close();
//       (*TMPfile).open ((outputDir+"/tmp/"+integralfamilyName+"/userDefinedSystem").c_str(),std::ofstream::app);
      
    }
    
    (*output) << "Eq" << "\n";
    (*output) << (*eq)[0].first << "\n";
    (*output) << (*eq).size() << "\n";
    (*output) << (*eq).size() << " ";
    (*output) << (*eq)[0].second << " ";
    (*output) << (*eq)[0].first << " ";
    (*output) << get<2>(integral) << " ";
    (*output) << get<1>(integral) << " ";
    (*output) << 0 << "\n";
    
//     (*TMPfile) << "topo7[" << get<0>(integral)<<"]*(";
//     (*TMPfile) << (*eq)[0].second <<")\n";
    
    for (std::size_t i = 1; i != (*eq).size(); ++i) {
      
      get_properties((*eq)[i].first, integral);
      
      (*output) << (*eq).size() << " ";
      (*output) << (*eq)[i].second << " ";
      (*output) << (*eq)[i].first << " ";
      (*output) << get<2>(integral) << " ";
      (*output) << get<1>(integral) << " ";
      (*output) << 0 << "\n";
      
//       (*TMPfile) << "topo7[" << get<0>(integral)<<"]*(";
//       (*TMPfile) << (*eq)[i].second <<")\n";

    }
//     (*TMPfile) << "\n";
    (*countT)++;
  };
  
  void finalize(){
    
    (*output).close();
//     (*TMPfile).close();
    ofstream outputConfig;
    outputConfig.open ((outputDir+"/tmp/"+integralfamilyName+"/SYSTEMconfig").c_str());
    outputConfig << (*countT) << endl;
    outputConfig.close();
    logger << "\nNumber of selected equations to reduce: " << (*countT) << " equations\n";
  };
};

Fermat *Kira::fermat;
std::string Kira::treatcoeff2(const std::string& str_) {
  
  string str = str_;
  size_t found;
  
  if((found = str.find_first_of("+")) != string::npos && found == 0){
    str.erase(str.begin()+found,str.begin()+found+1);
  }
  
  while((found = str.find("(+")) != string::npos){
    str.erase(str.begin()+found+1,str.begin()+found+2);
  }
  
  /*Do d replacement d -> 16*/
  
//   char var[100];
// 
//   var[0]='\0';
//   sprintf(var,"%s\n","d");
//   fermat[0].unset_variable(var);
//   fermat[0].set_numeric(var,16);


  fermat[0].fermat_collect( const_cast<char*>(str.c_str()) );
    
  fermat[0].fermat_calc();
  
  
//   var[0]='\0';
//   sprintf(var,"%s\n","d");
//   fermat[0].unset_numeric(var);
//   fermat[0].set_variable(var);
  
  
  return fermat[0].g_baseout;
}

void Kira::generate_SOE(int flagOTF, std::vector<std::uint64_t> &selected_integrals){
  Clock clock;

  // Best performance with several threads: cache_level=2, auto_clear_cache_level=2
  // (single threaded may be faster with cache_level=2, auto_clear_cache_level=0).
  int cache_level{2};
  int auto_clear_cache_level{2};
  // cache_level: 0=(no cache), 1=(weight to integral only),
  //              2=(integral to weight only), 3=(both)
  Integral::use_cache(cache_level);
  Integral::auto_clear_cache(auto_clear_cache_level);
  
  
//   select integrals
  std::vector<SeedSpec> initiateSOE;
  select_initial_integrals(initiateSOE);

//   complement integrals
  std::vector<SeedSpec> complementSOE;
  complement_initial_integrals(complementSOE);
  
//   Initiate system without symmetries
//   auto sys = pyred::System(initiateSOE,
//      {Topology::id_to_topo(0)->seed_spec(0,1,1)}/*, initiateSYMOE*/);
  
//   Initiate system with symmetries
  auto sys = System();

  auto indep_eqnums = sys.generate_solve(initiateSOE, complementSOE, {});
  
//   auto sys = pyred::System(initiateSOE, {}/*initiateSYMOE*/);  
  
  logger << IntegralRelations::cache<Coeff_int>().size();
  logger << " independent coefficients\n";
  
    
  const std::function<std::string(const std::string&)> treatcoeff = treatcoeff2;
  
  EquationGetter treateq{outputDir, integralfamily.name,
		collectReductions, mastersMap};
  
//   Regenerate the system because we need unmodified equations to extract.
  logger << "Regenerate:\n";
  
  if(!OTF){
    std::pair<std::vector<intid>,std::vector<intid> > selection;
    
    selection = sys.select(selected_integrals, {});
    
    auto masters = selection.second;
//     logger << masters.size() << " master integrals\n";
    
    string pathMI = outputDir+"/results/"+integralfamily.name+"/masters";
    ofstream myfile;
    myfile.open (pathMI.c_str());
    
    logger <<"\nNumber of master integrals: " << masters.size()<<"\n";
    
    for( auto itg : masters){
      
//       auto mapContent = mastersReMap.find(itg);
//       
//       if(mapContent != mastersReMap.end())      
// 	itg = mapContent->second;

      tuple<string,unsigned,unsigned> integral;
      get_properties((itg),integral);
      logger << (itg) << ": ";
      logger << collectReductions[get<1>(integral)] << ": ";
      logger << get<0>(integral) << ": ";
      logger << get<2>(integral);
      logger << "\n";
      
      myfile << (itg) << ": ";
      myfile << collectReductions[get<1>(integral)] << ": ";
      myfile << get<0>(integral) << ": ";
      myfile << get<2>(integral);
      myfile << "\n";
    }
    
    sys.generate_retrieve(std::move(selection.first), treateq, treatcoeff);
  }
  else{
    
    vector<uint64_t> initial_seeds;
    
    for(size_t it = 0; it < sys.size(); it++)
      initial_seeds.push_back(it);
    sys.generate_retrieve(std::move(initial_seeds), treateq, treatcoeff);
    
  }
  treateq.finalize();
  
  if(writeNumericalSystem){
    auto& content = sys.reduction_content();
    
    ofstream output((outputDir+"/results/"+integralfamily.name+"/reducedEqs").c_str());
    
    for (auto itContent = content.begin(); itContent != content.end(); ++itContent) {
      
      output << "integral ID: " << ((*itContent).first) << " " <<"\n";
      output << "masters ID: ";
      
      auto vecTupel = get<1>((*itContent).second);
      
      for (auto iter = vecTupel.begin(); iter != vecTupel.end(); ++iter) {
	output << (*iter) << " ";
      }
      output << "\n";
    }
    
    output.close();
  }
    
  logger << "( " << clock.eval_time() << " s )\n";
}


void Kira::run_pyred(std::vector<std::uint64_t>& mandatory, 
                     std::vector<std::uint64_t>& optional, pyred::System& sys){
  
  Clock clock;
  logger << "\n***** Starting PYRED **************************************\n";
  
  logger << "mandatory: " << mandatory.size() << "\n";
  
  for (const auto &topoptr: pyred::Topology::get_topologies()) {
    logger << "Defined topology " << topoptr->name() << "\n";
  }
  
  auto indep_eqnums = sys.solve();
  
  std::vector<pyred::eqdata> equations;
  
  if(mandatory.size()==0){
  
    equations = sys.retrieve(std::move(indep_eqnums));
  }
  else{
    
    auto selection = sys.select(mandatory, optional);
    equations = sys.retrieve(std::move(selection.first));
    
    auto masters = selection.second;
    string pathMI = outputDir+"/results/"+integralfamily.name+"/masters";
    ofstream myfile;
    myfile.open (pathMI.c_str());
    
    logger <<"\nNumber of master integrals: " << masters.size()<<"\n";
    
    for( auto itg : masters){
      
      tuple<string,unsigned,unsigned> integral;
      get_properties((itg),integral);
      logger << (itg) << ": ";
      logger << collectReductions[get<1>(integral)] << ": ";
      logger << get<0>(integral) << ": ";
      logger << get<2>(integral);
      logger << "\n";
      
      myfile << (itg) << ": ";
      myfile << collectReductions[get<1>(integral)] << ": ";
      myfile << get<0>(integral) << ": ";
      myfile << get<2>(integral);
      myfile << "\n";
    }
  }
  
  ogzstream output;
  uint64_t countT;
  std::string integralfamilyName; 
  std::tuple<unsigned,unsigned> printControl;
  
  get<0>(printControl) = numeric_limits<unsigned>::max();
  get<1>(printControl) = numeric_limits<unsigned>::max();
  countT = 0;
  
  logger << "integralfamily: " << integralfamily.name << "\n";
  
  for(auto eq: equations){
    
    for (std::size_t i = 0; i != eq.size(); ++i) {
      
      for (std::size_t j = i+1; j != eq.size(); ++j) {
	
	if((eq.begin()+i)->first < (eq.begin()+j)->first){
	  
	  iter_swap(eq.begin()+i,eq.begin()+j);
	}
      }
    }
    std::tuple<std::string,unsigned,unsigned> integral;
    
    get_properties(eq[0].first, integral);
    
    if (get<2>(integral) != get<0>(printControl) || get<1>(integral) != get<1>(printControl)){
      
      get<1>(printControl) = get<1>(integral);
      get<0>(printControl) = get<2>(integral);
      
//       output.close();
      
//       output.open ((outputDir+"/tmp/"+integralfamily.name+"/SYSTEM_"+collectReductions[get<1>(integral)]+"_"+to_string(get<2>(integral))+".gz").c_str());
      
      
      output.close();
      
      
      if(file_exists((outputDir+"/tmp/"+integralfamily.name+"/SYSTEM_"+collectReductions[get<1>(integral)]+"_"+to_string(get<2>(integral))+".gz").c_str())){
	
	string content = "";
	
	string fileName=(outputDir+"/tmp/"+integralfamily.name+"/SYSTEM_"+collectReductions[get<1>(integral)]+"_"+to_string(get<2>(integral))+".gz");
	
	copyStream(fileName,content);
	
	output.open (fileName.c_str());
	
	output << content;
	
      }
      else{
	
	output.open ((outputDir+"/tmp/"+integralfamily.name+"/SYSTEM_"+collectReductions[get<1>(integral)]+"_"+to_string(get<2>(integral))+".gz").c_str());
      }
    }
    
    output << "Eq" << "\n";
    output << eq[0].first << "\n";
    output << eq.size() << "\n";
    output << eq.size() << " ";
    output << eq[0].second << " ";
    output << eq[0].first << " ";
    output << get<2>(integral) << " ";
    output << get<1>(integral) << " ";
    output << 0 << "\n";
    
    for (std::size_t i = 1; i != eq.size(); ++i) {
      
      get_properties(eq[i].first, integral);
      
      output << eq.size() << " ";
      output << eq[i].second << " ";
      output << eq[i].first << " ";
      output << get<2>(integral) << " ";
      output << get<1>(integral) << " ";
      output << 0 << "\n";
    }
    countT++;
  }
    
  output.close();
  ofstream outputConfig;
  outputConfig.open ((outputDir+"/tmp/"+integralfamily.name+"/SYSTEMconfig").c_str());
  outputConfig << countT << endl;
  outputConfig.close();
  logger << "\nNumber of selected equations to reduce: " << countT << " equations\n";

  
  logger << "( " << clock.eval_time() << " s )\n";
};


int Kira::complete_reduction(){
  
  DataBase * database = new DataBase(outputDir+"/results/kira.db");
  database->create_equation_table();
  delete database;
  
  string masterFileName = outputDir+"/results/"+integralfamily.name+"/masters";
  ifstream input;
  
  vector<tuple<std::uint64_t,string,string,string,string> > masterVector;
  
  if(file_exists(masterFileName.c_str())){  
    input.open(masterFileName.c_str());
    vector <tuple<string> > m;
    while(1){
      std::uint64_t ID;
      string tmp1, tmp2, tmp3, tmp4;
      if(!(input >>ID)) break;
      if((input >>tmp1))
	;
      if((input >>tmp2))
	;
      if((input >>tmp3))
	;
      if((input >>tmp4))
	;
      
      masterVector.push_back(make_tuple(ID,tmp1,tmp2,tmp3,tmp4));
    }
    input.close();
  }
  
  auto ret = selectMastersReduction.equal_range(integralfamily.name);
  
  if (ret.first != selectMastersReduction.end()){
    
    for (auto itRet=ret.first; itRet!=ret.second; ++itRet){
        
      logger << "\n***** Run the back substitution ***********************\n\n";
  
      logger << "Set these master integrals to zero:\n";
      
      for(unsigned i = 0; i < masterVector.size(); i++){
          
        auto findMaster = find( (itRet->second).begin(),(itRet->second).end(), i+1 );
        
        if( findMaster == (itRet->second).end() ){
          
          masterVectorSkip.push_back( get<0>(masterVector[i]) );
          logger <<get<0>(masterVector[i]);
	  logger <<get<1>(masterVector[i]);
	  logger <<get<2>(masterVector[i]);
	  logger <<get<3>(masterVector[i]);
	  logger <<get<4>(masterVector[i]);
	  logger << "\n";
        }
      }
      
      if( !load_back_substitution() )
      {
	return 0;
      }
      back_subs();
      clean_back_subs();
    }
  }
  else{
    logger << "\n***** Run the back substitution ***********************\n\n";
    
    if( !load_back_substitution() ){
      return 0;
    }
    back_subs();
    clean_back_subs();
  }
  return 1;
}

void Kira::clean_back_subs(){
  
  for ( unsigned it = 0, end = totalReihen+1; it != end; it++) {
    
    if (length[it]) {
      delete [] allEq[it][0];
    }
    if(allEq[it]!=0)
      delete [] allEq[it];
  }
  
  delete [] rdy2P;
  delete [] last_reduce;
  delete [] length;
  delete [] allEq;
  reduct2StartHere.clear();
  occurrence.clear();
  reverseLastReduce.clear();
  masterVectorSkip.clear();
}

void Kira::print_equationSW(BaseIntegral*& integral,ogzstream& output,ofstream& outputX,tuple<int,int,unsigned,int>& printControl,tuple<string,string>& dest,int& rememberID){
  
  if (integral[0].characteristics[SECTOR] != get<0>(printControl) ||integral[0].characteristics[TOPOLOGY] != get<1>(printControl)){
    
    get<0>(printControl) = integral[0].characteristics[SECTOR];
    get<1>(printControl) = integral[0].characteristics[TOPOLOGY];
    
    
    if(get<0>(printControl) != static_cast<int>(get<2>(printControl)) || get<1>(printControl) != get<3>(printControl)){
      
//       cout << "Put equation into the lower sector" << endl;
      output.close();
      outputX.close();
      get<0>(dest) = integralfamily.name+"/SYSTEMx";
      get<1>(dest) = "";
      string name = outputDir+"/tmp/" + get<0>(dest) + "_" + collectReductions[get<1>(printControl)] + "_" + something_string(get<0>(printControl))+get<1>(dest);
      outputX.open (name.c_str(),std::fstream::app);
    }
    else{
      outputX.close();
      output.close();
      get<0>(dest) = integralfamily.name+"/VER";
      get<1>(dest) = ".tmp";
      string name = outputDir+"/tmp/" + get<0>(dest) + "_" + collectReductions[get<1>(printControl)] + "_" + something_string(get<0>(printControl))+get<1>(dest)+".gz";
      output.open (name.c_str());
    }
    
//     output.close();    
//     get<0>(dest) = integralfamily.name+"/VER";
//     get<1>(dest) = "";
//     string name = outputDir+"/tmp/" + get<0>(dest) + "_" + collectReductions[get<1>(printControl)] + "_" + something_string(get<0>(printControl))+get<1>(dest)+".gz";
//     output.open (name.c_str());
//     cout << name << endl;
  }
  
//   cout << "hoho: " <<(outputDir+"/tmp/" + get<0>(dest) + "_" + collectReductions[get<1>(printControl)] + "_" + something_string(get<0>(printControl))+get<1>(dest)+".gz") << endl;
  
  if(get<1>(dest) != ".tmp"){

    integral[0].coefficientString = "-1";
    outputX << "Eq" << "\n";
    outputX << integral[0].id << "\n";
    outputX << integral[0].length << "\n";
    
    for (unsigned j = 0; j < integral[0].length; j++){
      
      integral[j].length = integral[0].length;
      outputX << integral[j];
    }
  }
  else{
    rememberID++;
    output << "Eq" << "\n";
    output << integral[0].id << "\n";
    output << integral[0].length << "\n";
    
    for (unsigned j = 0; j < integral[0].length; j++){
      
      integral[j].length = integral[0].length;
      output << integral[j];
    }
  }
}

void Kira::print_equation(BaseIntegral*& integral,ogzstream& output,ofstream& outputX,tuple<int,int/*,unsigned,int*/>& printControl,tuple<string,string>& dest){
  
  if (integral[0].characteristics[SECTOR] != get<0>(printControl) ||integral[0].characteristics[TOPOLOGY] != get<1>(printControl)){
    
    get<0>(printControl) = integral[0].characteristics[SECTOR];
    get<1>(printControl) = integral[0].characteristics[TOPOLOGY];
    
    output.close(); 
    
    
    get<0>(dest) = integralfamily.name+"/VER";
    get<1>(dest) = "";
    string name = outputDir+"/tmp/" + get<0>(dest) + "_" + collectReductions[get<1>(printControl)] + "_" + something_string(get<0>(printControl))+get<1>(dest)+".gz";
    
    int count = 1;
    while(file_exists(name.c_str())){
      
      name = outputDir+"/tmp/" + get<0>(dest) + "_" + collectReductions[get<1>(printControl)] + "_" + something_string(get<0>(printControl))+get<1>(dest)+"_"+to_string(count++)+".gz";
    }
    
    output.open (name.c_str());
  }

  output << "Eq" << "\n";
  output << integral[0].id << "\n";
  output << integral[0].length << "\n";
  
  for (unsigned j = 0; j < integral[0].length; j++){
    
    integral[j].length = integral[0].length;
    output << integral[j];
  }
}

void Kira::write_pyred(vector<vector<pair<std::uint64_t,string> > > & eqs,/*
  vector<vector<int> > & arraySeed,*/ vector<pyred::intid> & independent_eqnums){
  
  logger << "\nWrite with PYRED reduced system to hard disk \n";
  logger << "(--> "<<outputDir<<"/tmp/NUMconfig, "<<outputDir<<"/tmp/NUM###) \n";
  
  ofstream output;
  
  output.open ((outputDir+"/tmp/"+integralfamily.name+"/NUMconfig").c_str());
  output << eqs[eqs.size()-1][0].first << "\n";
  
  pair<unsigned,unsigned> printControl;
  printControl.first = -1;
  printControl.second = -1;
  
  for(unsigned it = 0; it < eqs.size(); it++){
    std::uint64_t id = eqs[it][0].first;
    
    auto iglback = pyred::Integral(id);
    auto property = iglback.properties(id);
    
    if (property.sector != printControl.first //sector
    || property.topology != printControl.second){ //topology
      printControl.first = property.sector; //sector
      printControl.second = property.topology; //topology
      output.close();
      string name = outputDir+"/tmp/" + integralfamily.name + "/NUM" + "_" + collectReductions[printControl.second] + "_" + something_string(printControl.first);
      output.open (name.c_str());
    }
    output << "Eq" << "\n";
    output << eqs[it][0].first << "\n";
    output << eqs[it].size() << "\n";
  
    for (size_t it2 = 0; it2 < eqs[it].size(); it2++){
      output << eqs[it].size() << " ";
      output << eqs[it][it2].second << " ";
      output << eqs[it][it2].first << " ";
      output << property.sector << " ";
      output << property.topology << " ";
      output << "0" << "\n"; //flag2
    }
  }
  
  output.close();
};

void Kira::write_triangularSW(unsigned sectorNumber, int k, std::unordered_map<std::uint64_t, std::tuple<BaseIntegral ***, int> >& forwardRed, std::vector <std::uint64_t>& reduce_please, int flagy ){
  
  logger << "\nWrite the triangular system to "<<outputDir<<"/tmp/ on hard disk\n";
  logger << "(--> "<<outputDir<<"/tmp/"<<integralfamily.name<<"/VERconfig, ";
  logger <<outputDir<<"/tmp/"<<integralfamily.name<<"/VER###) \n";
  
  ogzstream output;
  ofstream outputX;
  
  tuple<string,string> dest;
  
  tuple<int,int,unsigned,int> printControl;
  get<0>(printControl) = -1;
  get<1>(printControl) = -1;
  get<2>(printControl) = sectorNumber;
  get<3>(printControl) = k;
  int rememberID = 0;
  
  
  for( auto it = reduce_please.begin(); it < reduce_please.end(); it++){
    BaseIntegral*** fwEQ;
    unsigned lengthEq;
    auto mapContent = forwardRed.find(*it);
    
    if(mapContent != forwardRed.end()){
      tie(fwEQ,lengthEq) = mapContent->second;
      
      
      if( lengthEq /*&& fwEQ[0][0][0].flag2 == 0*/){
	
	print_equationSW(fwEQ[0][0],output,outputX,printControl,dest,rememberID);
      }
    }
  }
  
  output.close();
  
  ifstream inputConfig;
  unsigned tempNumber = 0;
  inputConfig.open ((outputDir+"/tmp/"+integralfamily.name+"/VERconfig.tmp").c_str());
  
  inputConfig >> tempNumber;
  inputConfig.close();
  
  ofstream outputConfig;
  outputConfig.open ((outputDir+"/tmp/"+integralfamily.name+"/VERconfig.tmp").c_str());
  
  if(flagy == 1)
    tempNumber += rememberID;
  
  outputConfig << tempNumber << "\n";
  outputConfig.close();
  
  rename( (outputDir+"/tmp/"+integralfamily.name+"/VER"+"_"+collectReductions[k]
  +"_"+to_string(sectorNumber)+".tmp.gz").c_str(), (outputDir+"/tmp/"+integralfamily.name
  +"/VER"+"_"+collectReductions[k]+"_"+to_string(sectorNumber)+".gz").c_str());

  remove((outputDir+"/tmp/"+integralfamily.name +"/SYSTEMx"+"_"+collectReductions[k]+"_"+to_string(sectorNumber)).c_str());
};



void Kira::write_triangular(/*unsigned sectorNumber, int k,*/ std::unordered_map<std::uint64_t, std::tuple<BaseIntegral ***, int> >& forwardRed, std::vector <std::uint64_t>& reduce_please, int flagy ){
  
  logger << "\nWrite the triangular system to "<<outputDir<<"/tmp/ on hard disk\n";
  logger << "(--> "<<outputDir<<"/tmp/"<<integralfamily.name<<"/VERconfig, ";
  logger <<outputDir<<"/tmp/"<<integralfamily.name<<"/VER###) \n";
  
  ogzstream output;
  ofstream outputX;
  
  tuple<string,string> dest;
  
  tuple<int,int/*,unsigned,int*/> printControl;
  get<0>(printControl) = -1;
  get<1>(printControl) = -1;
//   get<2>(printControl) = sectorNumber;
//   get<3>(printControl) = k;
  int rememberID=0;
  
  
  for( auto it = reduce_please.begin(); it < reduce_please.end(); it++){
    BaseIntegral*** fwEQ;
    unsigned lengthEq;
    auto mapContent = forwardRed.find(*it);
    
    if(mapContent != forwardRed.end()){
      tie(fwEQ,lengthEq) = mapContent->second;
      
      
      if( lengthEq /*&& fwEQ[0][0][0].flag2 == 0*/){

	print_equation(fwEQ[0][0],output,outputX,printControl,dest);
	rememberID++;
      }
    }
  }
  output.close();
  
  ofstream outputConfig;
  outputConfig.open ((outputDir+"/tmp/"+integralfamily.name+"/VERconfig").c_str());
  outputConfig << rememberID << "\n";
  outputConfig.close();
};

template<typename T>
void Kira::read_integral2(T& input, int eqLength,
			  std::unordered_map<std::uint64_t, std::tuple<BaseIntegral ***, int> >& forwardRed,
			  std::unordered_map<std::uint64_t,int>& rdy2F){
    
  BaseIntegral *integral = new BaseIntegral [eqLength];
  
  for (int j=0; j<eqLength;j++){
    
    if(!(input>>integral[j])) break;

    integral[j].length=eqLength;
  }
  
  auto mapContent = forwardRed.find(integral[0].id);
  
  if ( mapContent != forwardRed.end() ) {
    
    if(get<1>(mapContent->second) >= RED){
      
      BaseIntegral*** put2map = new BaseIntegral** [1];
      put2map[0] = new BaseIntegral * [get<1>(mapContent->second)+1];
              
      for(int kIt=0; kIt < get<1>(mapContent->second); kIt++){
        
        put2map[0][kIt] = get<0>(mapContent->second)[0][kIt];
      }
      
      delete [] get<0>(mapContent->second)[0];
      
      get<0>(mapContent->second)[0] = put2map[0];
    }
    
    get<0>(mapContent->second)[0][get<1>(mapContent->second)]=integral;
    get<1>(mapContent->second)++; 
    
  }
  else{
    
    BaseIntegral *** put2map = new BaseIntegral ** [1];
    put2map[0] = new BaseIntegral * [RED];
    put2map[0][0]=integral;
    forwardRed.insert(pair<std::uint64_t, tuple<BaseIntegral***, int> > (integral[0].id, make_tuple(put2map,1)));
    rdy2F.insert(pair<std::uint64_t, int> (integral[0].id, 0));
  }
}


void Kira::skip_integral(igzstream& input,int eqLength){
    
  BaseIntegral *integral = new BaseIntegral [eqLength];
  
  for (int j=0; j<eqLength;j++){
    
    if(!(input>>integral[j])) break;
  }
  delete [] integral;
}


void Kira::trace_integral(igzstream& input,int eqLength){
    
  BaseIntegral *integral = new BaseIntegral [eqLength];
  
  for (int j=0; j<eqLength;j++){
    
    if(!(input>>integral[j])) break;
    
    auto occContent = occurrence.find(integral[j].id);
    if ( occContent != occurrence.end() ) {
      (occContent->second)++;
    }
    else{
      occurrence.insert(pair<std::uint64_t, unsigned > (integral[j].id, 1) );
    }
  }
  delete [] integral;
}

void Kira::read_integral(BaseIntegral*& integral, igzstream& input, int eqLength){
    
  integral = new BaseIntegral [eqLength];
  
  for (int j = 0; j < eqLength; j++){
    
    if(!(input >> integral[j])) break;

    integral[j].length = eqLength;
    
    auto findSkipMaster = find(masterVectorSkip.begin(), masterVectorSkip.end(), integral[j].id);
    if ( findSkipMaster != masterVectorSkip.end() ) {
      integral[j].coefficientString = "0";
    }
  }
}

void Kira::record_masters(std::pair<std::vector<pyred::intid>,std::vector<std::uint64_t> >& subsys){
  
  ofstream output((outputDir+"/results/"+integralfamily.name+"/masters").c_str());
  ofstream tmpoutput((outputDir+"/tmp/"+integralfamily.name+"/masters").c_str());
  
  
//   for (unsigned i=0; i<subsys.second.size(); i++){
// 
//     auto mapContent = mastersReMap.find(subsys.second[i]);
//     
//     if(mapContent != mastersReMap.end()){
//       subsys.second[i]=mapContent->second;
//     }
//   }
  
  std::sort(subsys.second.begin(),subsys.second.end());
  subsys.second.resize(distance(subsys.second.begin(),
                                  unique(subsys.second.begin(), subsys.second.end())) );
  
  logger << "Master integrals: (" << subsys.second.size() << ")\n";
//   output << "Master integrals: (" << subsys.second.size() << ")\n";
  
  for (unsigned i = 0; i < subsys.second.size(); i++){
    tuple<string,unsigned,unsigned> integral;
    
    get_properties(subsys.second[i],integral);

    logger << subsys.second[i] << ": ";
    logger << collectReductions[get<1>(integral)] << ": ";
    logger << get<0>(integral)<< ": ";
    logger << get<2>(integral);
    logger << "\n";

    output << subsys.second[i] << ": ";
    output << collectReductions[get<1>(integral)] << ": ";
    output << get<0>(integral)<< ": ";
    output << get<2>(integral);
    output << "\n";
    
    tmpoutput << subsys.second[i] << "\n";
  }
  output.close();
}

void Kira::complete_triangular(int flagOTF, vector<std::uint64_t>& mandatory){
 
  logger << "\n***** Bring system in triangular form *********************\n";
  
  Clock clock;
  
  set<std::uint64_t> setMandatory(mandatory.begin(), mandatory.end());
  std::unordered_map<std::uint64_t, std::tuple<BaseIntegral ***, int> > forwardRed;
  std::vector <std::uint64_t> reduce_please;
  std::unordered_map<std::uint64_t,int> rdy2F;
  
  for(unsigned k = 0; k < collectReductions.size(); k++) {
    
    for (int sectorNumber = 0; sectorNumber < (1<<integralfamily.jule); sectorNumber++) {
      
//       string inputName = (outputDir+"/tmp/"+integralfamily.name+"/independentEQS_"+collectReductions[k]+"_"+something_string(sectorNumber)).c_str();
    
//       if( !(file_exists(inputName.c_str()) ) && flagOTF )
// 	continue;
      
      load_triangular(flagOTF, k, sectorNumber, forwardRed, reduce_please, rdy2F);
    }
  }
    
  for( auto toIt = forwardRed.begin(); toIt != forwardRed.end(); toIt++){ //collect all keys to iterate
    
    if(get<1>(toIt->second)>1){
      
      reduce_please.push_back(get<0>(toIt->second)[0][0]->id);
    }
  }
  
  sort(reduce_please.begin(),reduce_please.end());
  
  tuple<int,int> printControl;
  get<0>(printControl) = -1;
  get<1>(printControl) = -1;;
  run_reduction(forwardRed, reduce_please, rdy2F, setMandatory, printControl);
  
  write_triangular(/*sectorNumber, k,*/ forwardRed, reduce_please, 1);
  
  // Clean Up
  for( auto toIt = forwardRed.begin(); toIt != forwardRed.end(); toIt++){
    
    if(get<1>(toIt->second)>0){

      delete [] get<0>(toIt->second)[0][0];
    }
    
    delete [] get<0>(toIt->second)[0];
    delete [] get<0>(toIt->second);
  }
  forwardRed.clear();
  rdy2F.clear();
  reduce_please.clear();
  
  logger << "\nTriangular form completed after ( " << clock.eval_time() << " s )\n";
}


void Kira::complete_triangularSW(int flagOTF, vector<std::uint64_t>& mandatory){
 
  logger << "\n***** Bring system in triangular form *********************\n";
  
  Clock clock;
  
  for(int k = collectReductions.size() -1; k >= 0; k--) {
    
    for (int sectorNumber = (1<<integralfamily.jule) -1; sectorNumber >= 0; sectorNumber--) {
      
      std::unordered_map<std::uint64_t, std::tuple<BaseIntegral ***, int> > forwardRed;
      std::vector <std::uint64_t> reduce_please;
      std::unordered_map<std::uint64_t,int> rdy2F;
      
      if(file_exists((outputDir+"/tmp/"+integralfamily.name+"/VER"+"_"+
      collectReductions[k]+"_"+to_string(sectorNumber)+".gz").c_str()))
	continue;
      
//       string inputName = (outputDir+"/tmp/"+integralfamily.name+"/independentEQS_"+collectReductions[k]+"_"+something_string(sectorNumber)).c_str();
    
//       if( !(file_exists(inputName.c_str()) ) && flagOTF )
// 	continue;
      
      if(!load_triangular(flagOTF, k, sectorNumber, forwardRed, reduce_please, rdy2F))
	continue;
      
      logger << "Loaded topology: " << collectReductions[k] << " Loaded sector: " << sectorNumber << "\n";
      for( auto toIt = forwardRed.begin(); toIt != forwardRed.end(); toIt++){ //collect all keys to iterate
	
	if(get<1>(toIt->second)>1){
	  
	  reduce_please.push_back(get<0>(toIt->second)[0][0]->id);
	}
      }
      
      sort(reduce_please.begin(),reduce_please.end());
      
      set<std::uint64_t> setMandatory;
      string stringMandatory = outputDir+"/tmp/"+integralfamily.name+"/VERmandatory";
      if(file_exists((stringMandatory).c_str())){
	
	ifstream inputMandatory(stringMandatory);
	while(1){
	  
	  uint64_t tmpU;
	  if(!(inputMandatory >> tmpU)) break;
	  
	  setMandatory.insert(tmpU);
	}
      }
      else{
	copy(mandatory.begin(), mandatory.end(), inserter(setMandatory, setMandatory.begin()));
      }
//       stringMandatory = outputDir+"/tmp/"+integralfamily.name+"/VERmandatory";
      
      tuple<int,int> printControl;
      get<0>(printControl) = sectorNumber;
      get<1>(printControl) = k;
      
      run_reduction(forwardRed, reduce_please, rdy2F, setMandatory, printControl);
      
      ofstream outputMandatory((stringMandatory+".tmp"));
      for(auto itM : setMandatory){
	outputMandatory << itM << endl;
      }
      rename( (stringMandatory+".tmp").c_str(), (stringMandatory).c_str());
      
      write_triangularSW(sectorNumber, k, forwardRed, reduce_please, 1);
      
      // Clean Up
      for( auto toIt = forwardRed.begin(); toIt != forwardRed.end(); toIt++){ //collect all keys to iterate
	
	if(get<1>(toIt->second)>0){

	  delete [] get<0>(toIt->second)[0][0];
	}
	
	delete [] get<0>(toIt->second)[0];
	delete [] get<0>(toIt->second);
      }
      forwardRed.clear();
      rdy2F.clear();
      reduce_please.clear();
    }
  }
  
  rename( (outputDir+"/tmp/"+integralfamily.name+"/VERconfig.tmp").c_str(), (outputDir+"/tmp/"+integralfamily.name+"/VERconfig").c_str());
  
  logger << "\nTriangular form completed after ( " << clock.eval_time() << " s )\n";
}

int Kira::load_triangular(int flagOTF, unsigned k, int sectorNumber, 
			  std::unordered_map<std::uint64_t, std::tuple<BaseIntegral ***, int> >& forwardRed, std::vector <std::uint64_t>& reduce_please, 
			  std::unordered_map<std::uint64_t,int>& rdy2F){

  vector<unsigned> independent_eqnums;
  unsigned countEq = 0;
  unsigned countALL = 0;
  int breakpoint = 0;
  
  ifstream inputConfig;
  string inputNameOTF = (outputDir+"/tmp/"+integralfamily.name+"/independentEQS_"+collectReductions[k]+"_"+something_string(sectorNumber)).c_str();
  
  if(file_exists(inputNameOTF.c_str()) && flagOTF){
    inputConfig.open(inputNameOTF.c_str());
    
    while(1){
      
      unsigned numberIND;
      
      if(!(inputConfig >> numberIND)) break;
      
      independent_eqnums.push_back(numberIND);
    }
    inputConfig.close();
  }
  
  if(independent_eqnums.size()==0 && flagOTF)
    return 0;
  
  BaseIntegral *integral = new BaseIntegral [1];
  
  vector<string> add;
  add.push_back("");
  add.push_back("x");
  
  for(size_t ll = 0; ll<add.size(); ll++){
    
    if(breakpoint && flagOTF){
      
      break;
    }
    string inputName;
    if(ll == 0){
      inputName = outputDir+"/tmp/"+integralfamily.name + "/SYSTEM"+add[ll] + "_"+collectReductions[k] + "_"+something_string(sectorNumber)+".gz";
      
      if(file_exists(inputName.c_str())){
	igzstream input;
	input.open(inputName.c_str());
	while(1){
	  
	  int eqLength;
	  std::uint64_t ID;
	  string line;
	  if(!(input >> line)) break;
	  if(!(input >> ID)) break;
	  if(!(input >> eqLength)) break;
	  
	  if(flagOTF && (countALL != independent_eqnums[countEq])){
	    
	    countALL++;
	    
	    for (int j=0; j<eqLength;j++){
	      
	      if(!(input>>integral[0])) break;
	    }
	    continue;
	  }
	  else if(flagOTF){
	    countALL++;
	    countEq++;
	  }
	  
	  read_integral2<igzstream>(input, eqLength, forwardRed, rdy2F);
	  
	  if(flagOTF && (countEq > (independent_eqnums.size()-1)) ){
	    breakpoint = 1;
	    break;
	  }
	}
	input.close();
      }
    }
    else{
      inputName = outputDir+"/tmp/"+integralfamily.name + "/SYSTEM"+add[ll] + "_"+collectReductions[k] + "_"+something_string(sectorNumber);
      if(file_exists(inputName.c_str())){
	ifstream input;
	input.open(inputName.c_str());
	while(1){
	  
	  int eqLength;
	  std::uint64_t ID;
	  string line;
	  if(!(input >> line)) break;
	  if(!(input >> ID)) break;
	  if(!(input >> eqLength)) break;
	  
	  if(flagOTF && (countALL != independent_eqnums[countEq])){
	    
	    countALL++;
	    
	    for (int j=0; j<eqLength;j++){

	      if(!(input>>integral[0])) break;
	    }
	    continue;
	  }
	  else if(flagOTF){
	    countALL++;
	    countEq++;
	  }
	  
	  read_integral2<ifstream>(input, eqLength, forwardRed, rdy2F);
	  
	  if(flagOTF && (countEq > (independent_eqnums.size()-1)) ){
	    breakpoint = 1;
	    break;
	  }
	}
	input.close();
      }
    }
  }
  delete [] integral;
  
  if(forwardRed.size() == 0)
    return 0;
  else
    return 1;
};


void Kira::check_reduced_integrals(uint32_t sectorNumber, uint32_t k, DataBase databaseEQ[]){
  string inputName = outputDir+"/tmp/"+integralfamily.name+"/VER" + "_"+collectReductions[k] + "_"+something_string(sectorNumber)+".gz";
	
  int countF = 1;
  
  while(file_exists(inputName.c_str())){
    int num_ones = 0;
    unsigned testSector = sectorNumber;

    for(size_t i = 0; i < SEEDSIZE; ++i, testSector >>= 1) {
      if ((testSector & 1) == 1)
	++num_ones;
    }
    igzstream input;
    input.open(inputName.c_str());
    while(1){
      
      unsigned eqLength; 
      std::uint64_t ID;
      string line;
      if(!(input >> line)) break;
      if(!(input >> ID)) break;
      if(!(input >> eqLength)) break;
      
      if(conditionalSystem){
	
	if(databaseEQ[0].bind_id_get_BSequation(ID, allEq[eqnum][0], masterVectorSkip, occurrence, 1)){
	  
	  skip_integral(input, eqLength);
	  delete [] allEq[eqnum][0];
	}
	else{
	  
	  trace_integral(input, eqLength);
	}
	
      }
      else{
	
	trace_integral(input, eqLength);
      }
      
      eqnum++;
    }
    input.close();
    inputName = outputDir+"/tmp/"+integralfamily.name+"/VER" + "_"+collectReductions[k] + "_"+something_string(sectorNumber)+"_"+to_string(countF++)+".gz";
  }
}

void Kira::load_all_integrals(uint32_t sectorNumber, uint32_t k, DataBase databaseEQ[],std::vector <std::pair<uint32_t,uint32_t> >& setofS, uint32_t& reducedReihen, uint32_t& skipReihen){
  
  string inputName = outputDir+"/tmp/"+integralfamily.name+"/VER" + "_"+collectReductions[k] + "_"+something_string(sectorNumber)+".gz";
	
  int countF = 1;
  
  while(file_exists(inputName.c_str())){
    
    int num_ones=0;
    unsigned testSector = sectorNumber;

    for(size_t i = 0; i < SEEDSIZE; ++i, testSector >>= 1) {
      if ((testSector & 1) == 1)
	++num_ones;
    }
    int it=0;
    igzstream input;
    input.open(inputName.c_str());
    while(1){
      
      unsigned eqLength; 
      std::uint64_t ID;
      string line;
      if(!(input >> line)) break;
      if(!(input >> ID)) break;
      if(!(input >> eqLength)) break;
      
      auto occContent = occurrence.find(ID); 
      
      if(conditionalSystem){
	
	if(databaseEQ[0].bind_id_get_BSequation(ID, allEq[eqnum][0], masterVectorSkip, occurrence, 0)){
	  
	  // this integral is already in the database
	  
	  if(occContent != occurrence.end() && (occContent->second)<2){
	    
	    skip_integral(input, eqLength);
	    skipReihen++;
	    delete [] allEq[eqnum][0];
	    continue;
	  }
	  
	  skip_integral(input,eqLength);
	  reducedReihen++;
	  rdy2P[eqnum]=6;
	}
	else{
	  
	  read_integral(allEq[eqnum][0], input, eqLength);
	  rdy2P[eqnum]=3;
	}
	
      }
      else{
	
	read_integral(allEq[eqnum][0], input, eqLength);
	rdy2P[eqnum]=3;
      }
      
      last_reduce[eqnum] = ID;
      reverseLastReduce.insert( pair<std::uint64_t, unsigned> (ID, eqnum) );
      length[eqnum]++;
      
      if(num_ones < SEEDSIZE && !(it++)){
	int flagy = 0;
	for(size_t itt=0;itt<setofS.size();itt++){
	  if((setofS[itt].first /*&*/ == sectorNumber) /*== setofS[itt].first*/ && setofS[itt].second == k ){
	    flagy = 1;
	    break;
	  }
	}
	if(!flagy){
	  setofS.push_back(make_pair(sectorNumber,k));
	  reduct2StartHere.push_back(eqnum);
	}
      }
      eqnum++;
      
    }
    input.close();
    inputName = outputDir+"/tmp/"+integralfamily.name+"/VER" + "_"+collectReductions[k] + "_"+something_string(sectorNumber)+"_"+to_string(countF++)+".gz";
  }
}


int Kira::load_back_substitution(){
  
  logger << "Load all files from "<<outputDir<<"/tmp/\n";
  
  ifstream inputConfig((outputDir+"/tmp/"+integralfamily.name+"/VERconfig").c_str());
  
  inputConfig >> reihen;
  totalReihen = reihen;
  
  if(reihen == 0){
    logger << "The system you try to reduce contains no equations.\n";
    return 0;
  }
  
  
//   DataBase * database = new DataBase(outputDir+"/results/kira.db");
  DataBase * databaseEQ = new DataBase(outputDir+"/results/kira.db");
  
//   database[0].select_skipid();
  databaseEQ[0].prepare_lookup_id();
  
  reduct2StartHere.clear();
  occurrence.clear();
  reverseLastReduce.clear();
  
  uint32_t skipReihen = 0;
  uint32_t reducedReihen = 0;
  
  inputConfig.close();
    
  allEq = new BaseIntegral** [reihen+1];
  last_reduce = new std::uint64_t [reihen+1];
  length = new unsigned [reihen+1];
  rdy2P = new unsigned [reihen+1];
  
  for (unsigned i = 0; i < reihen+1; i++) {
    allEq[i] = new BaseIntegral* [1];
    length[i] = 0;
    rdy2P[i] = 0;
  }
  
  eqnum = 0;
  
  if(sectorOrdering==2){
  
    for(size_t k = 0; k < collectReductions.size(); k++) {
      for(int itBound = 0; itBound < integralfamily.jule+1; itBound++){
	
	for (size_t sectorNumber = 0; 
	  sectorNumber < static_cast<uint32_t>((1<<integralfamily.jule)); 
      sectorNumber++) {
	  
	  int num_ones=0;
	  unsigned testSector = sectorNumber;

	  for(size_t i = 0; i < SEEDSIZE; ++i, testSector >>= 1) {
	    if ((testSector & 1) == 1)
	      ++num_ones;
	  }
	  
	  if(num_ones == itBound)
	    check_reduced_integrals(sectorNumber, k, databaseEQ);
	  
	}
      }
    }
    
    eqnum = 0;
    vector <pair<unsigned,unsigned> > setofS;
    
    for(size_t k = 0; k < collectReductions.size(); k++) {
      for(int itBound = 0; itBound<integralfamily.jule+1;itBound++){
	
	for (size_t sectorNumber = 0; 
	  sectorNumber < static_cast<uint32_t>((1<<integralfamily.jule)); 
      sectorNumber++) {
	  
	  int num_ones=0;
	  unsigned testSector = sectorNumber;

	  for(size_t i = 0; i < SEEDSIZE; ++i, testSector >>= 1) {
	    if ((testSector & 1) == 1)
	      ++num_ones;
	  }
	  if(num_ones == itBound)
	    load_all_integrals(sectorNumber, k, databaseEQ, setofS, reducedReihen, skipReihen);
	}
      }
    }
  }
  else{
    for(size_t k = 0; k < collectReductions.size(); k++) {
      for (size_t sectorNumber = 0; 
	  sectorNumber < static_cast<uint32_t>((1<<integralfamily.jule)); 
      sectorNumber++) {
	check_reduced_integrals(sectorNumber, k, databaseEQ);
	
      }
    }
    
    eqnum = 0;
    vector <pair<unsigned,unsigned> > setofS;
    
    for(size_t k = 0; k < collectReductions.size(); k++) {
      for (size_t sectorNumber = 0; 
	  sectorNumber < static_cast<uint32_t>((1<<integralfamily.jule)); 
      sectorNumber++) {
	load_all_integrals(sectorNumber, k, databaseEQ, setofS, reducedReihen, skipReihen);
      
      }
    }
  }
  
  reihen = (reihen - skipReihen);
  logger << "Take " << reihen << " equations to reduce " << (reihen-reducedReihen);
  logger << " missing equations" << "\n";
  
//   database[0].finalize();
//   delete database;
  databaseEQ[0].finalize();
  delete databaseEQ;
  
  if(eqnum == 0){
    clean_back_subs();
    logger << "The system is already reduced.\n";
    return 0;
  }
  
  return 1;
  
}

ConvertResult::ConvertResult(Kira& kira, string topologyName_, int topologyNumber_, string &inputName_, const string &inputDir_, vector<std::uint64_t> & idOfSeed){
  
  topologyName = topologyName_;
  topologyNumber = topologyNumber_;
  inputDir = inputDir_;
  inputName = inputName_;
  
  if( !(file_exists(inputName.c_str())) ){
    logger << "\nMissing file with a list of integrals: " << inputName<<"\n";
    logger << "\nUnable to export results to Mathematica\n";
    exit(-1);
  }
  
  seedsInput.open( inputName.c_str() );
  
  look_up_seeds();
  database = new DataBase(inputDir+"/results/kira.db"); 
  
  get_integral_id_pyred(idOfSeed);
}

ConvertResult::ConvertResult(Kira& kira, string topologyName_, int topologyNumber_, const string &inputDir_, std::vector<pyred::Integral>& listOfIntegrals, vector<std::uint64_t> & idOfSeed, string &inputName_){
  
  topologyName = topologyName_;
  topologyNumber = topologyNumber_;
  inputDir = inputDir_;
  inputName = inputName_;
  
  for(auto igl: listOfIntegrals ){
    
//     auto iglback = pyred::Integral(*ItSeed);
      
//     auto property = iglback.properties(*ItSeed);
    
//     id2int << "- [" << *ItSeed << ",";
//     for(int itt = 0; itt < integralfamily.jule; itt++)
//       id2int << iglback.m_powers[itt] << ",";
//     id2int << property.sector << ",";
//     id2int << property.topology << ",";
//     id2int << property.lines << ",";
//     id2int << property.dots << ",";
//     id2int << property.sps << ",";
//     id2int << "0";
//     id2int << "]\n";
    
//     auto igl = pyred::Integral(topologyNumber,arraySeed[it]);
    
    std::uint64_t ID = igl.to_weight();
    if(Integral::is_zero(ID)){
      zeroIntegrals.push_back(igl.m_powers);
//       logger << "Integral: "<< igl.m_powers << " is zero.\n";
      continue;
    }
    idOfSeed.push_back(ID); 
  }

  database = new DataBase(inputDir+"/results/kira.db"); 
}

ConvertResult::~ConvertResult(){
  
 delete database;
 
}

void ConvertResult::get_integral_id_pyred(vector<std::uint64_t> &idOfSeed){  
  
  for(size_t it = 0; it!=arraySeed.size();it++){
    
//     auto iglback = pyred::Integral(*ItSeed);
      
//     auto property = iglback.properties(*ItSeed);
    
//     id2int << "- [" << *ItSeed << ",";
//     for(int itt = 0; itt < integralfamily.jule; itt++)
//       id2int << iglback.m_powers[itt] << ",";
//     id2int << property.sector << ",";
//     id2int << property.topology << ",";
//     id2int << property.lines << ",";
//     id2int << property.dots << ",";
//     id2int << property.sps << ",";
//     id2int << "0";
//     id2int << "]\n";
    
    auto igl = pyred::Integral(topologyNumber,arraySeed[it]);
    
    std::uint64_t ID = igl.to_weight();
    if(Integral::is_zero(ID)){
      zeroIntegrals.push_back(arraySeed[it]);
//       logger << "Integral: "<< arraySeed[it] << " is zero.\n";
      continue;
    }
    idOfSeed.push_back(ID); 
  }
  
//   for(auto itg : arraySeed)
//     cout << itg << endl;
}

void ConvertResult::look_up_seeds(){

  logger << "Look up the seeds.\n";

  string line;
  
  logger.set_level(2);
  
  while(getline(seedsInput, line)) {
    
    //get string indices
    if(line.size()<3)
      continue;
    
    size_t found;
    
    if((found = line.find_first_of("#")) != string::npos){
      logger << "this line will be skept: " << line<< "\n";
      continue;
    }
    
    if((found = line.find_first_of("[")) == string::npos){
      logger << "this line will be skept: " << line<< "\n";
      continue;
    }
    
    line.erase(line.begin(),line.begin()+found+1);
    line.erase(remove_if(line.begin(), line.end(), ::isspace), line.end());
    
    if((found = line.find_first_of("]")) == string::npos){
      logger << "this line will be skept: " << line<< "\n";
      continue;
    }
    
    line.erase(line.begin()+found);//indices
    
    //convert string indices to integer array
    int indicesCounter;
    vector<int> inputSeed;
    
    while((found = line.find_first_of(",")) != string::npos){
      
      istringstream(line.substr(0,found))>>indicesCounter;
      inputSeed.push_back(indicesCounter);
      line=line.substr(found+1);
      
    }
    
    istringstream(line) >> indicesCounter;
    inputSeed.push_back(indicesCounter);
    arraySeed.push_back(inputSeed);
    
  }
  logger.set_level(1);
}

void ConvertResult::reconstruct_mass( Kira& kira, vector<DBintegral>& integralV ){

  for(size_t iterator=0;iterator!=integralV.size();iterator++){
    
    int massDimension = integralV[iterator].massDimension-integralV[0].massDimension;

    Perl2Kira perlKira(kira.invar,kira.massSet2One,kira.invarDim,kira.massSet2OneDim);
    perlKira.pipe_kira();
    perlKira.put_pipe(integralV[iterator].coefficient); 
    perlKira.close_pipe();
    perlKira.read_pipe(integralV[iterator].coefficient);
    stringstream ss;
    ss <<"*"<< kira.massSet2One<<"^("<<massDimension*2<<"/"<<kira.massSet2OneDim<<")";
    string MD = ss.str();
    integralV[iterator].coefficient="("+integralV[iterator].coefficient+")"+MD;
    kira.fermat[0].fermat_collect(const_cast<char*>(integralV[iterator].coefficient.c_str()));
    kira.fermat[0].fermat_calc();
    integralV[iterator].coefficient.assign(kira.fermat[0].g_baseout);
  }
}

void ConvertResult::prepare_FORM( vector<DBintegral>& integralV ){
  
  for(size_t iterator=0;iterator!=integralV.size();iterator++){
    
    Perl2Kira perlKira;
    perlKira.pipe_kira();
    perlKira.put_pipe(integralV[iterator].coefficient); 
    perlKira.close_pipe();
    perlKira.read_pipe(integralV[iterator].coefficient);
  }
}

int ConvertResult::output(Kira& kira, int massReconstruction, vector<std::uint64_t>& idOfSeed, int choice){  
  
  kiraOutput outString(choice);
  string reducefile = inputDir+"/results/kira.db";
  
  if( !(file_exists(reducefile.c_str())) ){
    logger << "\nMissing file " << reducefile << ".\n";
    logger << "\nUnable to export results to "<<outString.str[0]<< "\n";
    return 0;
  }
  string name = "EQUATION";
  if( !database[0].checkTable(name)){
    logger << "\nNo reductions found in " << reducefile << ".\n";
    logger << "\nUnable to export results to "<<outString.str[0]<< "\n";
    database[0].finalize();
    return 0;
  }
  
  string masterFileName = inputDir+"/results/"+topologyName+"/masters";
  ifstream input;
  
  vector<std::uint64_t> masterVector;
  
  if(file_exists(masterFileName.c_str())){  
    input.open(masterFileName.c_str());
    string tmp;
    while(1){
      std::uint64_t ID;
      if(!(input >>ID)) break;
      if((input >>tmp))
	;
      if((input >>tmp))
	;
      if((input >>tmp))
	;
      if((input >>tmp))
	;
      
//       auto mapContent = kira.mastersMap.find(ID);
//                     
//       if(mapContent != kira.mastersMap.end()){
//         
//         ID=mapContent->second;
//       }
      masterVector.push_back(ID);
    }
    input.close();
  }
    
  string chars = "/";
  inputName.erase(remove_if(inputName.begin(), inputName.end(), [&chars](const char& c) {
    
    return chars.find(c) != string::npos;
  }), inputName.end());
  
  string mathematicafile = inputDir+"/results/" +topologyName + "/kira_"+inputName+ outString.str[1];
  Output.open( mathematicafile.c_str() );

  Output << outString.str[2];
    
  for(unsigned int it=0; it!=zeroIntegrals.size(); it++){
    
    Output << outString.str[3] << kira.collectReductions[topologyNumber];
    Output << outString.str[5];
    
    for(unsigned int iterator=0; iterator<zeroIntegrals[it].size();iterator++){
    
      Output <<zeroIntegrals[it][iterator];
     
      if(iterator!=zeroIntegrals[it].size()-1)
	Output<< ",";
    }
    Output << outString.str[7]<<outString.str[4]<<outString.str[10];
    if(it!=(zeroIntegrals.size()-1))
      Output << outString.str[6];
  }
  
  database[0].prepare_lookup_id();
  
  int counter=0;
  uint32_t unreduced = 0;
  for(size_t it=0; it!=idOfSeed.size(); it++){
    
    vector<DBintegral> integralV;
    database[0].bind_id_get_equation(idOfSeed[it],integralV);
    
    if(integralV.size()>0)
      counter++;
    
    if(counter==1 && zeroIntegrals.size()>0 && integralV.size()>0)
      Output <<outString.str[6];
    
    if(counter>1 && integralV.size()>0)
      Output <<outString.str[6];
    
    if(massReconstruction)
      reconstruct_mass( kira, integralV );
    
    if(choice == 0)
      prepare_FORM(integralV);
    
    for(size_t iterator=0;iterator!=integralV.size();iterator++){
    
      if(iterator == 0)
	Output << outString.str[3] << kira.collectReductions[integralV[iterator].topology] <<outString.str[5];
      else
	Output << outString.str[4]<<kira.collectReductions[integralV[iterator].topology]<<outString.str[5];
      
//       if(choice==2)
// 	Output << integralV[iterator].id << ",";
      
      Output <<integralV[iterator].indices;
      
      if(iterator==0)
	Output <<outString.str[7];
      else
	Output <<outString.str[8]<<integralV[iterator].coefficient<<outString.str[9];
    }
    
    if(integralV.size()==1)
      Output <<outString.str[10];
    
    if(integralV.size()==0 && idOfSeed[it] != 0){
      
      auto itM = find (masterVector.begin(), masterVector.end(), idOfSeed[it]);
      if (itM != masterVector.end()){
// 	std::cout << "Element found in masterVector: " << *itM << '\n';
	
// 	auto mapContent = mastersReMap.find(itg);
// 	if(mapContent != mastersReMap.end())      
// 	  itg = mapContent->second;
	logger << "Master integral: ";
	tuple<string,unsigned,unsigned> integral;
	get_properties(idOfSeed[it], integral);
	logger << (idOfSeed[it]) << ": ";
	logger << kira.collectReductions[get<1>(integral)] << ": ";
	logger << get<0>(integral) << ": ";
	logger << get<2>(integral);
	logger << "\n";
      }
      else{
	logger.set_level(2);
	logger << "Integral is unreduced: ";
	tuple<string,unsigned,unsigned> integral;
	get_properties(idOfSeed[it], integral);
	logger << (idOfSeed[it]) << ": ";
	logger << kira.collectReductions[get<1>(integral)] << ": ";
	logger << get<0>(integral) << ": ";
	logger << get<2>(integral);
	logger << "\n";
	logger.set_level(1);
	unreduced++;
      }
    }
    
    if(integralV.size()==0 && idOfSeed[it] == 0)
      logger << "One integral is zero.\n";
  }
  
  logger << "unreduced integrals: " << unreduced << ".\n";
  
  database[0].finalize();
  
  Output << outString.str[11]<< "\n";
  logger << "\n";
  return 1;
};
