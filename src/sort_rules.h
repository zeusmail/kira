/* This file is a part of the program Kira.
 * Copyright (C) Johann Usovitsch <jusovitsch@googlemail.com>
 * Philipp Maierhoefer <particle@maierhoefer.net>
 * Peter Uwer <peter.uwer@physik.hu-berlin.de>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version , or (at
 * your option) any later version as published by the Free Software 
 * Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
*/
#ifndef SORT_RULES_H
#define SORT_RULES_H

class sort_rules {
private:
  int len;
public:
 sort_rules(int len_) : len(len_) { }
  
 bool operator() (int l[], int r[]) {

    int DENCOUNTL = 0, SECTORL = 0, DOTSL = 0, NUML = 0;
    int DENCOUNTR = 0, SECTORR = 0, DOTSR = 0, NUMR = 0;

    for (int it = 0; it<len; it++) {
     
      if ( l[it] > 0 ) {
        SECTORL += ( 1 << it );
        DENCOUNTL++;
        DOTSL +=l[it];
      }
      else{
        NUML-=l[it];
      }
      
      if ( r[it] > 0 ) {
        SECTORR += ( 1 << it );
        DENCOUNTR++;
        DOTSR +=r[it];
      }
      else{
        NUMR -= r[it];
      }
  
    }
  
    
    
    // compare dots (lines)
  if ( DENCOUNTL < DENCOUNTR ) {
    return 1;
  }
  
  if ( DENCOUNTL > DENCOUNTR ) {
    return 0;
  }
  
  
  if ( SECTORL < SECTORR ) { 
    return 1;
  }

  if ( SECTORL > SECTORR ) {
    return 0;
  }
    
  // compare dots (total)
  if ( DOTSL < DOTSR ) {
    return 1;
  }
  
  if ( DOTSL > DOTSR ) {
    return 0;
  }
  
  // compare negative dots (total)
  if ( NUML < NUMR ) { 
    return 1;
  }
  
  if ( NUML > NUMR ) {
    return 0;
  }
  
  //first propagator with negative dots is simpler
  for (int it = 0; it<len; it++){
    
    if(l[it]>0)
      continue;
    
    if ( l[it] < r[it] ){
      return 1;
    }
    if ( l[it] > r[it] ){
      return 0;
    }
  }
  
  //last propagator with positive dots is simpler
  for (int it = 0; it<len; it++) {
    
    if(l[it]<0)
      continue;
    
    if ( l[it] < r[it] ){
      return 1;
    }
    if ( l[it] > r[it] ){
      return 0;
    }
  }
    
    
    
//     for (int it = 0; it<len; it++) {
//       if ( r[it] > 0 ) {
//         rI2 += ( 1 << it );
//         RightTotal++;
//       }
//   
//       if ( l[it] > 0 ) {
//         lI2 += ( 1 << it );
//         LeftTotal++;
//       }
//     }
//   
//     if (LeftTotal < RightTotal)
//       return 1;
// 
//     if (LeftTotal > RightTotal)
//       return 0;
//   
//   
//     if ( lI2 < rI2 ) 
//       return 1;
// 
//     if ( lI2 > rI2 ) 
//       return 0;
    
    return 0;
  }
};

class equal_rules {
 private:
  int len;
 public:
 equal_rules(int len_) : len(len_) { }
  
  bool operator() (int l[], int r[]) {
    
    int total=0;
    for (int it = 0; it<len; it++) {
      if ( r[it] == l[it] ) {
        total++;
      }
    }
    if(total==len)
      return 1;
    
    return 0;
  }
};

#endif //SORT_RULES_H
